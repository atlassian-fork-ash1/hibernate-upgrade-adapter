# Hibernate Migration Adapter

Hibernate Migration Adapter provides a way for an application to use the Hibernate v5.x.x APIs
through v2.1.8 interfaces, for the purpose of upgrading Hibernate in a large project without
refactoring all the code at once. This library was originally written for the purpose of upgrading
Confluence 6.0 to Hibernate 5.

The adapter implementation is compatible with .hbm configured mappings, most Hibernate v2 APIs and
Spring hibernate v2 integration.

## Features

 - No code changes are required to use new Hibernate v5 features alongside v2 features.
 - Both v2 and v5 Session implementations may be used simultaneously within the same transaction
   (using HibernateBridgeMode.*_WITH_SESSION_BRIDGE modes).
 - v5 SessionFactory and Session implementations may be used behind v2 adapter interfaces, usually
   without any code changes.
 - The hibernate engine version used may be configured at runtime.

## Usage

To use, add this maven package as a dependency to your application project and replace uses of
the Configuration class in code and LocalSessionFactoryBean in spring config:

 - Add hibernate-upgrade-adapter as a maven dependency.
   If running slightly different versions of Spring and hibernate 2, consider excluding them:

```
<dependency>
  <groupId>com.atlassian.hibernate</groupId>
  <artifactId>hibernate.adapter</artifactId>
  <version>1.0.0</version>
  <exclusions>
    <exclusion>
      <groupId>hibernate</groupId>
      <artifactId>hibernate</artifactId>
      <version>2.1.8</version>
    </exclusion>
    <exclusion>
      <groupId>org.springframework</groupId>
      <artifactId>spring-hibernate</artifactId>
      <version>1.2.9</version>
    </exclusion>
  </exclusions>
</dependency>
```

 - Use com.atlassian.hibernate.adapter.ConfigurationV2Bridge in place of
   net.sf.hibernate.cfg.Configuration.
   
   For example:

```
Configuration cfg = new ConfigurationV2Bridge(); // new Configuration();
    .addResource("Item.hbm.xml")
    .addResource("Bid.hbm.xml");
```

 - Use com.atlassian.hibernate.adapter.LocalSessionFactoryV2BridgeBean in place of
   org.springframework.orm.hibernate.LocalSessionFactoryBean.
   
   For example:
```
 <bean id="sessionFactory" class="com.atlassian.hibernate.adapter.LocalSessionFactoryV2BridgeBean">
                       <!--class="org.springframework.orm.hibernate.LocalSessionFactoryBean"-->
    <property name="dataSource" ref="dataSource" />
    <property name="hibernateProperties">
       <props>
          <prop key="hibernate.hbm2ddl.auto">${hibernate.hbm2ddl.auto}</prop>
          <prop key="hibernate.dialect">${hibernate.dialect}</prop>
       </props>
    </property>
 </bean>
```

## v2 APIs not supported

When using v5 -> v2 adapter mode, be aware that the adapter logic won't emulate all v2 features perfectly.

Also, the following features are not supported and won't work:

 - JTA transactions
 - Some v2 HQL queries may need minor changes
 - Object mappings not configured through .hbm files
 - Other - search this repository for instances of 'throw new NotImplementedException'

## How to get to the v5 interfaces
See com.atlassian.hibernate.adapter.HibernateBridge.

## Package Structure
*com.atlassian.hibernate.adapter* - classes that implement the SessionFactory / Session bridge.

*com.atlassian.hibernate.adapter.adapters* - contains the v5 -> v2 adapter classes.

*com.atlassian.hibernate.util* - helper classes (such as SessionHelper) that may be used
                                 to help migrate from v2 to v5 interfaces.

## Tests
To build and run tests, run:
```
mvn clean install
```

## Bamboo
The bamboo build that's used to build and release atlassian-graphql can be found here:

https://confluence-cloud-bamboo.internal.atlassian.com/browse/HBA

## Contributors
Pull requests, issues and comments welcome. For pull requests:

* Add tests for new features and bug fixes
* Follow the existing style
* Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.

For bigger changes, make sure you start a discussion first by creating
an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement,
known as a CLA. This serves as a record stating that the contributor is
entitled to contribute the code/documentation/translation to the project
and is willing to have it used in distributions and derivative works
(or is willing to transfer ownership).

Prior to accepting your contributions we ask that you please follow the appropriate
link below to digitally sign the CLA. The Corporate CLA is for those who are
contributing as a member of an organization and the individual CLA is for
those contributing as an individual.

* [CLA for corporate contributors](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
* [CLA for individuals](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

## License
Copyright (c) 2017 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.
