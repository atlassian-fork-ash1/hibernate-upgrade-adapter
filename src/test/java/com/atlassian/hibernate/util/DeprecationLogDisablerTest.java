package com.atlassian.hibernate.util;

import org.junit.Test;

public class DeprecationLogDisablerTest {
    @Test
    public void testDisableLogging() {
        // expect no exception
        DeprecationLogDisabler.disableLogging("logDeprecationOfClassEntityTypeSelector", true);
        DeprecationLogDisabler.disableLogging("logDeprecationOfClassEntityTypeSelector", true);
    }
}
