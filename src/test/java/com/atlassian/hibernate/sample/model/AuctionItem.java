package com.atlassian.hibernate.sample.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AuctionItem extends Persistent {
	private String description;
	private List<Bid> bids;
	private Bid successfulBid;
	private User seller;
	private Date ends;
	private int condition;

	public AuctionItem() { }

	public AuctionItem(final String description, final Date ends, final User seller, final int condition) {
		this.description = description;
		this.ends = ends;
		this.seller = seller;
		this.condition = condition;
		this.bids = new ArrayList<>();
	}

	public List<Bid> getBids() {
		return bids;
	}

	public String getDescription() {
		return description;
	}

	public User getSeller() {
		return seller;
	}

	public Bid getSuccessfulBid() {
		return successfulBid;
	}

	public void setBids(final List<Bid> bids) {
		this.bids = bids;
	}

	public void setDescription(final String string) {
		description = string;
	}

	public void setSeller(final User user) {
		seller = user;
	}

	public void setSuccessfulBid(final Bid bid) {
		successfulBid = bid;
	}

	public Date getEnds() {
		return ends;
	}

	public void setEnds(final Date date) {
		ends = date;
	}
	
	public int getCondition() {
		return condition;
	}

	public void setCondition(final int i) {
		condition = i;
	}

	public String toString() {
		return description + " (" + condition + "/10)";
	}
}
