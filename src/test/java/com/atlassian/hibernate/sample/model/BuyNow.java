package com.atlassian.hibernate.sample.model;

import java.util.Date;

public class BuyNow extends Bid {
	public BuyNow() { }

	public BuyNow(final AuctionItem item, final float amount, final Date datetime, final User bidder) {
		super(item, amount, datetime, bidder);
	}

	public boolean isBuyNow() {
		return true;
	}

	public String toString() {
		return super.toString() + " (buy now)";
	}
}
