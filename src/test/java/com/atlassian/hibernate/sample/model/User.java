package com.atlassian.hibernate.sample.model;

import java.util.List;

public class User extends Persistent {
	private String userName;
	private String password;
	private String email;
	private Name name;
	private List<Bid> bids;
	private List<AuctionItem> auctions;
	
	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}

	public String getUserName() {
		return userName;
	}

	public void setEmail(final String string) {
		email = string;
	}

	public void setPassword(final String string) {
		password = string;
	}

	public void setUserName(final String string) {
		userName = string;
	}

	public List<AuctionItem> getAuctions() {
		return auctions;
	}

	public List<Bid> getBids() {
		return bids;
	}

	public void setAuctions(final List<AuctionItem> list) {
		auctions = list;
	}

	public void setBids(final List<Bid> list) {
		bids = list;
	}
	
	public String toString() {
		return userName;
	}

	public Name getName() {
		return name;
	}

	public void setName(final Name name) {
		this.name = name;
	}
}
