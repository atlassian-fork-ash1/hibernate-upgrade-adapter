package com.atlassian.hibernate.sample.model;

public class Name {
	private String firstName;
	private String lastName;
	private Character initial;

	private Name() { }

	public Name(final String first, final Character middle, final String last) {
		firstName = first;
		initial = middle;
		lastName = last;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	public Character getInitial() {
		return initial;
	}

	public void setInitial(final Character initial) {
		this.initial = initial;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}
	
	public String toString() {
		final StringBuffer buf = new StringBuffer()
			.append(firstName)
			.append(' ');
		if (initial!=null) {
			buf.append(initial).append(' ');
		}
		return buf.append(lastName).toString();
	}
}
