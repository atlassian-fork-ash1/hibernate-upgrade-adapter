package com.atlassian.hibernate.adapter.reflection;

import com.atlassian.hibernate.adapter.reflection.criterion.BetweenExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.ExampleV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.IlikeExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.InExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.LogicalExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.NotExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.NotNullExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.NullExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.PropertyExpressionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.SQLCriterionV2Reflection;
import com.atlassian.hibernate.adapter.reflection.criterion.SimpleExpressionV2Reflection;
import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(Parameterized.class)
public class ReflectionTest {
    private final Class reflectionClass;

    public ReflectionTest(final Class reflectionClass) {
        this.reflectionClass = reflectionClass;
    }

    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new Object[][] {
            { ConfigurationV2Reflection.class },
            { JunctionV2Reflection.class },
            { OrderV2Reflection.class },
            { BetweenExpressionV2Reflection.class },
            { ExampleV2Reflection.class },
            { IlikeExpressionV2Reflection.class },
            { InExpressionV2Reflection.class },
            { LogicalExpressionV2Reflection.class },
            { NotExpressionV2Reflection.class },
            { NotNullExpressionV2Reflection.class },
            { NullExpressionV2Reflection.class },
            { PropertyExpressionV2Reflection.class },
            { SimpleExpressionV2Reflection.class },
            { SQLCriterionV2Reflection.class },
            { SessionFactoryImplV2Reflection.class },
            { SessionImplV2Reflection.class },
            { FilterImplV2Reflection.class },
            { CustomTypeV2Reflection.class }
        });
    }

    public void testUnsafeReflectionHelperImpl() {
        Class unsafe = null;
        try {
            unsafe = Class.forName("com.sun.Unsafe");
        } catch (final ClassNotFoundException ex) {
            // no com.sun.Unsafe for this JVM
        }
        if (unsafe != null) {
            assertEquals("UnsafeReflectionHelper", ReflectionHelper.INSTANCE.getClass().getSimpleName());
        }
    }

    @Test
    public void testFields() throws Exception {
        for (final Field field : reflectionClass.getDeclaredFields()) {
            field.setAccessible(true);
            if (Modifier.isStatic(field.getModifiers())) {
                assertNotNull(field.getName(), field.get(null));
            }
        }
    }
}
