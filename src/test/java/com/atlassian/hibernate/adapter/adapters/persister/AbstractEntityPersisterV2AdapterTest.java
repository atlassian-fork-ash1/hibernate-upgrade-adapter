package com.atlassian.hibernate.adapter.adapters.persister;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.bridge.factory.SessionFactoryBridgeAssociations;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.tuple.entity.EntityTuplizer;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashMap;

import static org.junit.Assert.assertNotNull;

public class AbstractEntityPersisterV2AdapterTest {
    @Test
    public void testConstructor() {
        final EntityTuplizer entityTuplizer = Mockito.mock(EntityTuplizer.class);
        Mockito.when(entityTuplizer.getMappedClass()).thenReturn(Object.class);

        final org.hibernate.persister.entity.AbstractEntityPersister persister =
                Mockito.mock(org.hibernate.persister.entity.AbstractEntityPersister.class);
        Mockito.when(persister.getEntityTuplizer()).thenReturn(entityTuplizer);

        final SessionFactoryWithBridge sessionFactory = Mockito.mock(SessionFactoryWithBridge.class);
        final net.sf.hibernate.SessionFactory sessionFactoryV2 = Mockito.mock(net.sf.hibernate.engine.SessionFactoryImplementor.class);
        Mockito.when(sessionFactory.getProperties()).thenReturn(new HashMap<>());
        SessionFactoryBridgeAssociations.associate(sessionFactory, sessionFactory);
        Mockito.when(sessionFactory.getV2orV5SessionFactory()).thenReturn(sessionFactoryV2);

        final net.sf.hibernate.persister.AbstractEntityPersister adapted =
                AbstractEntityPersisterV2Adapter.adaptAbstractEntityPersister(persister, sessionFactory, null);
        assertNotNull("AbstractEntityPersister v2 adapted successfully", adapted);
    }

    public interface SessionFactoryWithBridge extends SessionFactoryImplementor, HibernateBridge {
    }
}
