package com.atlassian.hibernate.adapter.adapters;

import com.atlassian.hibernate.adapter.adapters.type.UserTypeV5Adapter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class UserTypeV5AdapterTest {
    @Mock
    private MockUserType userType;

    static abstract class MockUserType implements net.sf.hibernate.UserType {
        public final boolean equals(Object that) {
            return that.getClass() == this.getClass();
        }
    }

    @Test
    public void testSerializeDeserialize_NonSerializableUserType() throws Exception {
        final org.hibernate.usertype.UserType type = UserTypeV5Adapter.adapt(userType);
        final org.hibernate.usertype.UserType deserializedUserType = (org.hibernate.usertype.UserType)
                serializeDeserialize((Serializable) type);
        assertEquals(
                "net.sf.hibernate.UserType of correct type should be constructed on deserialization",
                type, deserializedUserType);
    }

    private static Object serializeDeserialize(final Serializable obj) throws Exception {
        final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(obj);
        objectOutputStream.flush();

        final ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
        final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        return objectInputStream.readObject();
    }
}
