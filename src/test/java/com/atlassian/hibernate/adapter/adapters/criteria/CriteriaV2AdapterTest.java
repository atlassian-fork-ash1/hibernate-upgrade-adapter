package com.atlassian.hibernate.adapter.adapters.criteria;

import com.atlassian.hibernate.sample.model.AuctionItem;
import com.atlassian.hibernate.sample.model.User;
import com.atlassian.hibernate.test.HibernateAdapterTestBase;
import net.sf.hibernate.Criteria;
import net.sf.hibernate.Session;
import net.sf.hibernate.expression.Disjunction;
import net.sf.hibernate.expression.EqExpression;
import net.sf.hibernate.expression.Expression;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class CriteriaV2AdapterTest extends HibernateAdapterTestBase {
    @Test
    @SuppressWarnings("unchecked")
    public void testAddAndList() throws Exception {
        Session session = sessionFactoryV2.openSession();
        try {
            AuctionItem item = createTestAuctionItem(session);

            Criteria criteria = session.createCriteria(User.class);
            Disjunction disjunction = Expression.disjunction();

            // the order of add() here is significant
            criteria.add(disjunction);
            disjunction.add(new EqExpression("id", item.getSeller().getId(), false));

            List<User> list = criteria.list();
            assertEquals("1 user returned", 1, list.size());
            assertEquals("Correct user returned", item.getSeller().getName(), list.get(0).getName());
        } finally {
            session.close();
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testAddAndUniqueResult() throws Exception {
        Session session = sessionFactoryV2.openSession();
        try {
            AuctionItem item = createTestAuctionItem(session);

            Criteria criteria = session.createCriteria(User.class);
            Disjunction disjunction = Expression.disjunction();

            // the order of add() here is significant
            criteria.add(disjunction);
            disjunction.add(new EqExpression("id", item.getSeller().getId(), false));
            assertEquals("User returned", item.getSeller().getName(), ((User) criteria.uniqueResult()).getName());
        } finally {
            session.close();
        }
    }
}
