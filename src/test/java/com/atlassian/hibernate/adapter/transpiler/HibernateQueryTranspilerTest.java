package com.atlassian.hibernate.adapter.transpiler;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HibernateQueryTranspilerTest {
    @Test
    public void testCommaBeforeWhere() {
        final String transpiled = HibernateQueryTranspiler.transpileQuery("select * from x,  where");
        assertEquals("select * from x  where", transpiled);
    }

    @Test
    public void testCastCountFromIntToString() {
        final String transpiled = HibernateQueryTranspiler.transpileQuery("select count(*)");
        assertEquals("select CAST(count(*) AS integer)", transpiled);
    }

    @Test
    public void testCastCountFromIntToString_Capitalized() {
        final String transpiled = HibernateQueryTranspiler.transpileQuery("select COUNT(*)");
        assertEquals("select CAST(COUNT(*) AS integer)", transpiled);
    }

    @Test
    public void testCastCountFromIntToString_SkipIfAlreadyTranspiled() {
        final String transpiled = HibernateQueryTranspiler.transpileQuery("select CAST(count(*) AS integer)");
        assertEquals("select CAST(count(*) AS integer)", transpiled);
    }

    @Test
    public void testClassToTypeFunction() {
        final String transpiled = HibernateQueryTranspiler.transpileQuery(
                "select blah.class where (blah.class = com.AClass) and (blah.class != com.AClass)");
        assertEquals("select type(blah) where (type(blah) = com.AClass) and (type(blah) != com.AClass)", transpiled);
    }

    @Test
    public void testClassToTypeFunction2() {
        final String transpiled = HibernateQueryTranspiler.transpileQuery(
                "from com.atlassian.confluence.core.ContentEntityObject content\n" +
                        "where content.originalVersion is null\n" +
                        "and content.contentStatus = 'current'\n" +
                        "and (content.class = com.atlassian.confluence.pages.Page\n" +
                        "     or content.class = com.atlassian.confluence.pages.BlogPost\n" +
                        "     or content.class = com.atlassian.confluence.pages.Comment)\n" +
                        "and content.creationDate > :startDate");
        assertEquals(
                "from com.atlassian.confluence.core.ContentEntityObject content\n" +
                        "where content.originalVersion is null\n" +
                        "and content.contentStatus = 'current'\n" +
                        "and (type(content) = com.atlassian.confluence.pages.Page\n" +
                        "     or type(content) = com.atlassian.confluence.pages.BlogPost\n" +
                        "     or type(content) = com.atlassian.confluence.pages.Comment)\n" +
                        "and content.creationDate > :startDate",
                transpiled);
    }

    @Test
    public void testClassToTypeFunction_NotForInOperator() {
        final String transpiled = HibernateQueryTranspiler.transpileQuery(
                "select blah.class where blah.class in (:types)");
        assertEquals("select type(blah) where blah.class in (:types)", transpiled);
    }

    @Test
    public void testInClass() {
        final String transpiled = HibernateQueryTranspiler.transpileQuery(
                "select distinct hibuser from hibuser in class com.etc.Entity where");
        assertEquals("select distinct hibuser from com.etc.Entity as hibuser where", transpiled);
    }

    @Test
    public void testInClass2() {
        final String transpiled = HibernateQueryTranspiler.transpileQuery(
                "select item.key from item in class bucket.user.propertyset.BucketPropertySetItem where");
        assertEquals("select item.key from bucket.user.propertyset.BucketPropertySetItem as item where", transpiled);
    }

    @Test
    public void testSkipLiteral() {
        final String transpiled = HibernateQueryTranspiler.transpileQuery(
                "select * from x  where x.fieldName = '.class'");
        assertEquals("select * from x  where x.fieldName = '.class'", transpiled);
    }

    @Test
    public void testSkipLiteral2() {
        final String transpiled = HibernateQueryTranspiler.transpileQuery(
                "select * from x  where x.fieldName = '\\'.class'");
        assertEquals("select * from x  where x.fieldName = '\\'.class'", transpiled);
    }
}
