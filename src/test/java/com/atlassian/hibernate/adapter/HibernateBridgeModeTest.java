package com.atlassian.hibernate.adapter;

import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class HibernateBridgeModeTest {
    @Test
    public void testIsV2() {
        for (final HibernateBridgeMode value : HibernateBridgeMode.values()) {
            assertEquals(value.toString(), value.toString().contains("V2"), value.isV2());
        }
    }

    @Test
    public void testIsV5() {
        for (final HibernateBridgeMode value : HibernateBridgeMode.values()) {
            assertEquals(value.toString(), value.toString().contains("V5"), value.isV5());
        }
    }
}
