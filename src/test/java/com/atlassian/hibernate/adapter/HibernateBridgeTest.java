package com.atlassian.hibernate.adapter;

import com.atlassian.hibernate.adapter.bridge.session.SessionBridgeAssociations;
import com.atlassian.hibernate.test.HibernateAdapterTestBase;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class HibernateBridgeTest extends HibernateAdapterTestBase {
    private HibernateBridge hibernateBridge;

    @Before
    public void setUp() {
        hibernateBridge = HibernateBridge.get(sessionFactoryV2);
    }

    @Test
    public void testInstanceSameForV2andV5() {
        assertEquals(HibernateBridge.get(sessionFactoryV2), HibernateBridge.get(sessionFactoryV2));
    }

    @Test
    public void testGetFromV2() {
        assertNotNull(HibernateBridge.get(sessionFactoryV2));
        assertNotNull(HibernateBridge.get(wrap(sessionFactoryV2)));
    }

    @Test
    public void testGetFromV5() {
        assertNotNull(HibernateBridge.get(sessionFactoryV5));
        assertNotNull(HibernateBridge.get(wrap(sessionFactoryV5)));
    }

    @Test
    public void testGetV2SessionFactory() {
        assertEquals("SessionFactoryV2BridgeProxy", hibernateBridge.getV2SessionFactory().getClass().getSimpleName());
    }

    @Test
    public void testGetV5SessionFactory() {
        assertEquals("SessionFactoryV5BridgeProxy", hibernateBridge.getV5SessionFactory().getClass().getSimpleName());
    }

    @Test
    public void testGetV2orV5SessionFactory() {
        assertEquals("SessionFactoryV2orV5BridgeProxy", hibernateBridge.getV2orV5SessionFactory().getClass().getSimpleName());
    }

    @Test
    public void testUpgradeDowngradeReturnsSameSessionV2Reference() throws Exception {
        bridgeMode = HibernateBridgeMode.V5_ADAPTER;
        net.sf.hibernate.Session sessionV2 = hibernateBridge.getV2orV5SessionFactory().openSession();
        net.sf.hibernate.Session upgradeDowngrade = hibernateBridge.getV2orV5Session(hibernateBridge.getV5Session(sessionV2));
        assertEquals(
                "getV2orV5Session must return the original Session v2 reference so that SessionFactoryUtils.releaseSession works correctly",
                true,
                sessionV2 == upgradeDowngrade);
    }

    @Test
    public void testGetV2Session() throws Exception {
        bridgeMode = HibernateBridgeMode.V2_WITH_SESSION_BRIDGE;
        assertEquals("SessionV2BridgeProxy", hibernateBridge.getV2Session(sessionFactoryV2.openSession()).getClass().getSimpleName());
        assertEquals("SessionV2BridgeProxy", hibernateBridge.getV2Session(sessionFactoryV5.openSession()).getClass().getSimpleName());
        assertEquals("SessionV2BridgeProxy", hibernateBridge.getV2Session(wrap(sessionFactoryV2.openSession())).getClass().getSimpleName());
        assertEquals("SessionV2BridgeProxy", hibernateBridge.getV2Session(wrap(sessionFactoryV5.openSession())).getClass().getSimpleName());

        bridgeMode = HibernateBridgeMode.V5_ADAPTER_WITH_SESSION_BRIDGE;
        assertEquals("SessionV2BridgeProxy", hibernateBridge.getV2Session(sessionFactoryV2.openSession()).getClass().getSimpleName());
        assertEquals("SessionV2BridgeProxy", hibernateBridge.getV2Session(sessionFactoryV5.openSession()).getClass().getSimpleName());
        assertEquals("SessionV2BridgeProxy", hibernateBridge.getV2Session(wrap(sessionFactoryV2.openSession())).getClass().getSimpleName());
        assertEquals("SessionV2BridgeProxy", hibernateBridge.getV2Session(wrap(sessionFactoryV5.openSession())).getClass().getSimpleName());
    }

    @Test
    public void testGetV2SessionFromRealV5Session() throws Exception {
        bridgeMode = HibernateBridgeMode.V2_WITH_SESSION_BRIDGE;
        testGetV2SessionFromRealV5SessionImpl();

        bridgeMode = HibernateBridgeMode.V5_ADAPTER_WITH_SESSION_BRIDGE;
        testGetV2SessionFromRealV5SessionImpl();
    }

    private void testGetV2SessionFromRealV5SessionImpl() throws Exception {
        final org.hibernate.Session session = sessionFactoryV5.openSession();
        final org.hibernate.internal.SessionImpl realSession = (org.hibernate.internal.SessionImpl)
                SessionBridgeAssociations.getSessionBridge(session).getRealV5SessionNoCreate();
        assertEquals("SessionV2BridgeProxy", hibernateBridge.getV2Session(realSession).getClass().getSimpleName());
    }

    @Test
    public void testGetV5Session() throws Exception {
        bridgeMode = HibernateBridgeMode.V2_WITH_SESSION_BRIDGE;
        assertEquals("SessionV5BridgeProxy", hibernateBridge.getV5Session(sessionFactoryV2.openSession()).getClass().getSimpleName());
        assertEquals("SessionV5BridgeProxy", hibernateBridge.getV5Session(wrap(sessionFactoryV2.openSession())).getClass().getSimpleName());

        bridgeMode = HibernateBridgeMode.V5_ADAPTER_WITH_SESSION_BRIDGE;
        assertEquals("SessionV5BridgeProxy", hibernateBridge.getV5Session(sessionFactoryV2.openSession()).getClass().getSimpleName());
        assertEquals("SessionV5BridgeProxy", hibernateBridge.getV5Session(wrap(sessionFactoryV2.openSession())).getClass().getSimpleName());
    }

    @SuppressWarnings("unchecked")
    private static net.sf.hibernate.SessionFactory wrap(final net.sf.hibernate.SessionFactory session) {
        return wrap(new Class[] { net.sf.hibernate.engine.SessionFactoryImplementor.class }, session);
    }

    @SuppressWarnings("unchecked")
    private static org.hibernate.SessionFactory wrap(final org.hibernate.SessionFactory session) {
        return wrap(new Class[] { org.hibernate.engine.spi.SessionFactoryImplementor.class }, session);
    }

    @SuppressWarnings("unchecked")
    private static net.sf.hibernate.Session wrap(final net.sf.hibernate.Session session) {
        return wrap(new Class[] { net.sf.hibernate.Session.class, net.sf.hibernate.engine.SessionImplementor.class }, session);
    }

    @SuppressWarnings("unchecked")
    private static org.hibernate.Session wrap(final org.hibernate.Session session) {
        return wrap(new Class[] { org.hibernate.Session.class, org.hibernate.engine.spi.SessionImplementor.class }, session);
    }

    /**
     * Simulate an interface being wrapped by a java or Spring proxy.
     */
    @SuppressWarnings("unchecked")
    private static <T> T wrap(final Class[] interfaceClasses, final T obj) {
        final InvocationHandler handler = (proxy, method, args) -> method.invoke(obj, args);
        return (T) Proxy.newProxyInstance(interfaceClasses[0].getClassLoader(), interfaceClasses, handler);
    }
}
