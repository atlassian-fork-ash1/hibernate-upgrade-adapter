package com.atlassian.hibernate.test;

import net.sf.hibernate.cache.Cache;
import net.sf.hibernate.cache.CacheException;
import net.sf.hibernate.cache.HashtableCache;
import net.sf.hibernate.cache.HashtableCacheProvider;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * A HashtableCacheProvider whereby the buildCache may be called multiple times and return
 * the same HashtableCache object for the same region name.
 */
public class BridgableHashtableCacheProvider extends HashtableCacheProvider {
    private final Map<String, HashtableCache> map = new HashMap<>();

    public Cache buildCache(final String regionName, final Properties properties)
            throws CacheException {

        HashtableCache cache = this.map.get(regionName);
        if (cache == null) {
            cache = new HashtableCache();
            this.map.put(regionName, cache);
        }
        return cache;
    }
}
