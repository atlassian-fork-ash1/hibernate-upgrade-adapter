/*
 * Hibernate, Relational Persistence for Idiomatic Java
 *
 * Copyright (c) 2010, Red Hat Inc. or third-party contributors as
 * indicated by the @author tags or express copyright attribution
 * statements applied by the authors.  All third-party contributions are
 * distributed under license by Red Hat Inc.
 *
 * This copyrighted material is made available to anyone wishing to use, modify,
 * copy, or redistribute it subject to the terms and conditions of the GNU
 * Lesser General Public License, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this distribution; if not, write to:
 * Free Software Foundation, Inc.
 * 51 Franklin Street, Fifth Floor
 * Boston, MA  02110-1301  USA
 */
package net.sf.hibernate.dialect;

import java.sql.SQLException;
import java.sql.Types;

import net.sf.hibernate.Hibernate;
import net.sf.hibernate.cfg.Environment;
import net.sf.hibernate.exception.TemplatedViolatedConstraintNameExtracter;
import net.sf.hibernate.exception.ViolatedConstraintNameExtracter;

/**
 * A dialect compatible with the H2 database.
 *
 * alex Backported from hibernate 3.6 1555ba9, only for 1.4+
 *
 * @author Thomas Mueller
 */
public class H2Dialect extends Dialect {

	private String querySequenceString;

	public H2Dialect() {
		super();

		querySequenceString = "select sequence_name from information_schema.sequences";

		registerColumnType( Types.BOOLEAN, "boolean" );
		registerColumnType( Types.BIGINT, "bigint" );
		registerColumnType( Types.BINARY, "binary" );
		registerColumnType( Types.BIT, "boolean" );
		registerColumnType( Types.CHAR, "char($l)" );
		registerColumnType( Types.DATE, "date" );
		registerColumnType( Types.DECIMAL, "decimal($p,$s)" );
		registerColumnType( Types.NUMERIC, "decimal($p,$s)" );
		registerColumnType( Types.DOUBLE, "double" );
		registerColumnType( Types.FLOAT, "float" );
		registerColumnType( Types.INTEGER, "integer" );
		registerColumnType( Types.LONGVARBINARY, "longvarbinary" );
		registerColumnType( Types.LONGVARCHAR, "longvarchar" );
		registerColumnType( Types.REAL, "real" );
		registerColumnType( Types.SMALLINT, "smallint" );
		registerColumnType( Types.TINYINT, "tinyint" );
		registerColumnType( Types.TIME, "time" );
		registerColumnType( Types.TIMESTAMP, "timestamp" );
		registerColumnType( Types.VARCHAR, "varchar($l)" );
		registerColumnType( Types.VARBINARY, "binary($l)" );
		registerColumnType( Types.BLOB, "blob" );
		registerColumnType( Types.CLOB, "clob" );

		// select topic, syntax from information_schema.help
		// where section like 'Function%' order by section, topic
		//
		// see also ->  http://www.h2database.com/html/functions.html

		// Numeric Functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		registerFunction( "acos", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "asin", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "atan", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "atan2", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "bitand", new StandardSQLFunction( Hibernate.INTEGER ) );
		registerFunction( "bitor", new StandardSQLFunction( Hibernate.INTEGER ) );
		registerFunction( "bitxor", new StandardSQLFunction( Hibernate.INTEGER ) );
		registerFunction( "ceiling", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "cos", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "compress", new StandardSQLFunction( Hibernate.BINARY ) );
		registerFunction( "cot", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "decrypt", new StandardSQLFunction( Hibernate.BINARY ) );
		registerFunction( "degrees", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "encrypt", new StandardSQLFunction( Hibernate.BINARY ) );
		registerFunction( "exp", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "expand", new StandardSQLFunction( Hibernate.BINARY ) );
		registerFunction( "floor", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "hash", new StandardSQLFunction( Hibernate.BINARY ) );
		registerFunction( "log", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "log10", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "pi", new NoArgSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "power", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "radians", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "rand", new NoArgSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "round", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "roundmagic", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "sign", new StandardSQLFunction( Hibernate.INTEGER ) );
		registerFunction( "sin", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "tan", new StandardSQLFunction( Hibernate.DOUBLE ) );
		registerFunction( "truncate", new StandardSQLFunction( Hibernate.DOUBLE ) );

		// String Functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		registerFunction( "ascii", new StandardSQLFunction( Hibernate.INTEGER ) );
		registerFunction( "char", new StandardSQLFunction( Hibernate.CHARACTER ) );
		registerFunction( "concat", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "difference", new StandardSQLFunction( Hibernate.INTEGER ) );
		registerFunction( "hextoraw", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "insert", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "left", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "lcase", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "ltrim", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "octet_length", new StandardSQLFunction( Hibernate.INTEGER ) );
		registerFunction( "position", new StandardSQLFunction( Hibernate.INTEGER ) );
		registerFunction( "rawtohex", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "repeat", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "replace", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "right", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "rtrim", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "soundex", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "space", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "stringencode", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "stringdecode", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "stringtoutf8", new StandardSQLFunction( Hibernate.BINARY ) );
		registerFunction( "ucase", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "utf8tostring", new StandardSQLFunction( Hibernate.STRING ) );

		// Time and Date Functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		registerFunction( "curdate", new NoArgSQLFunction( Hibernate.DATE ) );
		registerFunction( "curtime", new NoArgSQLFunction( Hibernate.TIME ) );
		registerFunction( "curtimestamp", new NoArgSQLFunction( Hibernate.TIME ) );
		registerFunction( "current_date", new NoArgSQLFunction( Hibernate.DATE ) );
		registerFunction( "current_time", new NoArgSQLFunction( Hibernate.TIME ) );
		registerFunction( "current_timestamp", new NoArgSQLFunction( Hibernate.TIMESTAMP ) );
		registerFunction( "datediff", new StandardSQLFunction( Hibernate.INTEGER ) );
		registerFunction( "dayname", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "dayofmonth", new StandardSQLFunction( Hibernate.INTEGER ) );
		registerFunction( "dayofweek", new StandardSQLFunction( Hibernate.INTEGER ) );
		registerFunction( "dayofyear", new StandardSQLFunction( Hibernate.INTEGER ) );
		registerFunction( "monthname", new StandardSQLFunction( Hibernate.STRING ) );
		registerFunction( "now", new NoArgSQLFunction( Hibernate.TIMESTAMP ) );
		registerFunction( "quarter", new StandardSQLFunction( Hibernate.INTEGER ) );
		registerFunction( "week", new StandardSQLFunction( Hibernate.INTEGER ) );

		// System Functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		registerFunction( "database", new NoArgSQLFunction( Hibernate.STRING ) );
		registerFunction( "user", new NoArgSQLFunction( Hibernate.STRING ) );

		getDefaultProperties().setProperty( Environment.STATEMENT_BATCH_SIZE, DEFAULT_BATCH_SIZE );
	}

	public String getAddColumnString() {
		return "add column";
	}

	public boolean supportsIdentityColumns() {
		return true;
	}

	public String getIdentityColumnString() {
		return "generated by default as identity"; // not null is implicit
	}

	public String getIdentitySelectString() {
		return "call identity()";
	}

	public String getIdentityInsertString() {
		return "null";
	}

	public String getForUpdateString() {
		return " for update";
	}

	public boolean supportsUnique() {
		return true;
	}

	public boolean supportsLimit() {
		return true;
	}

	public String getLimitString(String sql, boolean hasOffset) {
		return new StringBuffer( sql.length() + 20 )
				.append( sql )
				.append( hasOffset ? " limit ? offset ?" : " limit ?" )
				.toString();
	}

	public boolean bindLimitParametersInReverseOrder() {
		return true;
	}

	public boolean bindLimitParametersFirst() {
		return false;
	}

	public boolean supportsIfExistsAfterTableName() {
		return true;
	}

	public boolean supportsSequences() {
		return true;
	}

	public boolean supportsPooledSequences() {
		return true;
	}

	public String getCreateSequenceString(String sequenceName) {
		return "create sequence " + sequenceName;
	}

	public String getDropSequenceString(String sequenceName) {
		return "drop sequence " + sequenceName;
	}

	public String getSelectSequenceNextValString(String sequenceName) {
		return "next value for " + sequenceName;
	}

	public String getSequenceNextValString(String sequenceName) {
		return "call next value for " + sequenceName;
	}

	public String getQuerySequencesString() {
		return querySequenceString;
	}

	public ViolatedConstraintNameExtracter getViolatedConstraintNameExtracter() {
		return EXTRACTER;
	}

	private static ViolatedConstraintNameExtracter EXTRACTER = new TemplatedViolatedConstraintNameExtracter() {
		/**
		 * Extract the name of the violated constraint from the given SQLException.
		 *
		 * @param sqle The exception that was the result of the constraint violation.
		 * @return The extracted constraint name.
		 */
		public String extractConstraintName(SQLException sqle) {
			String constraintName = null;
			// 23000: Check constraint violation: {0}
			// 23001: Unique index or primary key violation: {0}
			if ( sqle.getSQLState().startsWith( "23" ) ) {
				final String message = sqle.getMessage();
				int idx = message.indexOf( "violation: " );
				if ( idx > 0 ) {
					constraintName = message.substring( idx + "violation: ".length() );
				}
			}
			return constraintName;
		}
	};
}
