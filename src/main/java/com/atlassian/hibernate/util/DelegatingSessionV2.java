package com.atlassian.hibernate.util;

import net.sf.hibernate.Criteria;
import net.sf.hibernate.FlushMode;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.LockMode;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.Query;
import net.sf.hibernate.ReplicationMode;
import net.sf.hibernate.ScrollableResults;
import net.sf.hibernate.SessionFactory;
import net.sf.hibernate.Transaction;
import net.sf.hibernate.collection.ArrayHolder;
import net.sf.hibernate.collection.CollectionPersister;
import net.sf.hibernate.collection.PersistentCollection;
import net.sf.hibernate.engine.Batcher;
import net.sf.hibernate.engine.Key;
import net.sf.hibernate.engine.QueryParameters;
import net.sf.hibernate.engine.SessionFactoryImplementor;
import net.sf.hibernate.engine.SessionImplementor;
import net.sf.hibernate.persister.ClassPersister;
import net.sf.hibernate.type.Type;
import org.apache.commons.lang3.NotImplementedException;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Base class for v2 Session implementations that wish to implement only parts of that contract
 * themselves while forwarding other method invocations to a delegate instance.
 */
public abstract class DelegatingSessionV2 implements SessionImplementor {
    private SessionImplementor session;

    protected DelegatingSessionV2() {
    }

    protected DelegatingSessionV2(final SessionImplementor session) {
        this.session = session;
    }

    /**
     * Get or create the wrapped Session object.
     */
    protected SessionImplementor getSession() throws HibernateException {
        if (session == null) {
            session = createSession();
        }
        return session;
    }

    protected SessionImplementor getSessionNoCreate() {
        return session;
    }

    protected boolean hasSession() {
        return session != null;
    }

    /**
     * Create the wrapped Session object.
     */
    protected SessionImplementor createSession() throws HibernateException {
        throw new NotImplementedException("Override createSession() or pass a session into the constructor");
    }

    @Override
    public Serializable getLoadedCollectionKey(final PersistentCollection collection) {
        try {
            return getSession().getLoadedCollectionKey(collection);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public Serializable getSnapshot(final PersistentCollection collection) {
        try {
            return getSession().getSnapshot(collection);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public ArrayHolder getArrayHolder(final Object array) {
        try {
            return getSession().getArrayHolder(array);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public void addArrayHolder(final ArrayHolder holder) {
        try {
            getSession().addArrayHolder(holder);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public void initializeCollection(final PersistentCollection collection, final boolean writing) throws HibernateException {
        getSession().initializeCollection(collection, writing);
    }

    @Override
    public boolean isInverseCollection(final PersistentCollection collection) {
        try {
            return getSession().isInverseCollection(collection);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public PersistentCollection getLoadingCollection(final CollectionPersister persister, final Serializable id, final Object resultSetId)
            throws HibernateException {
        return getSession().getLoadingCollection(persister, id, resultSetId);
    }

    @Override
    public void endLoadingCollections(final CollectionPersister persister, final Object resultSetId)
            throws HibernateException {
        getSession().endLoadingCollections(persister, resultSetId);
    }

    @Override
    public void afterLoad() {
        try {
            getSession().afterLoad();
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public void beforeLoad() {
        try {
            getSession().beforeLoad();
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public void initializeNonLazyCollections() throws HibernateException {
        getSession().initializeNonLazyCollections();
    }

    @Override
    public Object getCollection(final String role, final Serializable id, final Object owner) throws HibernateException {
        return getSession().getCollection(role, id, owner);
    }

    @Override
    public Object internalLoad(final Class persistentClass, final Serializable id) throws HibernateException {
        return getSession().internalLoad(persistentClass, id);
    }

    @Override
    public Object internalLoadOneToOne(final Class persistentClass, final Serializable id) throws HibernateException {
        return getSession().internalLoadOneToOne(persistentClass, id);
    }

    @Override
    public Object immediateLoad(final Class persistentClass, final Serializable id) throws HibernateException {
        return getSession().immediateLoad(persistentClass, id);
    }

    @Override
    public Object loadByUniqueKey(final Class persistentClass, final String uniqueKeyPropertyName, final Serializable id)
            throws HibernateException {
        return getSession().loadByUniqueKey(persistentClass, uniqueKeyPropertyName, id);
    }

    @Override
    public long getTimestamp() {
        try {
            return getSession().getTimestamp();
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public SessionFactoryImplementor getFactory() {
        try {
            return getSession().getFactory();
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public Batcher getBatcher() {
        try {
            return getSession().getBatcher();
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public void postInsert(final Object object) {
        try {
            getSession().postInsert(object);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public void postDelete(final Object object) {
        try {
            getSession().postDelete(object);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public void postUpdate(final Object object, final Object[] updatedState, final Object nextVersion) throws HibernateException {
        getSession().postUpdate(object, updatedState, nextVersion);
    }

    @Override
    public List find(final String query, final QueryParameters queryParameters) throws HibernateException {
        return getSession().find(query, queryParameters);
    }

    @Override
    public Iterator iterate(final String query, final QueryParameters queryParameters) throws HibernateException {
        return getSession().iterate(query, queryParameters);
    }

    @Override
    public ScrollableResults scroll(final String query, final QueryParameters queryParameters) throws HibernateException {
        return getSession().scroll(query, queryParameters);
    }

    @Override
    public List filter(final Object collection, final String filter, final QueryParameters queryParameters)
            throws HibernateException {
        return getSession().filter(collection, filter, queryParameters);
    }

    @Override
    public Iterator iterateFilter(final Object collection, final String filter, final QueryParameters queryParameters)
            throws HibernateException {
        return getSession().iterateFilter(collection, filter, queryParameters);
    }

    @Override
    public ClassPersister getPersister(final Object object) throws MappingException {
        try {
            return getSession().getPersister(object);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public void addUninitializedEntity(final Key key, final Object object, final LockMode lockMode) {
        try {
            getSession().addUninitializedEntity(key, object, lockMode);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public void postHydrate(final ClassPersister persister, final Serializable id, final Object[] values, final Object object, final LockMode lockMode)
            throws HibernateException {
        getSession().postHydrate(persister, id, values, object, lockMode);
    }

    @Override
    public void initializeEntity(final Object object) throws HibernateException {
        getSession().initializeEntity(object);
    }

    @Override
    public Object getEntity(final Key key) {
        try {
            return getSession().getEntity(key);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public Object proxyFor(final ClassPersister persister, final Key key, final Object impl) throws HibernateException {
        return getSession().proxyFor(persister, key, impl);
    }

    @Override
    public Object proxyFor(final Object impl) throws HibernateException {
        return getSession().proxyFor(impl);
    }

    @Override
    public void afterTransactionCompletion(final boolean successful) {
        try {
            getSession().afterTransactionCompletion(successful);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public Serializable getEntityIdentifier(final Object obj) {
        try {
            return getSession().getEntityIdentifier(obj);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public Serializable getEntityIdentifierIfNotUnsaved(final Object object) throws HibernateException {
        return getSession().getEntityIdentifierIfNotUnsaved(object);
    }

    @Override
    public boolean isSaved(final Object object) throws HibernateException {
        return getSession().isSaved(object);
    }

    @Override
    public Object instantiate(final Class clazz, final Serializable id) throws HibernateException {
        return getSession().instantiate(clazz, id);
    }

    @Override
    public void setLockMode(final Object entity, final LockMode lockMode) {
        try {
            getSession().setLockMode(entity, lockMode);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public Object getVersion(final Object entity) {
        try {
            return getSession().getVersion(entity);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public LockMode getLockMode(final Object object) {
        try {
            return getSession().getLockMode(object);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public Collection getOrphans(final PersistentCollection coll) throws HibernateException {
        return getSession().getOrphans(coll);
    }

    @Override
    public Serializable[] getCollectionBatch(final CollectionPersister collectionPersister, final Serializable id, final int batchSize) {
        try {
            return getSession().getCollectionBatch(collectionPersister, id, batchSize);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public Serializable[] getClassBatch(final Class clazz, final Serializable id, final int batchSize) {
        try {
            return getSession().getClassBatch(clazz, id, batchSize);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public void scheduleBatchLoad(final Class clazz, final Serializable id) throws MappingException {
        try {
            getSession().scheduleBatchLoad(clazz, id);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public List findBySQL(final String sqlQuery, final String[] aliases, final Class[] classes, final QueryParameters queryParameters, final Collection querySpaces)
            throws HibernateException {
        return getSession().findBySQL(sqlQuery, aliases, classes, queryParameters, querySpaces);
    }

    @Override
    public void addNonExist(final Key key) {
        try {
            getSession().addNonExist(key);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public Object copy(final Object object, final Map copiedAlready) throws HibernateException {
        return getSession().copy(object, copiedAlready);
    }

    @Override
    public Object getCollectionOwner(final Serializable key, final CollectionPersister collectionPersister)
            throws MappingException {
        try {
            return getSession().getCollectionOwner(key, collectionPersister);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public void flush() throws HibernateException {
        getSession().flush();
    }

    @Override
    public FlushMode getFlushMode() {
        try {
            return getSession().getFlushMode();
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public void setFlushMode(final FlushMode flushMode) {
        try {
            getSession().setFlushMode(flushMode);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public SessionFactory getSessionFactory() {
        try {
            return getSession().getSessionFactory();
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public Connection connection() throws HibernateException {
        return getSession().connection();
    }

    @Override
    public Connection disconnect() throws HibernateException {
        return getSession().disconnect();
    }

    @Override
    public void reconnect() throws HibernateException {
        getSession().reconnect();
    }

    @Override
    public void reconnect(final Connection connection) throws HibernateException {
        getSession().reconnect(connection);
    }

    @Override
    public Connection close() throws HibernateException {
        return getSession().close();
    }

    @Override
    public void cancelQuery() throws HibernateException {
        getSession().cancelQuery();
    }

    @Override
    public boolean isOpen() {
        try {
            return getSession().isOpen();
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public boolean isConnected() {
        try {
            return getSession().isConnected();
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public boolean isDirty() throws HibernateException {
        return getSession().isDirty();
    }

    @Override
    public Serializable getIdentifier(final Object object) throws HibernateException {
        return getSession().getIdentifier(object);
    }

    @Override
    public boolean contains(final Object object) {
        try {
            return getSession().contains(object);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public void evict(final Object object) throws HibernateException {
        getSession().evict(object);
    }

    @Override
    public Object load(final Class theClass, final Serializable id, final LockMode lockMode) throws HibernateException {
        return getSession().load(theClass, id, lockMode);
    }

    @Override
    public Object load(final Class theClass, final Serializable id) throws HibernateException {
        return getSession().load(theClass, id);
    }

    @Override
    public void load(final Object object, final Serializable id) throws HibernateException {
        getSession().load(object, id);
    }

    @Override
    public void replicate(final Object object, final ReplicationMode replicationMode) throws HibernateException {
        getSession().replicate(object, replicationMode);
    }

    @Override
    public Serializable save(final Object object) throws HibernateException {
        return getSession().save(object);
    }

    @Override
    public void save(final Object object, final Serializable id) throws HibernateException {
        getSession().save(object, id);
    }

    @Override
    public void saveOrUpdate(final Object object) throws HibernateException {
        getSession().saveOrUpdate(object);
    }

    @Override
    public void update(final Object object) throws HibernateException {
        getSession().update(object);
    }

    @Override
    public void update(final Object object, final Serializable id) throws HibernateException {
        getSession().update(object, id);
    }

    @Override
    public Object saveOrUpdateCopy(final Object object) throws HibernateException {
        return getSession().saveOrUpdateCopy(object);
    }

    @Override
    public Object saveOrUpdateCopy(final Object object, final Serializable id) throws HibernateException {
        return getSession().saveOrUpdateCopy(object, id);
    }

    @Override
    public void delete(final Object object) throws HibernateException {
        getSession().delete(object);
    }

    @Override
    public List find(final String query) throws HibernateException {
        return getSession().find(query);
    }

    @Override
    public List find(final String query, final Object value, final Type type) throws HibernateException {
        return getSession().find(query, value, type);
    }

    @Override
    public List find(final String query, final Object[] values, final Type[] types) throws HibernateException {
        return getSession().find(query, values, types);
    }

    @Override
    public Iterator iterate(final String query) throws HibernateException {
        return getSession().iterate(query);
    }

    @Override
    public Iterator iterate(final String query, final Object value, final Type type) throws HibernateException {
        return getSession().iterate(query, value, type);
    }

    @Override
    public Iterator iterate(final String query, final Object[] values, final Type[] types) throws HibernateException {
        return getSession().iterate(query, values, types);
    }

    @Override
    public Collection filter(final Object collection, final String filter) throws HibernateException {
        try {
            return getSession().filter(collection, filter);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public Collection filter(final Object collection, final String filter, final Object value, final Type type)
            throws HibernateException {
        return getSession().filter(collection, filter, value, type);
    }

    @Override
    public Collection filter(final Object collection, final String filter, final Object[] values, final Type[] types)
            throws HibernateException {
        return getSession().filter(collection, filter, values, types);
    }

    @Override
    public int delete(final String query) throws HibernateException {
        return getSession().delete(query);
    }

    @Override
    public int delete(final String query, final Object value, final Type type) throws HibernateException {
        return getSession().delete(query, value, type);
    }

    @Override
    public int delete(final String query, final Object[] values, final Type[] types) throws HibernateException {
        return getSession().delete(query, values, types);
    }

    @Override
    public void lock(final Object object, final LockMode lockMode) throws HibernateException {
        getSession().lock(object, lockMode);
    }

    @Override
    public void refresh(final Object object) throws HibernateException {
        getSession().refresh(object);
    }

    @Override
    public void refresh(final Object object, final LockMode lockMode) throws HibernateException {
        getSession().refresh(object, lockMode);
    }

    @Override
    public LockMode getCurrentLockMode(final Object object) throws HibernateException {
        return getSession().getCurrentLockMode(object);
    }

    @Override
    public Transaction beginTransaction() throws HibernateException {
        return getSession().beginTransaction();
    }

    @Override
    public Criteria createCriteria(final Class persistentClass) {
        try {
            return getSession().createCriteria(persistentClass);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public Query createQuery(final String queryString) throws HibernateException {
        return getSession().createQuery(queryString);
    }

    @Override
    public Query createFilter(final Object collection, final String queryString) throws HibernateException {
        return getSession().createFilter(collection, queryString);
    }

    @Override
    public Query getNamedQuery(final String queryName) throws HibernateException {
        return getSession().getNamedQuery(queryName);
    }

    @Override
    public Query createSQLQuery(final String sql, final String returnAlias, final Class returnClass) {
        try {
            return getSession().createSQLQuery(sql, returnAlias, returnClass);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public Query createSQLQuery(final String sql, final String[] returnAliases, final Class[] returnClasses) {
        try {
            return getSession().createSQLQuery(sql, returnAliases, returnClasses);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public void clear() {
        try {
            getSession().clear();
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Override
    public Object get(final Class clazz, final Serializable id) throws HibernateException {
        return getSession().get(clazz, id);
    }

    @Override
    public Object get(final Class clazz, final Serializable id, final LockMode lockMode) throws HibernateException {
        return getSession().get(clazz, id, lockMode);
    }
}
