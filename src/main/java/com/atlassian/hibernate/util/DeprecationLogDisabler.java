package com.atlassian.hibernate.util;

import org.hibernate.internal.log.DeprecationLogger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Modifier;
import java.lang.reflect.Proxy;
import java.util.HashSet;
import java.util.Set;

/**
 * Disables logging of deprecated features per DeprecationLogger method.
 */
public final class DeprecationLogDisabler {
    private DeprecationLogDisabler() { }

    private static Set<String> disabledLoggerMethods = new HashSet<>();
    private static boolean loggerIntercepted;

    /**
     * Disable logging of a DeprecationLogger method.
     * @param methodName The name of the method on the DeprecationLogger interface.
     */
    public static synchronized void disableLogging(final String methodName, final boolean throwOnError) {
        interceptLogger(throwOnError);
        disabledLoggerMethods.add(methodName);
    }

    private static synchronized void interceptLogger(final boolean throwOnError) {
        if (loggerIntercepted) {
            return;
        }

        // intercept DeprecationLogger interface, filtering out disabled method calls
        DeprecationLogger original = DeprecationLogger.DEPRECATION_LOGGER;
        InvocationHandler handler = (proxy, method, args) -> {
            if (!disabledLoggerMethods.contains(method.getName())) {
                return method.invoke(original, args);
            }
            return null;
        };
        DeprecationLogger intercepted = (DeprecationLogger) Proxy.newProxyInstance(
                DeprecationLogDisabler.class.getClassLoader(), new Class[] {DeprecationLogger.class}, handler);

        // assign the intercepted DeprecationLogger
        try {
            Field field = getAccessibleNonFinalField(DeprecationLogger.class, "DEPRECATION_LOGGER");
            field.set(null, intercepted);
        } catch (Throwable ex) {
            if (throwOnError) {
                ThrowableUtil.propagateAll(ex);
            }
        }

        loggerIntercepted = true;
    }

    /**
     * Get a private field that can be set, even if marked as final.
     */
    private static Field getAccessibleNonFinalField(final Class clazz, final String fieldName)
            throws ReflectiveOperationException {

        Field field = clazz.getField(fieldName);
        field.setAccessible(true);

        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        return field;
    }
}
