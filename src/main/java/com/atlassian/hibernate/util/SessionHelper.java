package com.atlassian.hibernate.util;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.event.spi.EventSource;
import org.hibernate.event.spi.EventType;
import org.hibernate.event.spi.FlushEvent;
import org.hibernate.event.spi.FlushEventListener;
import org.hibernate.event.spi.SaveOrUpdateEvent;
import org.hibernate.event.spi.SaveOrUpdateEventListener;
import org.hibernate.internal.SessionImpl;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides utility methods for invoking Session objects using hibernate v2 method signatures,
 * that have since been removed.
 */
public final class SessionHelper {
    private SessionHelper() { }

    /**
     * Persist the given transient instance, using the given identifier.
     *
     * @param session the Session object
     * @param object  a transient instance of a persistent class
     * @param id      an unused valid identifier
     * @throws org.hibernate.HibernateException when the entity could not be saved
     */
    public static void save(final Session session, final Object object, final Serializable id)
            throws HibernateException {

        if (!(session instanceof SessionImpl)) {
            throw new IllegalArgumentException(
                    "Save with identifier requires a real Session object (org.hibernate.internal.SessionImpl)");
        }

        if (object != null && id != null) {
            fireSave(session, new SaveOrUpdateEvent(null, object, id, (org.hibernate.event.spi.EventSource) session));
        } else {
            session.save(object);
        }
    }

    private static Serializable fireSave(final Session session, final SaveOrUpdateEvent event) {
        final SessionImplementor sessionImplementor = (SessionImplementor) session;

        errorIfClosed(sessionImplementor);
        checkNoUnresolvedActionsBeforeOperation(sessionImplementor);
        for (final SaveOrUpdateEventListener listener : eventListenerGroup(session, EventType.SAVE).listeners()) {
            listener.onSaveOrUpdate(event);
        }
        checkNoUnresolvedActionsAfterOperation(sessionImplementor);
        return event.getResultId();
    }

    /**
     * Force the session to flush, but don't throw an exception if performed outside of a transaction.
     *
     * @param session the Session object
     * @throws org.hibernate.HibernateException Indicates problems flushing the session or talking to the database
     */
    public static void flushAllowNoTransaction(final Session session) throws HibernateException {
        SessionImplementor sessionImplementor = (SessionImplementor) session;
        errorIfClosed(sessionImplementor);
        try {
            if (sessionImplementor.getPersistenceContext().getCascadeLevel() > 0) {
                throw new HibernateException("Flush during cascade is dangerous");
            }

            FlushEvent flushEvent = new FlushEvent((EventSource) sessionImplementor);
            for (final FlushEventListener listener : eventListenerGroup(session, EventType.FLUSH).listeners()) {
                listener.onFlush(flushEvent);
            }
        } catch (RuntimeException e) {
            throw sessionImplementor.getExceptionConverter().convert(e);
        }
    }

    private static void errorIfClosed(final SessionImplementor sessionImplementor) {
        if (sessionImplementor.isClosed()) {
            throw new org.hibernate.SessionException("Session is closed!");
        }
    }

    private static void checkNoUnresolvedActionsBeforeOperation(final SessionImplementor sessionImplementor) {
        final org.hibernate.event.spi.EventSource eventSource = (org.hibernate.event.spi.EventSource) sessionImplementor;
        if (sessionImplementor.getPersistenceContext().getCascadeLevel() == 0
                && eventSource.getActionQueue().hasUnresolvedEntityInsertActions()) {
            throw new IllegalStateException("There are delayed insert actions before operation as cascade level 0.");
        }
    }

    private static void checkNoUnresolvedActionsAfterOperation(final SessionImplementor sessionImplementor) {
        final org.hibernate.event.spi.EventSource eventSource = (org.hibernate.event.spi.EventSource) sessionImplementor;
        if (sessionImplementor.getPersistenceContext().getCascadeLevel() == 0) {
            eventSource.getActionQueue().checkNoUnresolvedActionsAfterOperation();
        }
    }

    private static <T> org.hibernate.event.service.spi.EventListenerGroup<T> eventListenerGroup(
            final Session session,
            final org.hibernate.event.spi.EventType<T> type) {

        final org.hibernate.engine.spi.SessionFactoryImplementor sessionFactoryImpl =
                (org.hibernate.engine.spi.SessionFactoryImplementor) session.getSessionFactory();
        return sessionFactoryImpl.getServiceRegistry().getService(org.hibernate.event.service.spi.EventListenerRegistry.class)
                .getEventListenerGroup(type);
    }

    /**
     * Delete all objects returned by the query. Return the number of objects deleted.
     *
     * @param session the Session object
     * @param query   the query string
     * @param values  a list of values to be written to parameters in the query.
     * @param types   a list of Hibernate types of the values
     * @return the number of instances deleted
     * @throws HibernateException when the entities could not be deleted
     */
    public static int delete(final Session session, final String query, final Object[] values, final Type[] types)
            throws HibernateException {

        final List list = createQuery(session, query, values, types).list();
        final int size = list.size();
        for (int i = 0; i < size; i++) {
            session.delete(list.get(i));
        }
        return size;
    }

    /**
     * Create a new instance of <tt>Query</tt> for the given query string and bind parameter.
     * <p>
     * Bind a value to a parameter in the query string.
     *
     * @param session the Session object
     * @param queryString the query string
     * @param value   a value to be bound to a placeholder (JDBC IN parameter).
     * @param type    the Hibernate type of the value
     * @return Query
     * @throws HibernateException when the query could not be created
     */
    public static Query createQuery(final Session session, final String queryString, final Object value, final Type type)
            throws HibernateException {

        return createQuery(session, queryString, new Object[]{value}, new Type[]{type});
    }

    /**
     * Create a new instance of <tt>Query</tt> for the given query string and bind parameters.
     * <p>
     * Binding an array of values to parameters in the query string.
     *
     * @param session     the Session object
     * @param queryString the query string
     * @param values      an array of values to be bound to the parameters
     * @param types       an array of Hibernate types of the values
     * @return Query
     * @throws HibernateException when the query could not be created
     */
    public static Query createQuery(
            final Session session,
            final String queryString,
            final Object[] values,
            final Type[] types) throws HibernateException {

        if (values == null) {
            throw new IllegalArgumentException("values is null");
        }
        if (types == null) {
            throw new IllegalArgumentException("types is null");
        }
        if (values.length != types.length) {
            throw new IllegalArgumentException("values.length != types.length");
        }

        final Query query = session.createQuery(queryString);
        final List<String> parameterNames = findQueryParameterNames(queryString);
        if (parameterNames.size() != 0 && parameterNames.size() != values.length) {
            throw new IllegalArgumentException("parameter-names.length != values.length");
        }

        // with named parameters
        if (parameterNames.size() > 0) {
            for (int i = 0; i < values.length; i++) {
                query.setParameter(parameterNames.get(i), values[i], types[i]);
            }
        } else {
            // with only ? parameters
            for (int i = 0; i < values.length; i++) {
                query.setParameter(i, values[i], types[i]);
            }
        }
        return query;
    }

    /**
     * Find the names of a query's named parameters.
     */
    private static List<String> findQueryParameterNames(final String queryString) {
        final List<String> list = new ArrayList<>();
        int i = StringUtils.indexOfIgnoreCase(queryString, "where");

        while (i != -1) {
            final int start = queryString.indexOf(":", i);
            final int end = findParameterNameEnd(queryString, start);
            if (end != -1) {
                list.add(queryString.substring(start + 1, end));
            }
            i = end != -1 ? end : -1;
        }
        return list;
    }

    private static int findParameterNameEnd(final String queryString, final int start) {
        if (start == -1) {
            return -1;
        }

        for (int i = start + 1; i < queryString.length(); i++) {
            final char c = queryString.charAt(i);
            if (!Character.isLetterOrDigit(c) && c != '_') {
                return i;
            }
        }
        return queryString.length();
    }

    /**
     * Create a new instance of <tt>Query</tt> for the given SQL string.
     *
     * @param sql           a query expressed in SQL
     * @param returnAliases an array of table aliases that appear inside <tt>{}</tt> in the SQL string
     * @param returnClasses the returned persistent classes
     */
    public static NativeQuery createNativeQuery(
            final Session session,
            final String sql,
            final String[] returnAliases,
            final Class[] returnClasses) {

        NativeQuery query = session.createNativeQuery(sql);
        for (int i = 0; i < returnAliases.length; i++) {
            query = query.addEntity(returnAliases[i], returnClasses[i]);
        }
        return query;
    }
}
