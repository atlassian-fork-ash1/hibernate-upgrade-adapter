package com.atlassian.hibernate.util.reflection;

import com.atlassian.hibernate.util.ThrowableUtil;
import sun.misc.Unsafe;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Helper methods for using java reflection efficiently.
 * This implementation is faster, but only works in the presence of com.sun.Unsafe.
 * If instantiation of this class fails due to use of com.sun.Unsafe, ReflectionHelper
 * may be used safely in it's place.
 */
@SuppressWarnings("checkstyle:illegalimport")
class UnsafeReflectionHelper extends ReflectionHelper {

    private Unsafe unsafe = null;

    public UnsafeReflectionHelper() throws SecurityException {
        unsafe = Unsafe.getUnsafe();
    }

    /**
     * Get a function that returns the value of a private field, without throwing checked exceptions.
     */
    @Override
    public Function<Object, Object> getPrivateFieldGetter(final Class clazz, final String fieldName) {
        try {
            final Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);

            // try to make a fast accessor (with less reflection overhead)
            if (unsafe != null
                    && !Modifier.isStatic(field.getModifiers())
                    && !Modifier.isVolatile(field.getModifiers())) {

                // optimize for primitives
                final Unsafe unsafe = this.unsafe;
                final long fieldOffset = unsafe.objectFieldOffset(field);
                if (field.getType().equals(int.class))
                    return obj -> unsafe.getInt(obj, fieldOffset);
                if (field.getType().equals(boolean.class))
                    return obj -> unsafe.getBoolean(obj, fieldOffset);
                if (!field.getType().isPrimitive())
                    return obj -> unsafe.getObject(obj, fieldOffset);
            }
            return super.getPrivateFieldGetter(clazz, fieldName);
        } catch (NoSuchFieldException | SecurityException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    /**
     * Get a function that sets the value of a private field, without throwing checked exceptions.
     */
    @Override
    public BiConsumer<Object, Object> getPrivateFieldSetter(final Class clazz, final String fieldName) {
        try {
            final Field field = clazz.getDeclaredField(fieldName);
            field.setAccessible(true);

            // try to make a fast accessor (with less reflection overhead)
            final Unsafe unsafe = this.unsafe;
            if (unsafe != null
                    && !Modifier.isStatic(field.getModifiers())
                    && !Modifier.isVolatile(field.getModifiers())) {
                // optimize for primitives
                final long fieldOffset = unsafe.objectFieldOffset(field);
                if (field.getType().equals(long.class))
                    return (obj, value) -> unsafe.putLong(obj, fieldOffset, (long) value);
                if (!field.getType().isPrimitive())
                    return (obj, value) -> unsafe.putObject(obj, fieldOffset, value);
            }
            return super.getPrivateFieldSetter(clazz, fieldName);
        } catch (NoSuchFieldException | SecurityException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }
}
