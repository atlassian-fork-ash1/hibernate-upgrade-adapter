package com.atlassian.hibernate.util;

import net.sf.hibernate.Databinder;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Interceptor;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.Session;
import net.sf.hibernate.cache.QueryCache;
import net.sf.hibernate.collection.CollectionPersister;
import net.sf.hibernate.connection.ConnectionProvider;
import net.sf.hibernate.dialect.Dialect;
import net.sf.hibernate.engine.SessionFactoryImplementor;
import net.sf.hibernate.exception.SQLExceptionConverter;
import net.sf.hibernate.metadata.ClassMetadata;
import net.sf.hibernate.metadata.CollectionMetadata;
import net.sf.hibernate.persister.ClassPersister;
import net.sf.hibernate.type.Type;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.transaction.TransactionManager;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Map;

/**
 * Base class for v2 SessionFactory implementations that wish to implement only parts of that contract
 * themselves while forwarding other method invocations to a delegate instance.
 */
public abstract class DelegatingSessionFactoryV2 implements SessionFactoryImplementor {
    private final SessionFactoryImplementor sessionFactory;

    protected DelegatingSessionFactoryV2(final SessionFactoryImplementor sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public ClassPersister getPersister(final Class clazz) throws MappingException {
        return sessionFactory.getPersister(clazz);
    }

    @Override
    public ClassPersister getPersister(final String className) throws MappingException {
        return sessionFactory.getPersister(className);
    }

    @Override
    public CollectionPersister getCollectionPersister(final String role) throws MappingException {
        return sessionFactory.getCollectionPersister(role);
    }

    @Override
    public boolean isOuterJoinedFetchEnabled() {
        return sessionFactory.isOuterJoinedFetchEnabled();
    }

    @Override
    public boolean isScrollableResultSetsEnabled() {
        return sessionFactory.isScrollableResultSetsEnabled();
    }

    @Override
    public boolean isGetGeneratedKeysEnabled() {
        return sessionFactory.isGetGeneratedKeysEnabled();
    }

    @Override
    public String getDefaultSchema() {
        return sessionFactory.getDefaultSchema();
    }

    @Override
    public Dialect getDialect() {
        return sessionFactory.getDialect();
    }

    @Override
    public Type[] getReturnTypes(final String queryString) throws HibernateException {
        return sessionFactory.getReturnTypes(queryString);
    }

    @Override
    public ConnectionProvider getConnectionProvider() {
        return sessionFactory.getConnectionProvider();
    }

    @Override
    public String[] getImplementors(final Class clazz) {
        return sessionFactory.getImplementors(clazz);
    }

    @Override
    public String getImportedClassName(final String name) {
        return sessionFactory.getImportedClassName(name);
    }

    @Override
    public int getJdbcBatchSize() {
        return sessionFactory.getJdbcBatchSize();
    }

    @Override
    public Integer getJdbcFetchSize() {
        return sessionFactory.getJdbcFetchSize();
    }

    @Override
    public Integer getMaximumFetchDepth() {
        return sessionFactory.getMaximumFetchDepth();
    }

    @Override
    public TransactionManager getTransactionManager() {
        return sessionFactory.getTransactionManager();
    }

    @Override
    public boolean isShowSqlEnabled() {
        return sessionFactory.isShowSqlEnabled();
    }

    @Override
    public QueryCache getQueryCache() {
        return sessionFactory.getQueryCache();
    }

    @Override
    public QueryCache getQueryCache(final String regionName) throws HibernateException {
        return sessionFactory.getQueryCache(regionName);
    }

    @Override
    public boolean isQueryCacheEnabled() {
        return sessionFactory.isQueryCacheEnabled();
    }

    @Override
    public boolean isJdbcBatchVersionedData() {
        return sessionFactory.isJdbcBatchVersionedData();
    }

    @Override
    public boolean isWrapResultSetsEnabled() {
        return sessionFactory.isWrapResultSetsEnabled();
    }

    @Override
    public Type getIdentifierType(final Class persistentClass) throws MappingException {
        return sessionFactory.getIdentifierType(persistentClass);
    }

    @Override
    public String getIdentifierPropertyName(final Class persistentClass) throws MappingException {
        return sessionFactory.getIdentifierPropertyName(persistentClass);
    }

    @Override
    public Type getPropertyType(final Class persistentClass, final String propertyName) throws MappingException {
        return sessionFactory.getPropertyType(persistentClass, propertyName);
    }

    @Override
    public Session openSession(final Connection connection) {
        return sessionFactory.openSession(connection);
    }

    @Override
    public Session openSession(final Interceptor interceptor) throws HibernateException {
        return sessionFactory.openSession(interceptor);
    }

    @Override
    public Session openSession(final Connection connection, final Interceptor interceptor) {
        return sessionFactory.openSession(connection, interceptor);
    }

    @Override
    public Session openSession() throws HibernateException {
        return sessionFactory.openSession();
    }

    @Override
    public Databinder openDatabinder() throws HibernateException {
        return sessionFactory.openDatabinder();
    }

    @Override
    public ClassMetadata getClassMetadata(final Class persistentClass) throws HibernateException {
        return sessionFactory.getClassMetadata(persistentClass);
    }

    @Override
    public CollectionMetadata getCollectionMetadata(final String roleName) throws HibernateException {
        return sessionFactory.getCollectionMetadata(roleName);
    }

    @Override
    public Map getAllClassMetadata() throws HibernateException {
        return sessionFactory.getAllClassMetadata();
    }

    @Override
    public Map getAllCollectionMetadata() throws HibernateException {
        return sessionFactory.getAllCollectionMetadata();
    }

    @Override
    public void close() throws HibernateException {
        sessionFactory.close();
    }

    @Override
    public void evict(final Class persistentClass) throws HibernateException {
        sessionFactory.evict(persistentClass);
    }

    @Override
    public void evict(final Class persistentClass, final Serializable id) throws HibernateException {
        sessionFactory.evict(persistentClass, id);
    }

    @Override
    public void evictCollection(final String roleName) throws HibernateException {
        sessionFactory.evictCollection(roleName);
    }

    @Override
    public void evictCollection(final String roleName, final Serializable id) throws HibernateException {
        sessionFactory.evictCollection(roleName, id);
    }

    @Override
    public void evictQueries() throws HibernateException {
        sessionFactory.evictQueries();
    }

    @Override
    public void evictQueries(final String cacheRegion) throws HibernateException {
        sessionFactory.evictQueries(cacheRegion);
    }

    @Override
    public SQLExceptionConverter getSQLExceptionConverter() {
        return sessionFactory.getSQLExceptionConverter();
    }

    @Override
    public Reference getReference() throws NamingException {
        return sessionFactory.getReference();
    }
}
