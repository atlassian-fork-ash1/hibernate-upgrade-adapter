package com.atlassian.hibernate.util;

import com.atlassian.hibernate.util.reflection.StatefulPersistenceContextV5Reflection;
import org.hibernate.collection.spi.PersistentCollection;
import org.hibernate.engine.internal.StatefulPersistenceContext;
import org.hibernate.engine.spi.EntityKey;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.internal.util.collections.ConcurrentReferenceHashMap;
import org.hibernate.internal.util.collections.IdentityMap;

import java.util.HashMap;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;

/**
 * Manipulates StatefulPersistenceContext so that Map fields are re-creating on clear() instead of
 * calling map.clear(), for performance in cases where the context stores a large number of entities.
 */
public final class FastClearStatefulPersistenceContext {
    private static final int INIT_COLL_SIZE = 8;

    private FastClearStatefulPersistenceContext() { }

    public static void updatePersistenceContext(final SessionImplementor session) {
        updatePersistenceContext((StatefulPersistenceContext) session.getPersistenceContext());
    }

    public static void updatePersistenceContext(final StatefulPersistenceContext persistentContent) {
        // set a new value for 'arrayHolders', with clear() overridden to perform our own cleanup
        StatefulPersistenceContextV5Reflection.setArrayHolders(
                persistentContent,
                createArrayHoldersMapWithClearThunk(persistentContent));
    }

    /**
     * Create a new instance for the 'arrayHolders' field, which is the first field to be cleared inside
     * of StatefulPersistenceContext.clear().
     * <p>
     * When clear() is called, we instead call our cleanup() method which re-instantiates the map fields
     * instead of clearing them.
     */
    private static Map<Object, PersistentCollection> createArrayHoldersMapWithClearThunk(
            final StatefulPersistenceContext persistentContent) {

        return new IdentityHashMap<Object, PersistentCollection>(INIT_COLL_SIZE) {
            @Override
            public void clear() {
                cleanup(persistentContent);
            }
        };
    }

    /**
     * Re-instantiate the Map fields to avoid expensive clear() operations.
     */
    private static void cleanup(final StatefulPersistenceContext persistentContent) {

        // re-create arrayHolders with thunk
        StatefulPersistenceContextV5Reflection.setArrayHolders(
                persistentContent, createArrayHoldersMapWithClearThunk(persistentContent));

        // re-instantiate other Map objects
        StatefulPersistenceContextV5Reflection.setEntitiesByKey(
                persistentContent, new HashMap<>(INIT_COLL_SIZE));
        StatefulPersistenceContextV5Reflection.setEntitiesByUniqueKey(
                persistentContent, new HashMap<>(INIT_COLL_SIZE));
        StatefulPersistenceContextV5Reflection.setParentsByChild(
                persistentContent, new IdentityHashMap<>(INIT_COLL_SIZE));
        StatefulPersistenceContextV5Reflection.setEntitySnapshotsByKey(
                persistentContent, new HashMap<>(INIT_COLL_SIZE));
        StatefulPersistenceContextV5Reflection.setCollectionsByKey(
                persistentContent, new HashMap<>(INIT_COLL_SIZE));
        StatefulPersistenceContextV5Reflection.setCollectionEntries(
                persistentContent, IdentityMap.instantiateSequenced(INIT_COLL_SIZE));

        if (StatefulPersistenceContextV5Reflection.getUnownedCollections(persistentContent) != null) {
            StatefulPersistenceContextV5Reflection.setUnownedCollections(
                    persistentContent, new HashMap<>(INIT_COLL_SIZE));
        }

        final ConcurrentReferenceHashMap<EntityKey, Object> proxiesByKey = new ConcurrentReferenceHashMap<>(
                INIT_COLL_SIZE, .75f, 1,
                ConcurrentReferenceHashMap.ReferenceType.STRONG,
                ConcurrentReferenceHashMap.ReferenceType.WEAK,
                null);
        StatefulPersistenceContextV5Reflection.setProxiesByKey(
                persistentContent, proxiesByKey);
        StatefulPersistenceContextV5Reflection.setNullifiableEntityKeys(
                persistentContent, new HashSet<>());
    }
}
