package com.atlassian.hibernate.util;

import com.google.common.collect.Iterables;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.ImplicitConstraintNameSource;
import org.hibernate.boot.model.naming.ImplicitForeignKeyNameSource;
import org.hibernate.boot.model.naming.ImplicitNamingStrategyJpaCompliantImpl;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.Constraint;
import org.hibernate.mapping.Table;
import org.hibernate.mapping.UniqueKey;

import java.util.Iterator;
import java.util.Locale;

/**
 * A ImplicitNamingStrategy implementation that names constraints the same way as hibernate 2 for schema compatibility.
 */
public class Hibernate2PostgresImplicitNamingStrategy extends ImplicitNamingStrategyJpaCompliantImpl {
    /**
     * Update the unique key constraint names, as per hibernate 2 naming.
     */
    @SuppressWarnings("unchecked")
    public void updateUniqueKeyConstraints(final Metadata metadata) {
        for (Table table : metadata.collectTableMappings()) {
            Iterator<Column> i = table.getColumnIterator();
            while (i.hasNext()) {
                final Column column = i.next();
                if (column.isUnique()) {
                    UniqueKey uk = table.getOrCreateUniqueKey(Constraint.generateName("UK_", table, column));
                    uk.setName(determineUniqueKeyName(table.getName(), column.getName()));
                }
            }
        }
    }

    private static String determineUniqueKeyName(final String tableName, final String columnName) {
        return tableName.toLowerCase(Locale.ENGLISH) + "_" + columnName.toLowerCase(Locale.ENGLISH) + "_key";
    }

    @Override
    public Identifier determineForeignKeyName(final ImplicitForeignKeyNameSource source) {
        return determineImplicitName("FK", source);
    }

    private Identifier determineImplicitName(final String prefix, final ImplicitConstraintNameSource source) {
        final String tableName = source.getTableName().getText();
        final Iterable<String> columnNames = Iterables.transform(source.getColumnNames(), Identifier::getText);
        return toIdentifier(
                prefix + uniqueColumnString(tableName, columnNames),
                source.getBuildingContext());
    }

    private static String uniqueColumnString(final String tableName, final Iterable<String> columnNames) {
        int hash = 0;
        for (String columnName : columnNames) {
            hash += columnName.hashCode();
        }
        final String str = Integer.toHexString(tableName.hashCode()) + Integer.toHexString(hash);
        return str.toUpperCase();
    }
}
