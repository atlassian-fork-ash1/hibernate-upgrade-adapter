package com.atlassian.hibernate.util.dialect;

import org.hibernate.dialect.Oracle9iDialect;

import java.sql.Types;

/**
 * A dialect for Oracle 9/10/11, which supports Unicode characters.
 * <p>
 * Copied from from atlassian:hibernate-2.1.8 for use with hibernate 5.
 */
public class OracleIntlDialectV5 extends Oracle9iDialect {
    /**
     * Copied from from atlassian:hibernate-2.1.8.
     * <p>
     * According to http://docs.oracle.com/cd/E11882_01/server.112/e10729/ch7progrunicode.htm#NLSPG336 :
     * The maximum column size allowed is 4000 characters when the national character set is UTF8 and 2000 when it is AL16UTF16.
     * The maximum length of an NVARCHAR2 column in bytes is 4000. Both the byte limit and the character limit must be met,
     * so the maximum number of characters that is actually allowed in an NVARCHAR2 column is the number of characters
     * that can be written in 4000 bytes.
     * <p>
     * This means, that you're perfectly able to create column of type NVARCHAR2(2000),
     * but you might not be able to write 2000 characters (rare control characters) there.
     */
    public OracleIntlDialectV5() {
        super();
        registerColumnType(Types.VARCHAR, 2000, "nvarchar2($l)");
        // we need to overwrite original column type registration done in Oracle9Dialect:
        // registerColumnType( Types.VARCHAR, 4000, "varchar2($l)" );
        registerColumnType(Types.VARCHAR, 4000, "nclob");
        registerColumnType(Types.VARCHAR, "nclob");
    }
}
