package com.atlassian.hibernate.adapter.transpiler;

import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;

/**
 * A transpiler for HQL query strings from v2 to v5 onwards.
 */
public final class HibernateQueryTranspiler {

    private static final String COUNT_CAST_PREFIX = "CAST(";
    private static final String COUNT_CAST_SUFFIX = " AS integer)";

    private static WeakHashMap<String, String> transpileCache = new WeakHashMap<>();
    private static final FastStringKeyMap<FragmentHandler> FRAGMENT_HANDLERS = new FastStringKeyMap<>();

    private HibernateQueryTranspiler() { }

    static {
        FRAGMENT_HANDLERS.put(" count(", (query, i, str) -> castCountFromLongToInt(i, str));
        FRAGMENT_HANDLERS.put("(count(", (query, i, str) -> castCountFromLongToInt(i, str));
        FRAGMENT_HANDLERS.put(" COUNT(", (query, i, str) -> castCountFromLongToInt(i, str));
        FRAGMENT_HANDLERS.put("(COUNT(", (query, i, str) -> castCountFromLongToInt(i, str));
        FRAGMENT_HANDLERS.put(".class", (query, i, str) -> updateClassToTypeFunction(query, i, str));
        FRAGMENT_HANDLERS.put("where", (query, i, str) -> removeCommaBeforeWhere(query, i, str));
        FRAGMENT_HANDLERS.put("WHERE", (query, i, str) -> removeCommaBeforeWhere(query, i, str));
        FRAGMENT_HANDLERS.put(" in class ", (query, i, str) -> updateInClassClause(query, i, str));
        FRAGMENT_HANDLERS.put(" IN CLASS ", (query, i, str) -> updateInClassClause(query, i, str));
        FRAGMENT_HANDLERS.put("'", (query, i, str) -> skipLiteral(query, i));
    }

    public static String transpileQuery(final String query) {
        // use the transpile cache
        String result;
        synchronized (transpileCache) {
            result = transpileCache.get(query);
        }
        if (result != null && result.length() == 0) {
            // "" means no transpiling was needed
            return query;
        }

        // transpile the query
        if (result == null) {
            result = transpileQueryImpl(query);
            synchronized (transpileCache) {
                transpileCache.put(query, result == query ? "" : result);
            }
        }
        return result;
    }

    private static String transpileQueryImpl(final String query) {
        final AtomicReference<StringBuilder> resultRef = new AtomicReference<>();
        final Supplier<StringBuilder> lazyStringBuilder = getLazyCreatedStringBuilder(query, resultRef);

        // this contains either the query String, or in progress StringBuilder
        CharSequence result = query;

        int i = 0;
        int length = result.length();
        while (i < length) {
            // at the current position in the query, find a handler
            final FragmentHandler handler = FRAGMENT_HANDLERS.get(result, i);
            if (handler != null) {
                i = handler.applyChanges(result, i, lazyStringBuilder);

                // if the handler created the StringBuilder, update the result
                final StringBuilder current = resultRef.get();
                if (current != null) {
                    result = current;
                    length = result.length();
                }
            } else {
                i++;
            }
        }
        return result.toString();
    }

    private static Supplier<StringBuilder> getLazyCreatedStringBuilder(
            final String query,
            final AtomicReference<StringBuilder> ref) {
        return () -> {
            StringBuilder result = ref.get();
            if (result == null) {
                result = new StringBuilder(query);
                ref.set(result);
            }
            return result;
        };
    }

    /**
     * Remove the comma in 'from entity, where'.
     */
    private static int removeCommaBeforeWhere(final CharSequence query, final int i, final Supplier<StringBuilder> result) {
        final int comma = skipWhitespacesBackFrom(query, i - 1);
        if (comma == -1 || query.charAt(comma) != ',')
            return i + 1;

        result.get().deleteCharAt(comma);
        return i + 5;
    }

    /**
     * Update 'count(*)' to 'CAST(count(*) AS integer)'.
     */
    private static int castCountFromLongToInt(
            final int i,
            final Supplier<StringBuilder> result) {

        final StringBuilder stringBuilder = result.get();
        final int j = stringBuilder.indexOf(")", i);
        if (j == -1)
            return i + 1;

        // skip if this has already been transpiled
        if (stringBuilder.substring(i - COUNT_CAST_PREFIX.length() + 1, i + 1).equals(COUNT_CAST_PREFIX)
                && stringBuilder.substring(j + 1, j + COUNT_CAST_SUFFIX.length() + 1).equals(COUNT_CAST_SUFFIX)) {
            return i + 1;
        }

        stringBuilder.insert(j + 1, COUNT_CAST_SUFFIX);
        stringBuilder.insert(i + 1, COUNT_CAST_PREFIX);
        return j + 18;
    }

    /**
     * Update 'alias.class = com.class.name' to 'type(alias) = 'com.class.name'.
     */
    private static int updateClassToTypeFunction(final CharSequence query, final int index, final Supplier<StringBuilder> result) {
        int i = index;

        // look for the start of the expression ('(' or ' ' before .class)
        final int j = findSpaceOrOpenBracketBackFrom(query, i);
        if (j == -1 || i < 2)
            return i + 1;

        final int cls = i;
        i = skipWhitespaces(query, i + 6);

        // if the 'in operator' operator is used, we will
        // actually be comparing the discriminator value and want to use .class
        if (query.charAt(i) == 'i'
                && query.charAt(i + 1) == 'n'
                && Character.isWhitespace(query.charAt(i + 2))) {
            return i + 3;
        }

        final StringBuilder stringBuilder = result.get();

        // if there's a an operator after .class, surround the rhs expression with quotes
        final int rhs = isOperator(query, i);
        if (rhs != -1) {
            final int startExpr = skipWhitespaces(query, rhs);
            final int endExpr = findWhitespaceOrCloseBracket(query, startExpr);
            if (endExpr == -1)
                return i;

            i = endExpr + 2;
        } else {
            i = cls + 6;
        }

        // surround the alias with type()
        stringBuilder.delete(cls, cls + 6);
        stringBuilder.insert(j + 1, "type(");
        stringBuilder.insert(cls + 5, ')');
        return i;
    }

    private static int isOperator(final CharSequence query, final int i) {
        if (query.charAt(i) == '!' && query.charAt(i + 1) == '=')
            return i + 2;
        else if (query.charAt(i) == '=')
            return i + 1;
        else
            return -1;
    }

    /**
     * Update 'from alias in class com.class.name' to 'from com.class.name as alias'.
     */
    private static int updateInClassClause(final CharSequence query, final int i, final Supplier<StringBuilder> result) {
        final int j = findSpaceBackFrom(query, i - 1);
        if (j == -1)
            return i + 1;

        final CharSequence name = query.subSequence(j + 1, i);
        final int k = indexOfWhitespace(result.get(), i + " in class ".length());
        if (k == -1)
            return i + 1;

        result.get().insert(k, " as " + name);
        result.get().delete(i - name.length() - 1, i + 9);
        return k + 4 + name.length();
    }

    private static int skipLiteral(final CharSequence query, final int start) {
        int length = query.length();
        for (int i = start + 1; i < length; i++) {
            char c = query.charAt(i);
            if (c == '\'') {
                return i + 1;
            } else if (c == '\\') {
                i++;
            }
        }
        return start + 1;
    }

    private static int indexOfWhitespace(final CharSequence str, final int start) {
        for (int i = start; i < str.length(); i++) {
            if (Character.isWhitespace(str.charAt(i))) {
                return i;
            }
        }
        return -1;
    }

    private static int skipWhitespaces(final CharSequence query, final int index) {
        int i = index;
        for (; i < query.length(); i++) {
            if (!Character.isWhitespace(query.charAt(i)))
                break;
        }
        return i;
    }

    private static int skipWhitespacesBackFrom(final CharSequence query, final int index) {
        int i = index;
        for (; i >= 0; i--) {
            if (!Character.isWhitespace(query.charAt(i)))
                return i;
        }
        return -1;
    }

    private static int findWhitespaceOrCloseBracket(final CharSequence query, final int index) {
        int i = index;
        for (; i < query.length(); i++) {
            if (Character.isWhitespace(query.charAt(i)) || query.charAt(i) == ')')
                break;
        }
        return i;
    }

    private static int findSpaceOrOpenBracketBackFrom(final CharSequence query, final int index) {
        int i = index;
        for (; i >= 0; i--) {
            if (Character.isWhitespace(query.charAt(i)) || query.charAt(i) == '(')
                return i;
        }
        return -1;
    }

    private static int findSpaceBackFrom(final CharSequence query, final int index) {
        int i = index;
        for (; i >= 0; i--) {
            if (Character.isWhitespace(query.charAt(i)))
                return i;
        }
        return -1;
    }

    private static void addReplaceHandler(
            final FastStringKeyMap<FragmentHandler> handlers,
            final String from,
            final String to) {

        handlers.put(
                from,
                (query, i, str) -> replace(from, to, str.get(), i));
    }

    private static int replace(final String from, final String to, final StringBuilder str, final int i) {
        str.replace(i, i + from.length(), to);
        return i + to.length();
    }

    private interface FragmentHandler {
        int applyChanges(CharSequence query, int i, Supplier<StringBuilder> str);
    }
}
