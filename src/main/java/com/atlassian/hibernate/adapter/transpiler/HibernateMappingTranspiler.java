package com.atlassian.hibernate.adapter.transpiler;

import com.google.common.base.Strings;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.DOMException;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * A transpiler for .hbm files from v2 to v5 onwards that performs the following transformations.
 * - !DOCTYPE element is updated
 * - &lt;cache&gt; element is removed with the 'cache-strategy' attribute returned
 * - 'lazy' attribute defaulted to 'false' if not specified
 */
public class HibernateMappingTranspiler {
    private static final String DTD_QUALIFIED_NAME = "hibernate-mapping";
    private static final String DTD_PUBLIC_ID = "-//Hibernate/Hibernate Mapping DTD 3.0//EN";
    private static final String DTD_SYSTEM_ID = "http://www.hibernate.org/dtd/hibernate-mapping-3.0.dtd";

    private static void transpileDocument(final HibernateMappingTranspilerResult mapping, final Document doc) {

        // set default-lazy
        if (StringUtils.isEmpty(doc.getDocumentElement().getAttribute("default-lazy"))) {
            doc.getDocumentElement().setAttribute("default-lazy", "false");
        }

        // transpile class elements
        final NodeList children = doc.getDocumentElement().getChildNodes();
        for (final Element element : getElementsByName(children, "class", "subclass", "joined-subclass")) {
            transpileClassElement(mapping, element);
        }

        // transpile queries
        for (final Element element : getElementsByName(children, "query")) {
            element.setTextContent(HibernateQueryTranspiler.transpileQuery(element.getTextContent()));
        }
    }

    private static void transpileClassElement(
            final HibernateMappingTranspilerResult mapping,
            final Element classElement) {

        final String className = classElement.getAttribute("name");
        transpileCacheStrategy(mapping, classElement, className);

        // remove <cache> from collection properties
        for (final Element collectionElement : getElementsByName(classElement.getChildNodes(), "set", "list", "bag", "ibag", "map")) {
            if (Strings.isNullOrEmpty(collectionElement.getAttribute("optimistic-lock"))) {
                collectionElement.setAttribute("optimistic-lock", "false");
            }
            transpileCacheStrategy(mapping, collectionElement, className + "." + collectionElement.getAttribute("name"));
        }

        // transpile sub-class elements
        for (final Element element : getElementsByName(classElement.getChildNodes(), "subclass")) {
            transpileClassElement(mapping, element);
        }
    }

    private static void transpileCacheStrategy(
            final HibernateMappingTranspilerResult mapping,
            final Element classElement,
            final String className) {

        for (final Element cacheElement : getElementsByName(classElement.getChildNodes(), "cache")) {
            final String strategyClass = cacheElement.getAttribute("strategy-class");
            if (!Strings.isNullOrEmpty(strategyClass)) {
                mapping.addCacheStrategy(className, strategyClass);
            }

            cacheElement.removeAttribute("strategy-class");
            cacheElement.setAttribute("usage", "read-write");
        }
    }

    private static DocumentBuilder createDocumentBuilder() throws IOException {
        try {
            final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            // avoid downloading the DTD file over the internet
            factory.setAttribute("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            return factory.newDocumentBuilder();
        } catch (final ParserConfigurationException ex) {
            throw new IOException(ex.getMessage(), ex);
        }
    }

    private static Document parseDocument(final InputStream stream, final DocumentBuilder builder) throws IOException {
        try {
            return builder.parse(stream);
        } catch (final SAXException ex) {
            throw new IOException(ex.getMessage(), ex);
        }
    }

    private static org.w3c.dom.Document createDocumentWithNewDocType(
            final org.w3c.dom.Document doc,
            final DocumentBuilder builder) {

        final org.w3c.dom.Document newdoc = builder.newDocument();
        final DOMImplementation domImpl = doc.getImplementation();
        final DocumentType doctype = domImpl.createDocumentType(
                DTD_QUALIFIED_NAME, DTD_PUBLIC_ID, DTD_SYSTEM_ID);
        newdoc.appendChild(doctype);
        newdoc.appendChild(newdoc.importNode(doc.getDocumentElement(), true));
        return newdoc;
    }

    private static InputStream documentToStream(final Document doc) {
        final DOMImplementationLS domImpl = (DOMImplementationLS) doc.getImplementation();

        final LSOutput destination = domImpl.createLSOutput();
        destination.setEncoding("UTF-8");
        final ByteArrayOutputStream output = new ByteArrayOutputStream();
        destination.setByteStream(output);

        domImpl.createLSSerializer().write(doc, destination);
        return new ByteArrayInputStream(output.toByteArray());
    }

    private static Element[] getElementsByName(final NodeList nodes, final String... elementNames) {
        final List<Element> list = new ArrayList<>();
        for (int i = 0; i < nodes.getLength(); i++) {
            final Node node = nodes.item(i);
            if (node instanceof Element
                    && ArrayUtils.contains(elementNames, node.getNodeName())) {
                list.add((Element) node);
            }
        }
        return list.toArray(new Element[list.size()]);
    }

    /**
     * Transpile a v2 hbm file to a v5 onwards hbm file.
     *
     * @param stream The v2 hbm file contents
     * @return The v5 hbm file contents, and removed &lt;cache&gt; element details
     * @throws IOException when an error occurs during document parsing
     */
    public HibernateMappingTranspilerResult transpile(final InputStream stream) throws IOException {
        try {
            final DocumentBuilder builder = createDocumentBuilder();
            final Document doc = parseDocument(stream, builder);
            final org.w3c.dom.Document newdoc = createDocumentWithNewDocType(doc, builder);

            final HibernateMappingTranspilerResult mapping = new HibernateMappingTranspilerResult();
            transpileDocument(mapping, newdoc);

            mapping.setTranspiledMappingFile(documentToStream(newdoc));
            return mapping;
        } catch (final DOMException ex) {
            throw new IOException(ex.getMessage(), ex);
        }
    }
}
