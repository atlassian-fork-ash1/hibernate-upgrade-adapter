package com.atlassian.hibernate.adapter.transpiler;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * The return value of the HibernateMappingTranspiler.transpile() method that
 * returns the transpiled hbm content and cache strategy class names.
 */
public class HibernateMappingTranspilerResult {
    private final Map<String, String> cacheStrategyClassNames = new HashMap<>();
    private InputStream transpiledMappingFile;

    /**
     * Get the transpiled hbm file content.
     */
    public InputStream getTranspiledMappingFile() {
        return transpiledMappingFile;
    }

    /**
     * Set the transpiled hbm file content.
     */
    void setTranspiledMappingFile(final InputStream stream) {
        transpiledMappingFile = stream;
    }

    /**
     * Add a v2 CacheConcurrencyStrategy class name.
     */
    void addCacheStrategy(final String entityClassOrRole, final String cacheStrategyClassName) {
        cacheStrategyClassNames.put(entityClassOrRole, cacheStrategyClassName);
    }

    /**
     * Get the v2 CacheConcurrencyStrategy class names.
     */
    public Map<String, String> getCacheStrategyClassNames() {
        return cacheStrategyClassNames;
    }
}
