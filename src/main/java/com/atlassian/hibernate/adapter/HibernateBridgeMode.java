package com.atlassian.hibernate.adapter;

/**
 * Specifies which mode of hibernate to use when configuring hibernate from
 * ConfigurationV2Bridge, LocalSessionFactoryV2BridgeBean and HibernateBridge.getV2orV5SessionFactory.
 */
public enum HibernateBridgeMode {
    /**
     * Uses the v2 hibernate stack directly by creating v2 Session objects.
     * A v5 SessionFactory is not created automatically in this mode, but may
     * be lazy created by calling HibernateBridge.getV5SessionFactory().
     */
    V2(21),

    /**
     * Uses the v5 onwards hibernate stack using an v2 to v5 adapter interface.
     * Both v2 and v5 SessionFactory objects are created in this mode, which in general
     * will consume more memory, when enabled.
     */
    V5_ADAPTER(51),

    /**
     * Uses the v2 hibernate stack directly, without any support for v5.
     * This mode is the most compatible setting possible for existing code, and surfaces
     * the v2 SessionFactory / Session objects without any kind of proxy or interception.
     *
     * This mode is useful to apply to production environments while testing V5 in test environments
     * without requiring a redeployment.
     */
    V2_COLD_START(22),

    //---------- Session v2 / v5 side-by-side modes ----------//

    /**
     * This is a session v2 / v5 side-by-side mode, and usually cause both SessionFactory versions to reside in memory.
     *
     * Uses the v2 hibernate stack directly, with support to use both a v2 and v5 Session
     * implementation within the same transaction, using a SessionBridge.
     *
     * A v5 SessionFactory is not created automatically in this mode, but may
     * be lazy created by calling HibernateBridge.getV5SessionFactory() or HibernateBridge.getV5Session().
     */
    V2_WITH_SESSION_BRIDGE(23),

    /**
     * This is a session v2 / v5 side-by-side mode, and usually cause both SessionFactory versions to reside in memory.
     *
     * Uses the v5 onwards hibernate stack using an v2 to v5 adapter interface,
     * with support to use both a v5 and v2 Session implementation within
     * the same transaction, using a SessionBridge.
     *
     * Both v2 and v5 SessionFactory objects are created in this mode, which in general
     * will consume more memory, when enabled.
     */
    V5_ADAPTER_WITH_SESSION_BRIDGE(53),

    /**
     * This is a session v2 / v5 side-by-side mode, and usually cause both SessionFactory versions to reside in memory.
     *
     * Uses the v5 onwards hibernate stack using an v2 to v5 adapter interface,
     * along with some simple sanity checks during querying across v2 and v5 stacks.
     * This mode affects performance and isn't recommended in a production environment.
     * This setting implies V5_ADAPTER_WITH_SESSION_BRIDGE.
     */
    V5_ADAPTER_WITH_QUERY_COMPARISON(54);

    //--------------------//

    private final int value;

    HibernateBridgeMode(final int value) {
        this.value = value;
    }

    public boolean isV2() {
        return value <= 29;
    }

    public boolean isV5() {
        return value >= 50;
    }

    public boolean isSessionBridgeEnabled() {
        return
                this == V2_WITH_SESSION_BRIDGE
                || this == V5_ADAPTER_WITH_SESSION_BRIDGE
                || this == V5_ADAPTER_WITH_QUERY_COMPARISON;
    }
}
