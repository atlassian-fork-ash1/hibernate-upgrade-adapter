package com.atlassian.hibernate.adapter.generator;

import com.atlassian.hibernate.adapter.adapters.DialectAdapter;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.generator.IdentifierGeneratorV5Adapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV2Adapter;
import org.hibernate.MappingException;
import org.hibernate.dialect.Dialect;
import org.hibernate.envers.enhanced.OrderedSequenceGenerator;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.id.factory.internal.DefaultIdentifierGeneratorFactory;
import org.hibernate.type.Type;

import java.util.Locale;
import java.util.Properties;

/**
 * Provides a bridge between the v2 IdentifierGenerator interface
 * for hibernate v2 / v5 (onwards).
 */
public class IdentifierGeneratorFactoryV5Bridge extends DefaultIdentifierGeneratorFactory {
    private Dialect dialect;

    private static String upgradeSequenceParameters(final Properties config) {
        final String parametersString = (String) config.get("parameters");
        final String[] values = parametersString.split(" ");

        String strategyName = "sequence";
        for (int i = 0; i < values.length; i++) {

            // INCREMENT BY
            if (i + 2 < values.length
                    && values[i].toUpperCase(Locale.ENGLISH).equals("INCREMENT")
                    && values[i + 1].toUpperCase(Locale.ENGLISH).equals("BY")) {
                config.put(SequenceStyleGenerator.INCREMENT_PARAM, Integer.parseInt(values[i + 2]));
                i += 2;
                continue;
            }

            // START WITH
            if (i + 2 < values.length
                    && values[i].toUpperCase(Locale.ENGLISH).equals("START")
                    && values[i + 1].toUpperCase(Locale.ENGLISH).equals("WITH")) {
                config.put(SequenceStyleGenerator.INITIAL_PARAM, Integer.parseInt(values[i + 2]));
                i += 2;
                continue;
            }

            // ORDER
            if (values[i].equals("ORDER")) {
                strategyName = OrderedSequenceGenerator.class.getName();
            }
        }
        return strategyName;
    }

    @Override
    public void setDialect(final Dialect dialect) {
        this.dialect = dialect;
    }

    @Override
    public IdentifierGenerator createIdentifierGenerator(
            final String strategy,
            final Type type,
            final Properties config) {

        String newStrategy = strategy;

        // 'vm' is deprecated, but is the same as 'increment'
        if ("vm".equals(newStrategy)) {
            return createIdentifierGenerator("increment", type, config);
        }

        // sequence parameters have changed since v2, update the config
        if ("sequence".equals(newStrategy)) {
            if (config.get("sequence") != null && config.get(SequenceStyleGenerator.SEQUENCE_PARAM) == null) {
                config.put(SequenceStyleGenerator.SEQUENCE_PARAM, config.get("sequence"));
                config.remove("sequence");
            }
            newStrategy = upgradeSequenceParameters(config);
        }

        final Class clazz = super.getIdentifierGeneratorClass(newStrategy);
        if (net.sf.hibernate.id.IdentifierGenerator.class.isAssignableFrom(clazz)) {
            return instantiateAndAdapt(clazz, type, config);
        }
        return super.createIdentifierGenerator(newStrategy, type, config);
    }

    @Override
    public Class getIdentifierGeneratorClass(final String strategy) {
        final Class clazz = super.getIdentifierGeneratorClass(strategy);
        if (net.sf.hibernate.id.IdentifierGenerator.class.isAssignableFrom(clazz)) {
            return IdentifierGeneratorV5Adapter.class;
        }
        return clazz;
    }

    private IdentifierGenerator instantiateAndAdapt(
            final Class identifierGeneratorClassV2,
            final Type type,
            final Properties config) throws MappingException {
        final net.sf.hibernate.id.IdentifierGenerator identifierGeneratorV2 =
                instantiate(identifierGeneratorClassV2, config);

        if (identifierGeneratorV2 instanceof net.sf.hibernate.id.Configurable) {
            configure(type, config, (net.sf.hibernate.id.Configurable) identifierGeneratorV2);
        }
        return IdentifierGeneratorV5Adapter.adapt(identifierGeneratorV2);
    }

    private net.sf.hibernate.id.IdentifierGenerator instantiate(final Class identifierGeneratorClassV2, final Properties config) {
        final net.sf.hibernate.id.IdentifierGenerator identifierGeneratorV2;
        try {
            identifierGeneratorV2 = (net.sf.hibernate.id.IdentifierGenerator) identifierGeneratorClassV2.newInstance();
        } catch (final ReflectiveOperationException ex) {
            final String entityName = config.getProperty(IdentifierGenerator.ENTITY_NAME);
            throw new MappingException(String.format("Could not instantiate id generator [entity-name=%s]", entityName), ex);
        }
        return identifierGeneratorV2;
    }

    private void configure(final Type type, final Properties config, final net.sf.hibernate.id.Configurable identifierGeneratorV2) {
        final net.sf.hibernate.id.Configurable configurable = identifierGeneratorV2;
        try {
            configurable.configure(TypeV2Adapter.adapt(type), config, DialectAdapter.adapt(dialect));
        } catch (final net.sf.hibernate.MappingException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }
}
