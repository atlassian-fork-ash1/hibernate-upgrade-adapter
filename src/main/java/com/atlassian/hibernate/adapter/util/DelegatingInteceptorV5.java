package com.atlassian.hibernate.adapter.util;

import org.hibernate.CallbackException;
import org.hibernate.EntityMode;
import org.hibernate.Interceptor;
import org.hibernate.Transaction;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.Iterator;

/**
 * Base class for v5 Interceptor implementations that wish to implement only parts of that contract
 * themselves while forwarding other method invocations to a delegate instance.
 */
public abstract class DelegatingInteceptorV5 implements Interceptor {
    private final Interceptor interceptor;

    protected DelegatingInteceptorV5(final Interceptor interceptor) {
        this.interceptor = interceptor;
    }

    @Override
    public boolean onLoad(final Object entity, final Serializable id, final Object[] state, final String[] propertyNames, final Type[] types) throws CallbackException {
        return interceptor.onLoad(entity, id, state, propertyNames, types);
    }

    @Override
    public boolean onFlushDirty(final Object entity, final Serializable id, final Object[] currentState, final Object[] previousState, final String[] propertyNames, final Type[] types) throws CallbackException {
        return interceptor.onFlushDirty(entity, id, currentState, previousState, propertyNames, types);
    }

    @Override
    public boolean onSave(final Object entity, final Serializable id, final Object[] state, final String[] propertyNames, final Type[] types) throws CallbackException {
        return interceptor.onSave(entity, id, state, propertyNames, types);
    }

    @Override
    public void onDelete(final Object entity, final Serializable id, final Object[] state, final String[] propertyNames, final Type[] types) throws CallbackException {
        interceptor.onDelete(entity, id, state, propertyNames, types);
    }

    @Override
    public void onCollectionRecreate(final Object collection, final Serializable key) throws CallbackException {
        interceptor.onCollectionRecreate(collection, key);
    }

    @Override
    public void onCollectionRemove(final Object collection, final Serializable key) throws CallbackException {
        interceptor.onCollectionRemove(collection, key);
    }

    @Override
    public void onCollectionUpdate(final Object collection, final Serializable key) throws CallbackException {
        interceptor.onCollectionUpdate(collection, key);
    }

    @Override
    public void preFlush(final Iterator entities) throws CallbackException {
        interceptor.preFlush(entities);
    }

    @Override
    public void postFlush(final Iterator entities) throws CallbackException {
        interceptor.postFlush(entities);
    }

    @Override
    public Boolean isTransient(final Object entity) {
        return interceptor.isTransient(entity);
    }

    @Override
    public int[] findDirty(final Object entity, final Serializable id, final Object[] currentState, final Object[] previousState, final String[] propertyNames, final Type[] types) {
        return interceptor.findDirty(entity, id, currentState, previousState, propertyNames, types);
    }

    @Override
    public Object instantiate(final String entityName, final EntityMode entityMode, final Serializable id) throws CallbackException {
        return interceptor.instantiate(entityName, entityMode, id);
    }

    @Override
    public String getEntityName(final Object object) throws CallbackException {
        return interceptor.getEntityName(object);
    }

    @Override
    public Object getEntity(final String entityName, final Serializable id) throws CallbackException {
        return interceptor.getEntity(entityName, id);
    }

    @Override
    public void afterTransactionBegin(final Transaction tx) {
        interceptor.afterTransactionBegin(tx);
    }

    @Override
    public void beforeTransactionCompletion(final Transaction tx) {
        interceptor.beforeTransactionCompletion(tx);
    }

    @Override
    public void afterTransactionCompletion(final Transaction tx) {
        interceptor.afterTransactionCompletion(tx);
    }

    @Override
    @Deprecated
    public String onPrepareStatement(final String sql) {
        return interceptor.onPrepareStatement(sql);
    }
}
