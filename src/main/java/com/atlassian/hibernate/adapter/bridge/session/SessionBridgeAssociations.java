package com.atlassian.hibernate.adapter.bridge.session;

/**
 * Retrieves SessionBridge objects from v2 and v5 Session objects.
 * <p>
 * This is done in such a way that uses Session and SessionFactory interfaces only without
 * casting to a concrete class or special interface, for compatibility with
 * java proxies, Spring proxies, and OSGI.
 */
public final class SessionBridgeAssociations {

    private SessionBridgeAssociations() { }

    /**
     * Get the SessionBridge object from a Session.
     */
    public static SessionBridge getSessionBridge(final net.sf.hibernate.Session session) {
        if (session == null) {
            return null;
        }

        // get(SessionBridge.class) is implemented by SessionV2BridgeAdapter / SessionV2BridgeProxy.
        // This won't work when passed the real v2 SessionImpl object, but that isn't needed anywhere.
        try {
            return (SessionBridge) session.get(SessionBridge.class, null);
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw new IllegalArgumentException("session was not created by a SessionFactoryBridge");
        }
    }

    /**
     * Get whether a SessionBridge is available from a v5 Session object.
     */
    public static boolean hasSessionBridge(final org.hibernate.Session session) {
        org.hibernate.Interceptor interceptor =
                ((org.hibernate.engine.spi.SessionImplementor) session).getInterceptor();
        return interceptor instanceof InterceptorV5WithSessionBridgeHolder;
    }

    /**
     * Get the SessionBridge object from a Session.
     */
    public static SessionBridge getSessionBridge(final org.hibernate.Session session) {
        if (session == null) {
            return null;
        }

        // The real v5 Session object's public getInterceptor is used to hold the SessionBridge.
        // This is because getInterceptor can be assigned without reflection on a v5 object, and
        // the SessionBridge must be available from both the real SessionImpl and
        // SessionFactoryV5BridgeProxy objects.
        final org.hibernate.Interceptor interceptor =
                ((org.hibernate.engine.spi.SessionImplementor) session).getInterceptor();
        if (!(interceptor instanceof InterceptorV5WithSessionBridgeHolder)) {
            throw new IllegalArgumentException("Session was not created by a SessionFactoryBridge");
        }

        // return the SessionBridge's v2 Session
        return ((InterceptorV5WithSessionBridgeHolder) interceptor).getSessionBridge();
    }
}
