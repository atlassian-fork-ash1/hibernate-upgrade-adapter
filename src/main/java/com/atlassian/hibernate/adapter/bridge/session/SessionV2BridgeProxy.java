package com.atlassian.hibernate.adapter.bridge.session;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.FlushModeAdapter;
import com.atlassian.hibernate.util.DelegatingSessionV2;
import net.sf.hibernate.FlushMode;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.SessionFactory;
import net.sf.hibernate.Transaction;
import net.sf.hibernate.engine.SessionImplementor;

import javax.persistence.PersistenceException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * The v2 Session that has v2 interface specific behaviour and delegates it's implementation to
 * SessionBridge / SessionFactoryBridge.
 */
class SessionV2BridgeProxy extends DelegatingSessionV2 {
    private final SessionBridge sessionBridge;

    public SessionV2BridgeProxy(final SessionBridge sessionBridge) {
        this.sessionBridge = sessionBridge;
    }

    @Override
    public Object get(final Class clazz, final Serializable id) throws HibernateException {
        // this is how HibernateBridgeImpl gets to the v5 Session object, even if
        // the Session interface is intercepted by a java or spring proxy
        if (clazz == org.hibernate.Session.class) {
            return sessionBridge.getV5SessionProxy();
        }

        // this is how SessionFactoryBridge gets to the v5 Session object, even if
        // the Session interface is intercepted by a java or spring proxy
        if (clazz == SessionBridge.class) {
            return sessionBridge;
        }
        return super.get(clazz, id);
    }

    @Override
    public SessionFactory getSessionFactory() {
        return sessionBridge.getHibernateBridge().getV2orV5SessionFactory();
    }

    @Override
    public Connection connection() throws HibernateException {
        try {
            return sessionBridge.connection();
        } catch (final SQLException ex) {
            throw HibernateExceptionAdapter.toV2HibernateException(getSessionFactory(), ex, ex.getMessage());
        }
    }

    @Override
    public boolean isOpen() {
        return sessionBridge.isOpen();
    }

    @Override
    public boolean isConnected() {
        return sessionBridge.isConnected();
    }

    @Override
    public Connection close() throws HibernateException {
        try {
            return sessionBridge.close();
        } catch (final SQLException ex) {
            throw HibernateExceptionAdapter.toV2HibernateException(getSessionFactory(), ex, "Cannot close connection");
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Connection disconnect() throws HibernateException {
        try {
            return sessionBridge.disconnect();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void reconnect() throws HibernateException {
        try {
            sessionBridge.reconnect();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void reconnect(final Connection connection) throws HibernateException {
        try {
            sessionBridge.reconnect(connection);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void flush() throws HibernateException {
        try {
            sessionBridge.flush();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    //---------- Lifecycle ----------//

    @Override
    public boolean hasSession() {
        return super.hasSession();
    }

    @Override
    public SessionImplementor getSession() throws HibernateException {
        return super.getSession();
    }

    @Override
    public SessionImplementor getSessionNoCreate() {
        return super.getSessionNoCreate();
    }

    @Override
    protected SessionImplementor createSession() throws HibernateException {
        return (SessionImplementor) sessionBridge.createV2Session();
    }

    //---------- get/setFlushMode ----------//

    @Override
    public FlushMode getFlushMode() {
        return FlushModeAdapter.adapt(sessionBridge.getHibernateFlushMode());
    }

    @Override
    public void setFlushMode(final FlushMode flushMode) {
        sessionBridge.setHibernateFlushMode(FlushModeAdapter.adapt(flushMode));
    }

    public void setRealFlushMode(final FlushMode flushMode) {
        getSessionNoCreate().setFlushMode(flushMode);
    }

    //---------- beginTransaction ----------//

    @Override
    public Transaction beginTransaction() throws HibernateException {
        return sessionBridge.beginTransactionV2();
    }
}
