package com.atlassian.hibernate.adapter.bridge.session;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.HibernateBridgeMode;
import com.atlassian.hibernate.adapter.adapters.FlushModeAdapter;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.session.SessionV2Supplier;
import com.atlassian.hibernate.adapter.session.SessionV5Supplier;
import com.atlassian.hibernate.util.FastClearStatefulPersistenceContext;
import org.apache.commons.lang3.NotImplementedException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The v2 / v5 (onwards) session bridge implementation.
 * v2 and v5 sessions are lazy created and their transactions and
 * other state are synchronized.
 */
public class SessionBridge {
    private final HibernateBridge hibernateBridge;
    private final HibernateBridgeMode bridgeMode;

    private final SessionV2Supplier sessionV2Creator;
    private final SessionV5Supplier sessionV5Creator;
    private final net.sf.hibernate.connection.ConnectionProvider connectionProvider;
    private SessionV2BridgeProxy sessionV2Proxy;
    private SessionV5BridgeProxy sessionV5Proxy;
    private SessionV2BridgeAdapter sessionV2Adapter;

    private Connection connection;
    private List<TransactionBridge> activeTransactions = new ArrayList<>(1);
    private boolean autoClose;
    private boolean disconnected;
    private boolean closed;

    private org.hibernate.FlushMode flushMode = org.hibernate.FlushMode.AUTO;

    public SessionBridge(
            final HibernateBridge hibernateBridge,
            final HibernateBridgeMode bridgeMode,
            final net.sf.hibernate.connection.ConnectionProvider connectionProvider,
            final Connection connection,
            final SessionV2Supplier sessionV2Creator,
            final SessionV5Supplier sessionV5Creator) {

        this.hibernateBridge = hibernateBridge;
        this.bridgeMode = bridgeMode;
        this.connectionProvider = connectionProvider;
        this.connection = connection;
        this.autoClose = connection == null;
        this.sessionV2Creator = sessionV2Creator;
        this.sessionV5Creator = sessionV5Creator;
    }

    public HibernateBridge getHibernateBridge() {
        return hibernateBridge;
    }

    public HibernateBridgeMode getBridgeMode() {
        return bridgeMode;
    }

    public boolean isOpen() {
        final net.sf.hibernate.Session sessionV2 = getRealV2SessionNoCreate();
        if (sessionV2 != null) {
            return sessionV2.isOpen();
        }

        final org.hibernate.Session sessionV5 = getRealV5SessionNoCreate();
        if (sessionV5 != null) {
            return sessionV5.isOpen();
        }
        return !closed;
    }

    public boolean isConnected() {
        final net.sf.hibernate.Session sessionV2 = getRealV2SessionNoCreate();
        if (sessionV2 != null) {
            return sessionV2.isConnected();
        }

        final org.hibernate.Session sessionV5 = getRealV5SessionNoCreate();
        if (sessionV5 != null) {
            return sessionV5.isConnected();
        }
        return !disconnected;
    }

    public Connection close()
            throws SQLException, net.sf.hibernate.HibernateException, org.hibernate.HibernateException {

        final Connection result = connection;
        final net.sf.hibernate.Session sessionV2 = getRealV2SessionNoCreate();
        final org.hibernate.Session sessionV5 = getRealV5SessionNoCreate();
        try {
            if (sessionV2 != null) {
                sessionV2.close();
            }
        } finally {
            try {
                if (sessionV5 != null) {
                    sessionV5.close();
                }
            } finally {
                closed = true;
                if (autoClose) {
                    connectionProvider.closeConnection(connection);
                    autoClose = false;
                }
            }
        }
        return result;
    }

    public Connection disconnect()
            throws net.sf.hibernate.HibernateException, org.hibernate.HibernateException {

        final Connection result = connection;
        final net.sf.hibernate.Session sessionV2 = getRealV2SessionNoCreate();
        final org.hibernate.Session sessionV5 = getRealV5SessionNoCreate();
        try {
            if (sessionV5 != null) {
                sessionV5.disconnect();
            }
        } finally {
            try {
                if (sessionV2 != null) {
                    sessionV2.disconnect();
                }
            } finally {
                connection = null;
                disconnected = true;
            }
        }
        return result;
    }

    public void reconnect()
            throws net.sf.hibernate.HibernateException, org.hibernate.HibernateException {

        final net.sf.hibernate.Session sessionV2 = getRealV2SessionNoCreate();
        final org.hibernate.Session sessionV5 = getRealV5SessionNoCreate();

        if (sessionV5 != null) {
            throw new NotImplementedException("reconnect() not available when using a v5 Session");
        }
        if (sessionV2 != null) {
            sessionV2.reconnect();
            this.connection = sessionV2.connection();
        }
    }

    public void reconnect(final Connection connection)
            throws net.sf.hibernate.HibernateException, org.hibernate.HibernateException {

        final net.sf.hibernate.Session sessionV2 = getRealV2SessionNoCreate();
        final org.hibernate.Session sessionV5 = getRealV5SessionNoCreate();

        if (sessionV5 != null) {
            throw new NotImplementedException("reconnect() not available when using a v5 Session");
        }
        if (sessionV2 != null) {
            sessionV2.reconnect(connection);
        }
        this.connection = connection;
    }

    public void flush()
            throws net.sf.hibernate.HibernateException, org.hibernate.HibernateException {
        final net.sf.hibernate.Session sessionV2 = getRealV2SessionNoCreate();
        final org.hibernate.Session sessionV5 = getRealV5SessionNoCreate();

        if (sessionV5 != null) {
            sessionV5.flush();
        }
        if (sessionV2 != null) {
            sessionV2.flush();
        }
    }

    //---------- Lifecycle ----------//

    /**
     * Lazy-create the SessionV2BridgeProxy object.
     */
    public SessionV2BridgeProxy getV2SessionProxy() {
        if (sessionV2Proxy == null) {
            sessionV2Proxy = new SessionV2BridgeProxy(this);
        }
        return sessionV2Proxy;
    }

    /**
     * Lazy-create the SessionV5BridgeProxy object.
     */
    public SessionV5BridgeProxy getV5SessionProxy() {
        if (sessionV5Proxy == null) {
            sessionV5Proxy = new SessionV5BridgeProxy(this);
        }
        return sessionV5Proxy;
    }

    /**
     * Lazy-create the SessionV2BridgeAdapter object.
     */
    public SessionV2BridgeAdapter getV2SessionAdapter() {
        if (sessionV2Adapter == null) {
            sessionV2Adapter = new SessionV2BridgeAdapter(
                    this,
                    hibernateBridge.getV2orV5SessionFactory(),
                    bridgeMode == HibernateBridgeMode.V5_ADAPTER_WITH_QUERY_COMPARISON);
        }
        return sessionV2Adapter;
    }

    /**
     * Get the real hibernate v2 Session object.
     */
    public net.sf.hibernate.Session getRealV2SessionNoCreate() {
        return sessionV2Proxy != null ? sessionV2Proxy.getSessionNoCreate() : null;
    }

    /**
     * Get the real hibernate v5 Session object.
     */
    public org.hibernate.Session getRealV5SessionNoCreate() {
        return sessionV5Proxy != null ? sessionV5Proxy.getSessionNoCreate() : null;
    }

    /**
     * Lazy-create the real hibernate v5 Session object.
     */
    public org.hibernate.Session getRealV5Session() {
        return getV5SessionProxy().getWrappedSession();
    }

    /**
     * Create the real v2 Session object,
     * synchronizing the transaction and flush mode if necessary.
     */
    public net.sf.hibernate.Session createV2Session() throws net.sf.hibernate.HibernateException {
        final net.sf.hibernate.Session session;
        try {
            session = sessionV2Creator.get(connection());
        } catch (final SQLException ex) {
            throw HibernateExceptionAdapter.toV2HibernateException(
                    hibernateBridge.getV2SessionFactory(), ex, "Cannot open connection");
        }
        session.setFlushMode(FlushModeAdapter.adapt(flushMode));

        for (int i = 0; i < activeTransactions.size(); i++) {
            activeTransactions.get(i).setTransactionV2(session);
        }
        return session;
    }

    /**
     * Create the real v5 Session object,
     * synchronizing the transaction and flush mode if necessary.
     */
    @SuppressWarnings("deprecation")
    public org.hibernate.Session createV5Session() throws org.hibernate.HibernateException {
        final org.hibernate.Session session;
        try {
            session = sessionV5Creator.get(this, connection());
            FastClearStatefulPersistenceContext.updatePersistenceContext(
                    (org.hibernate.engine.spi.SessionImplementor) session);
        } catch (final SQLException ex) {
            throw HibernateExceptionAdapter.toV5HibernateException(ex, "Cannot open connection");
        }
        session.setFlushMode(flushMode);

        for (int i = 0; i < activeTransactions.size(); i++) {
            activeTransactions.get(i).setTransactionV5(session);
        }
        return session;
    }

    //---------- Connection ----------//

    /**
     * Lazy-create a Connection object.
     */
    public Connection connection() throws SQLException {
        if (connection == null) {
            connection = connectionProvider.getConnection();
        }
        return connection;
    }

    //---------- Session-less FlushMode ----------//

    /**
     * Get the current FlushMode.
     */
    public org.hibernate.FlushMode getHibernateFlushMode() {
        return flushMode;
    }

    /**
     * Set the current flush mode.
     */
    public void setHibernateFlushMode(final org.hibernate.FlushMode flushMode) {
        if (sessionV2Proxy != null && sessionV2Proxy.hasSession()) {
            sessionV2Proxy.setRealFlushMode(FlushModeAdapter.adapt(flushMode));
        }
        if (sessionV5Proxy != null && sessionV5Proxy.hasSession()) {
            sessionV5Proxy.setRealHibernateFlushMode(flushMode);
        }
        this.flushMode = flushMode;
    }

    //---------- Synchronized Transactions ----------//

    public net.sf.hibernate.Transaction beginTransactionV2() throws net.sf.hibernate.HibernateException {
        final TransactionBridge transaction = new TransactionBridge(this);
        activeTransactions.add(transaction);
        return transaction.beginTransactionV2();
    }

    public org.hibernate.Transaction beginTransactionV5() throws org.hibernate.HibernateException {
        final TransactionBridge transaction = new TransactionBridge(this);
        activeTransactions.add(transaction);
        return transaction.beginTransactionV5();
    }

    void removeActiveTransaction(final TransactionBridge transaction) {
        activeTransactions.remove(transaction);
    }

    void clearActiveTransactions(final TransactionBridge transaction) {
        activeTransactions.remove(transaction);
    }
}
