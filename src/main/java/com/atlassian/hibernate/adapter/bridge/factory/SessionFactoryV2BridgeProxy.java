package com.atlassian.hibernate.adapter.bridge.factory;

import com.atlassian.hibernate.adapter.HibernateBridgeMode;
import net.sf.hibernate.SessionFactory;

/**
 * The v2 SessionFactory that works together with SessionFactoryBridge and delegates
 * it's implementation to a real v2 SessionFactory.
 */
class SessionFactoryV2BridgeProxy extends SessionFactoryV2orV5BridgeProxy {
    public SessionFactoryV2BridgeProxy(
            final SessionFactoryBridge sessionFactoryBridge,
            final SessionFactory sessionFactoryV2) {

        super(sessionFactoryBridge, () -> sessionFactoryV2, null, () -> HibernateBridgeMode.V2);
    }
}
