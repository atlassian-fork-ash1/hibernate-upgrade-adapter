package com.atlassian.hibernate.adapter.bridge.factory;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.adapters.session.SessionV2Adapter;
import org.apache.commons.lang3.NotImplementedException;

import java.lang.ref.WeakReference;
import java.util.WeakHashMap;
import java.util.function.Supplier;

/**
 * Provides a default HibernateBridge implementation.
 */
public class HibernateBridgeImpl implements HibernateBridge {
    private Supplier<net.sf.hibernate.SessionFactory> sessionFactoryV2;
    private Supplier<org.hibernate.SessionFactory> sessionFactoryV5;
    private Supplier<net.sf.hibernate.SessionFactory> sessionFactoryV2orV5;
    private net.sf.hibernate.SessionFactory springV2SessionFactory;
    private WeakHashMap<org.hibernate.Session, WeakReference<net.sf.hibernate.Session>> v2SessionAdapterCache =
            new WeakHashMap<>();

    public HibernateBridgeImpl setSessionFactories(
            final Supplier<net.sf.hibernate.SessionFactory> sessionFactoryV2,
            final Supplier<org.hibernate.SessionFactory> sessionFactoryV5,
            final Supplier<net.sf.hibernate.SessionFactory> sessionFactoryV2orV5) {

        this.sessionFactoryV2 = sessionFactoryV2;
        this.sessionFactoryV5 = sessionFactoryV5;
        this.sessionFactoryV2orV5 = sessionFactoryV2orV5;
        return this;
    }

    @Override
    public net.sf.hibernate.SessionFactory getV2SessionFactory() {
        if (sessionFactoryV2 == null) {
            throw new IllegalStateException("SessionFactory v2 is not available for the configured HibernateBridgeMode");
        }
        return sessionFactoryV2.get();
    }

    @Override
    public org.hibernate.SessionFactory getV5SessionFactory() {
        return sessionFactoryV5.get();
    }

    @Override
    public net.sf.hibernate.SessionFactory getV2orV5SessionFactory() {
        return sessionFactoryV2orV5.get();
    }

    @Override
    public net.sf.hibernate.SessionFactory getSpringV2SessionFactory() {
        return springV2SessionFactory != null ? springV2SessionFactory : getV2orV5SessionFactory();
    }

    @Override
    public void setSpringV2SessionFactory(final net.sf.hibernate.SessionFactory sessionFactory) {
        springV2SessionFactory = sessionFactory;
    }

    @Override
    public net.sf.hibernate.Session getV2Session(final net.sf.hibernate.Session session) {
        return session;
    }

    @Override
    public org.hibernate.Session getV5Session(final net.sf.hibernate.Session session) {
        try {
            return (org.hibernate.Session) session.get(org.hibernate.Session.class, null);
        } catch (final net.sf.hibernate.HibernateException | NullPointerException ex) {
            throw new IllegalStateException("session is not a " + SessionV2Adapter.class.getSimpleName());
        }
    }

    @Override
    public synchronized net.sf.hibernate.Session getV2orV5Session(final org.hibernate.Session session) {
        WeakReference<net.sf.hibernate.Session> ref = v2SessionAdapterCache.get(session);
        net.sf.hibernate.Session result = ref != null ? ref.get() : null;
        if (result == null) {
            result = SessionV2Adapter.adapt(session, sessionFactoryV2orV5.get());

            // cache the result, so that the same instance is returned each time
            v2SessionAdapterCache.put(session, new WeakReference<>(result));
        }
        return result;
    }

    @Override
    public net.sf.hibernate.Session getV2Session(final org.hibernate.Session session) {
        throw new NotImplementedException("getV2Session is not supported without using a session bridging mode");
    }
}
