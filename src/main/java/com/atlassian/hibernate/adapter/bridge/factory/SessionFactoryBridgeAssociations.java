package com.atlassian.hibernate.adapter.bridge.factory;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.reflection.SessionFactoryImplV2Reflection;
import com.atlassian.hibernate.adapter.util.HibernateBridgeHolder;
import com.atlassian.hibernate.util.ThrowableUtil;

/**
 * Associates HibernateBridge objects with v2 and v5 SessionFactory objects.
 * <p>
 * This is done in such a way that uses Session and SessionFactory interfaces only without
 * casting to a concrete class or special interface, for compatibility with
 * java proxies, Spring proxies, and OSGI.
 */
public final class SessionFactoryBridgeAssociations {

    private SessionFactoryBridgeAssociations() { }

    /**
     * Get an instance of the HibernateBridge from a SessionFactory.
     */
    public static HibernateBridge getHibernateBridge(final org.hibernate.SessionFactory sessionFactory) {
        if (sessionFactory == null)
            return null;

        // the special key=HibernateBridge.class property is set in associate() below
        return (HibernateBridge) ((org.hibernate.engine.spi.SessionFactoryImplementor) sessionFactory)
                .getProperties().get(HibernateBridge.class.getName());
    }

    /**
     * Get an instance of the HibernateBridge from a SessionFactory.
     */
    public static HibernateBridge getHibernateBridge(final net.sf.hibernate.SessionFactory sessionFactory) {
        if (sessionFactory == null)
            return null;

        // getPersister(HibernateBridge.class) is implemented by SessionFactoryV2orV5BridgeProxy
        try {
            return (HibernateBridge) ((net.sf.hibernate.engine.SessionFactoryImplementor) sessionFactory)
                    .getPersister(HibernateBridge.class.getName()).getVersion(null);
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    /**
     * Associate the v2 SessionFactory with a HibernateBridge, by adding to the
     * internal classPersistersByName map.
     * <p>
     * This is required because SessionFactory may be intercepted by Spring, and so it's not possible
     * simply implement HibernateBridge or cast to one of the adapter implementations.
     */
    @SuppressWarnings("unchecked")
    public static net.sf.hibernate.SessionFactory associate(
            final net.sf.hibernate.SessionFactory sessionFactory,
            final HibernateBridge hibernateBridge) {

        if (!(sessionFactory instanceof net.sf.hibernate.impl.SessionFactoryImpl)) {
            throw new IllegalArgumentException("Expected a real net.sf.hibernate.impl.SessionFactoryImpl");
        }

        final net.sf.hibernate.impl.SessionFactoryImpl impl = (net.sf.hibernate.impl.SessionFactoryImpl) sessionFactory;
        SessionFactoryImplV2Reflection.getClassPersistersByName(impl)
                .put(HibernateBridge.class.getName(), new HibernateBridgeHolder(hibernateBridge));
        return sessionFactory;
    }

    /**
     * Associate the v5 SessionFactory with a HibernateBridge, by adding it as a property.
     * It's nice that the v5 SessionFactory.getProperties is public, because it allows us to
     * avoid using reflection.
     */
    public static org.hibernate.SessionFactory associate(
            final org.hibernate.SessionFactory sessionFactory,
            final HibernateBridge hibernateBridge) {

        ((org.hibernate.engine.spi.SessionFactoryImplementor) sessionFactory).getProperties().put(
                HibernateBridge.class.getName(), hibernateBridge);
        return sessionFactory;
    }
}
