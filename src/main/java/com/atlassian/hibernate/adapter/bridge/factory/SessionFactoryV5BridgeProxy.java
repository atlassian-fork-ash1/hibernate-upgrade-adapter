package com.atlassian.hibernate.adapter.bridge.factory;

import com.atlassian.hibernate.adapter.HibernateBridgeMode;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.bridge.session.SessionBridgeFactory;
import com.atlassian.hibernate.adapter.session.SessionBuilderV5NotImplemented;
import org.hibernate.EmptyInterceptor;
import org.hibernate.HibernateException;
import org.hibernate.Interceptor;
import org.hibernate.Session;
import org.hibernate.SessionBuilder;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionBuilderImplementor;
import org.hibernate.engine.spi.SessionFactoryDelegatingImpl;
import org.hibernate.engine.spi.SessionFactoryImplementor;

import java.sql.Connection;

/**
 * The v5 SessionFactory that works together with SessionFactoryBridge and delegates
 * it's implementation to a real v5 SessionFactory.
 */
class SessionFactoryV5BridgeProxy extends SessionFactoryDelegatingImpl {
    private final SessionFactoryBridge sessionFactoryBridge;
    private final SessionBridgeFactory sessionBridgeFactory;

    public SessionFactoryV5BridgeProxy(final SessionFactoryBridge sessionFactoryBridge, final SessionFactory sessionFactory) {
        super((SessionFactoryImplementor) sessionFactory);
        this.sessionFactoryBridge = sessionFactoryBridge;
        this.sessionBridgeFactory = new SessionBridgeFactory(sessionFactoryBridge);
    }

    @Override
    public void close() throws HibernateException {
        try {
            sessionFactoryBridge.close();
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    //---------- openSession ----------//

    @Override
    public Session openSession() throws HibernateException {
        return withOptions().openSession();
    }

    @Override
    public SessionBuilderImplementor withOptions() {
        return new SessionBuilderV5NotImplemented() {
            private Interceptor interceptor;
            private Connection connection;

            @Override
            public Session openSession() {
                HibernateBridgeMode bridgeMode = sessionFactoryBridge.getBridgeMode();

                // provide a session bridge (session v2 / v5 side-by-side mode)
                if (bridgeMode.isSessionBridgeEnabled()) {
                    return sessionBridgeFactory.openSession(bridgeMode, connection, interceptor).getV5SessionProxy();
                }

                return
                        sessionFactoryBridge.getSessionFactoryV5()
                        .withOptions()
                        .connection(connection)
                        .interceptor(interceptor)
                        .openSession();
            }

            @Override
            public SessionBuilder interceptor(final Interceptor interceptor) {
                this.interceptor = interceptor;
                return this;
            }

            @Override
            public SessionBuilder noInterceptor() {
                this.interceptor = EmptyInterceptor.INSTANCE;
                return this;
            }

            @Override
            public SessionBuilder connection(final Connection connection) {
                this.connection = connection;
                return this;
            }
        };
    }
}
