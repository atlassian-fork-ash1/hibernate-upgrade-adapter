package com.atlassian.hibernate.adapter.bridge.session;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.FlushModeAdapter;
import com.atlassian.hibernate.util.TransactionHelper;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.persistence.PersistenceException;
import javax.transaction.Synchronization;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * The v2 / v5 (onwards) session Transaction bridge that
 * begins, commits and rolls v2 and v5 transactions together.
 */
class TransactionBridge {
    private static final Log LOG = LogFactory.getLog(TransactionBridge.class);

    private final SessionBridge sessionBridge;
    private net.sf.hibernate.Transaction transactionV2;
    private org.hibernate.Transaction transactionV5;
    private boolean toggleAutoCommit;
    private boolean isFromV2Begin;
    private boolean commitFailed;

    public TransactionBridge(final SessionBridge sessionBridge) {
        this.sessionBridge = sessionBridge;
    }

    /**
     * Begin a transaction and return a v2 Transaction interface.
     */
    public net.sf.hibernate.Transaction beginTransactionV2() throws net.sf.hibernate.HibernateException {
        isFromV2Begin = true;
        try {
            beginImpl();
        } catch (final SQLException ex) {
            throw new net.sf.hibernate.TransactionException("Begin failed with SQL exception: ", ex);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }

        return new net.sf.hibernate.Transaction() {
            private boolean rolledBack;
            private boolean committed;
            private boolean commitFailed;

            @Override
            public void commit() throws net.sf.hibernate.HibernateException {
                try {
                    TransactionBridge.this.commitImpl();
                    committed = true;
                } catch (final net.sf.hibernate.HibernateException ex) {
                    commitFailed = true;
                    throw ex;
                } catch (final PersistenceException ex) {
                    commitFailed = true;
                    throw HibernateExceptionAdapter.adapt(ex);
                } catch (final SQLException ex) {
                    commitFailed = true;
                    LOG.error("Commit failed", ex);
                    throw new net.sf.hibernate.TransactionException("Commit failed with SQL exception: ", ex);
                }
            }

            @Override
            public void rollback() throws net.sf.hibernate.HibernateException {
                try {
                    TransactionBridge.this.rollbackImpl();
                    rolledBack = !commitFailed;
                } catch (final PersistenceException ex) {
                    throw HibernateExceptionAdapter.adapt(ex);
                } catch (final SQLException ex) {
                    LOG.error("Rollback failed", ex);
                    throw new net.sf.hibernate.TransactionException("Rollback failed with SQL exception: ", ex);
                }
            }

            @Override
            public boolean wasRolledBack() throws net.sf.hibernate.HibernateException {
                return rolledBack;
            }

            @Override
            public boolean wasCommitted() throws net.sf.hibernate.HibernateException {
                return committed;
            }
        };
    }

    /**
     * Begin a transaction and return a v5 Transaction interface.
     */
    public org.hibernate.Transaction beginTransactionV5() throws org.hibernate.HibernateException {
        try {
            beginImpl();
        } catch (final SQLException ex) {
            throw HibernateExceptionAdapter.toV5HibernateException(ex, "Failed to begin transaction");
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }

        return new org.hibernate.Transaction() {
            @Override
            public void begin() {
                throw new NotImplementedException("begin() not implemented");
            }

            @Override
            public void commit() {
                try {
                    commitImpl();
                } catch (final net.sf.hibernate.HibernateException ex) {
                    throw HibernateExceptionAdapter.adapt(ex);
                } catch (final SQLException ex) {
                    throw new org.hibernate.TransactionException("Unable to commit against JDBC Connection", ex);
                }
            }

            @Override
            public void rollback() {
                try {
                    rollbackImpl();
                } catch (final net.sf.hibernate.HibernateException ex) {
                    throw HibernateExceptionAdapter.adapt(ex);
                } catch (final SQLException ex) {
                    throw new org.hibernate.HibernateException("Unable to rollback against JDBC Connection", ex);
                }
            }

            @Override
            public org.hibernate.resource.transaction.spi.TransactionStatus getStatus() {
                throw new NotImplementedException("getStatus not implemented");
            }

            @Override
            public void registerSynchronization(final Synchronization synchronization)
                    throws org.hibernate.HibernateException {

                throw new NotImplementedException("registerSynchronization not implemented");
            }

            @Override
            public int getTimeout() {
                throw new NotImplementedException("getTimeout not implemented");
            }

            @Override
            public void setTimeout(final int seconds) {
                throw new NotImplementedException("setTimeout not implemented");
            }

            @Override
            public boolean getRollbackOnly() {
                throw new NotImplementedException("getRollbackOnly not implemented");
            }

            @Override
            public void setRollbackOnly() {
                throw new NotImplementedException("setRollbackOnly not implemented");
            }

            @Override
            public boolean isActive() {
                throw new NotImplementedException("isActive not implemented");
            }
        };
    }

    private void beginImpl()
            throws net.sf.hibernate.HibernateException, org.hibernate.HibernateException, SQLException {

        final Connection connection = sessionBridge.connection();
        toggleAutoCommit = connection.getAutoCommit();
        if (toggleAutoCommit) {
            connection.setAutoCommit(false);
        }

        final net.sf.hibernate.Session sessionV2 = sessionBridge.getRealV2SessionNoCreate();
        final org.hibernate.Session sessionV5 = sessionBridge.getRealV5SessionNoCreate();

        if (sessionV2 != null) {
            setTransactionV2(sessionV2);
        }
        if (sessionV5 != null) {
            setTransactionV5(sessionV5);
        }
    }

    private void commitImpl()
            throws net.sf.hibernate.HibernateException, org.hibernate.HibernateException, SQLException {

        sessionBridge.removeActiveTransaction(this);
        try {
            // coordinate a commit between both sessions
            if (transactionV2 != null && transactionV5 != null) {
                final net.sf.hibernate.Session s2 = sessionBridge.getRealV2SessionNoCreate();
                final org.hibernate.Session s5 = sessionBridge.getRealV5SessionNoCreate();

                // flush, as per net.sf.hibernate.JDBCTransaction and v5 equivalent
                if (sessionBridge.getHibernateFlushMode() != org.hibernate.FlushMode.MANUAL) {
                    s2.flush();
                    s5.flush();
                }

                try {
                    // don't flush during hibernate's commit, this was already done above
                    s2.setFlushMode(net.sf.hibernate.FlushMode.NEVER);
                    s5.setHibernateFlushMode(org.hibernate.FlushMode.MANUAL);

                    // v5 has pre-commit hooks that should run, this will also call connection.commit()
                    final boolean savedAutoCommit = sessionBridge.connection().getAutoCommit();
                    transactionV5.commit();
                    sessionBridge.connection().setAutoCommit(savedAutoCommit);

                    // only v2 has post-commit hooks, so we run it last
                    transactionV2.commit();
                } finally {
                    s2.setFlushMode(FlushModeAdapter.adapt(sessionBridge.getHibernateFlushMode()));
                    s5.setHibernateFlushMode(sessionBridge.getHibernateFlushMode());
                }
            } else if (transactionV2 != null) {
                transactionV2.commit();
            } else if (transactionV5 != null) {
                transactionV5.commit();
            } else {
                sessionBridge.connection().commit();
            }
        } catch (Exception ex) {
            commitFailed = true;
            throw ex;
        } finally {
            toggleAutoCommit();
        }
    }

    private void rollbackImpl()
            throws net.sf.hibernate.HibernateException, org.hibernate.HibernateException, SQLException {

        sessionBridge.clearActiveTransactions(this);
        try {
            if (transactionV2 == null && transactionV5 == null) {
                sessionBridge.connection().rollback();
            }
            try {
                if (transactionV2 != null) {
                    // handle the case where the v5 commit fails, but the v2 commit isn't attempted,
                    // and so v2 JDBCTransaction.commitFailed isn't set to true
                    if (!commitFailed) {
                        transactionV2.rollback();
                    }
                }
            } finally {
                if (transactionV5 != null) {
                    transactionV5.rollback();
                }
            }
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        } catch (final SQLException ex) {
            throw new org.hibernate.HibernateException("Unable to rollback against JDBC Connection", ex);
        } finally {
            toggleAutoCommit();
        }
    }

    public void setTransactionV2(final net.sf.hibernate.Session session) throws net.sf.hibernate.HibernateException {
        if (transactionV2 != null) {
            throw new IllegalStateException("v2 transaction already set");
        }
        transactionV2 = session.beginTransaction();
    }

    public void setTransactionV5(final org.hibernate.Session session) throws org.hibernate.HibernateException {
        if (transactionV5 != null) {
            throw new IllegalStateException("v2 transaction already set");
        }
        transactionV5 =
                isFromV2Begin
                ? TransactionHelper.beginTransactionCommitNested(session)
                : session.beginTransaction();
    }

    private void toggleAutoCommit() {
        if (toggleAutoCommit) {
            try {
                sessionBridge.connection().setAutoCommit(true);
            } catch (final Exception ex) {
                LOG.error("Could not toggle autocommit", ex);
            }
        }
    }
}
