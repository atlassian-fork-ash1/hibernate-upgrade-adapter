package com.atlassian.hibernate.adapter.bridge.factory;

import com.atlassian.hibernate.adapter.HibernateBridgeMode;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.interceptor.InterceptorV5Adapter;
import com.atlassian.hibernate.adapter.adapters.session.SessionFactoryV2Adapter;
import com.atlassian.hibernate.adapter.bridge.session.SessionBridge;
import com.atlassian.hibernate.adapter.bridge.session.SessionBridgeFactory;
import net.sf.hibernate.Databinder;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Interceptor;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.Session;
import net.sf.hibernate.cache.QueryCache;
import net.sf.hibernate.collection.CollectionPersister;
import net.sf.hibernate.connection.ConnectionProvider;
import net.sf.hibernate.dialect.Dialect;
import net.sf.hibernate.exception.SQLExceptionConverter;
import net.sf.hibernate.metadata.ClassMetadata;
import net.sf.hibernate.metadata.CollectionMetadata;
import net.sf.hibernate.persister.ClassPersister;
import net.sf.hibernate.type.Type;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.persistence.PersistenceException;
import javax.transaction.TransactionManager;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Map;
import java.util.function.Supplier;

/**
 * The v2 SessionFactory that works together with SessionFactoryBridge and delegates
 * it's implementation to either.
 * - a real v2 SessionFactory implementation
 * - a real v5 SessionFactory implementation, through a v5 to v2 interface adapter
 */
class SessionFactoryV2orV5BridgeProxy extends SessionFactoryV2Adapter {
    private final SessionFactoryBridge sessionFactoryBridge;
    private final SessionBridgeFactory sessionBridgeFactory;
    private final Supplier<net.sf.hibernate.SessionFactory> sessionFactoryV2;
    private final Supplier<org.hibernate.SessionFactory> sessionFactoryV5;
    private final Supplier<HibernateBridgeMode> bridgeMode;

    @SuppressWarnings("unchecked")
    public SessionFactoryV2orV5BridgeProxy(
            final SessionFactoryBridge sessionFactoryBridge,
            final Supplier<net.sf.hibernate.SessionFactory> sessionFactoryV2,
            final Supplier<org.hibernate.SessionFactory> sessionFactoryV5,
            final Supplier<HibernateBridgeMode> bridgeMode) {

        super(null, sessionFactoryBridge, null);
        this.sessionFactoryBridge = sessionFactoryBridge;
        this.sessionBridgeFactory = new SessionBridgeFactory(sessionFactoryBridge);
        this.sessionFactoryV2 = sessionFactoryV2;
        this.sessionFactoryV5 = sessionFactoryV5;
        this.bridgeMode = bridgeMode;
    }

    protected net.sf.hibernate.engine.SessionFactoryImplementor getSessionFactoryV2() {
        return (net.sf.hibernate.engine.SessionFactoryImplementor) sessionFactoryV2.get();
    }

    protected org.hibernate.engine.spi.SessionFactoryImplementor getSessionFactoryV5() {
        return (org.hibernate.engine.spi.SessionFactoryImplementor) sessionFactoryV5.get();
    }

    @Override
    public ClassPersister getPersister(final String className) throws MappingException {
        return
                callV2()
                        ? getSessionFactoryV2().getPersister(className)
                        : super.getPersister(className);
    }

    @Override
    public void close() throws HibernateException {
        try {
            sessionFactoryBridge.close();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    //---------- openSession ----------//

    @Override
    public Session openSession() throws HibernateException {
        final HibernateBridgeMode bridgeMode = this.bridgeMode.get();
        if (bridgeMode == HibernateBridgeMode.V2) {
            return getSessionFactoryV2().openSession();
        }
        if (bridgeMode == HibernateBridgeMode.V5_ADAPTER) {
            return sessionFactoryBridge.getV2orV5Session(setupV5Session(sessionFactoryV5.get().openSession()));
        }

        // provide a session bridge (session v2 / v5 side-by-side mode)
        if (bridgeMode.isSessionBridgeEnabled()) {
            return getV2orV5SessionBridge(bridgeMode, sessionBridgeFactory.openSession(bridgeMode));
        }
        throw new IllegalArgumentException("Unexpected bridge mode: " + bridgeMode);
    }

    @Override
    public Session openSession(final Connection connection) {
        final HibernateBridgeMode bridgeMode = this.bridgeMode.get();

        // no session bridge
        if (bridgeMode == HibernateBridgeMode.V2) {
            return getSessionFactoryV2().openSession(connection);
        }
        if (bridgeMode == HibernateBridgeMode.V5_ADAPTER) {
            final org.hibernate.Session session =
                    sessionFactoryV5.get().withOptions()
                            .connection(connection)
                            .openSession();
            setupV5Session(session);
            return sessionFactoryBridge.getV2orV5Session(session);
        }

        // provide a session bridge (session v2 / v5 side-by-side mode)
        if (bridgeMode.isSessionBridgeEnabled()) {
            return getV2orV5SessionBridge(bridgeMode, sessionBridgeFactory.openSession(bridgeMode, connection));
        }
        throw new IllegalArgumentException("Unexpected bridge mode: " + bridgeMode);
    }

    @Override
    public Session openSession(final Interceptor interceptor) throws HibernateException {
        final HibernateBridgeMode bridgeMode = this.bridgeMode.get();

        // no session bridge
        if (bridgeMode == HibernateBridgeMode.V2) {
            return getSessionFactoryV2().openSession(interceptor);
        }
        if (bridgeMode == HibernateBridgeMode.V5_ADAPTER) {
            final org.hibernate.Session session =
                    sessionFactoryV5.get().withOptions()
                            .interceptor(InterceptorV5Adapter.adapt(interceptor))
                            .openSession();
            setupV5Session(session);
            return sessionFactoryBridge.getV2orV5Session(session);
        }

        // provide a session bridge (session v2 / v5 side-by-side mode)
        if (bridgeMode.isSessionBridgeEnabled()) {
            return getV2orV5SessionBridge(bridgeMode, sessionBridgeFactory.openSession(bridgeMode, interceptor));
        }
        throw new IllegalArgumentException("Unexpected bridge mode: " + bridgeMode);
    }

    @Override
    public Session openSession(final Connection connection, final Interceptor interceptor) {
        final HibernateBridgeMode bridgeMode = this.bridgeMode.get();

        // no session bridge
        if (bridgeMode == HibernateBridgeMode.V2) {
            return getSessionFactoryV2().openSession(connection, interceptor);
        }
        if (bridgeMode == HibernateBridgeMode.V5_ADAPTER) {
            final org.hibernate.Session session =
                    sessionFactoryV5.get().withOptions()
                            .connection(connection)
                            .interceptor(InterceptorV5Adapter.adapt(interceptor))
                            .openSession();
            setupV5Session(session);
            return sessionFactoryBridge.getV2orV5Session(session);
        }

        // provide a session bridge (session v2 / v5 side-by-side mode)
        if (bridgeMode.isSessionBridgeEnabled()) {
            return getV2orV5SessionBridge(
                    bridgeMode, sessionBridgeFactory.openSession(bridgeMode, connection, interceptor));
        }
        throw new IllegalArgumentException("Unexpected bridge mode: " + bridgeMode);
    }

    // Session v2 / v5 side-by-side mode
    private Session getV2orV5SessionBridge(
            final HibernateBridgeMode bridgeMode,
            final SessionBridge sessionBridge) {

        return
                callV2(bridgeMode)
                ? sessionBridge.getV2SessionProxy()
                : sessionBridge.getV2SessionAdapter();
    }

    //---------- Call sessionFactoryV2 or SessionFactoryV2Adapter (base class) ----------//

    @Override
    public ClassPersister getPersister(final Class clazz) throws MappingException {
        return
                callV2()
                ? getSessionFactoryV2().getPersister(clazz)
                : super.getPersister(clazz);
    }

    @Override
    public CollectionPersister getCollectionPersister(final String role) throws MappingException {
        return
                callV2()
                ? getSessionFactoryV2().getCollectionPersister(role)
                : super.getCollectionPersister(role);
    }

    @Override
    public boolean isOuterJoinedFetchEnabled() {
        return
                callV2()
                ? getSessionFactoryV2().isOuterJoinedFetchEnabled()
                : super.isOuterJoinedFetchEnabled();
    }

    @Override
    public boolean isScrollableResultSetsEnabled() {
        return
                callV2()
                ? getSessionFactoryV2().isScrollableResultSetsEnabled()
                : super.isScrollableResultSetsEnabled();
    }

    @Override
    public boolean isGetGeneratedKeysEnabled() {
        return
                callV2()
                ? getSessionFactoryV2().isGetGeneratedKeysEnabled()
                : super.isGetGeneratedKeysEnabled();
    }

    @Override
    public String getDefaultSchema() {
        return
                callV2()
                ? getSessionFactoryV2().getDefaultSchema()
                : super.getDefaultSchema();
    }

    @Override
    public Dialect getDialect() {
        return
                callV2()
                ? getSessionFactoryV2().getDialect()
                : super.getDialect();
    }

    @Override
    public Type[] getReturnTypes(final String queryString) throws HibernateException {
        return
                callV2()
                ? getSessionFactoryV2().getReturnTypes(queryString)
                : super.getReturnTypes(queryString);
    }

    @Override
    public ConnectionProvider getConnectionProvider() {
        return
                callV2()
                ? getSessionFactoryV2().getConnectionProvider()
                : super.getConnectionProvider();
    }

    @Override
    public String[] getImplementors(final Class clazz) {
        return
                callV2()
                ? getSessionFactoryV2().getImplementors(clazz)
                : super.getImplementors(clazz);
    }

    @Override
    public String getImportedClassName(final String name) {
        return
                callV2()
                ? getSessionFactoryV2().getImportedClassName(name)
                : super.getImportedClassName(name);
    }

    @Override
    public int getJdbcBatchSize() {
        return
                callV2()
                ? getSessionFactoryV2().getJdbcBatchSize()
                : super.getJdbcBatchSize();
    }

    @Override
    public Integer getJdbcFetchSize() {
        return
                callV2()
                ? getSessionFactoryV2().getJdbcFetchSize()
                : super.getJdbcFetchSize();
    }

    @Override
    public Integer getMaximumFetchDepth() {
        return
                callV2()
                ? getSessionFactoryV2().getMaximumFetchDepth()
                : super.getMaximumFetchDepth();
    }

    @Override
    public TransactionManager getTransactionManager() {
        return
                callV2()
                ? getSessionFactoryV2().getTransactionManager()
                : super.getTransactionManager();
    }

    @Override
    public boolean isShowSqlEnabled() {
        return
                callV2()
                ? getSessionFactoryV2().isShowSqlEnabled()
                : super.isShowSqlEnabled();
    }

    @Override
    public QueryCache getQueryCache() {
        return
                callV2()
                ? getSessionFactoryV2().getQueryCache()
                : super.getQueryCache();
    }

    @Override
    public QueryCache getQueryCache(final String regionName) throws HibernateException {
        return
                callV2()
                ? getSessionFactoryV2().getQueryCache()
                : super.getQueryCache();
    }

    @Override
    public boolean isQueryCacheEnabled() {
        return
                callV2()
                ? getSessionFactoryV2().isQueryCacheEnabled()
                : super.isQueryCacheEnabled();
    }

    @Override
    public boolean isJdbcBatchVersionedData() {
        return
                callV2()
                ? getSessionFactoryV2().isJdbcBatchVersionedData()
                : super.isJdbcBatchVersionedData();
    }

    @Override
    public boolean isWrapResultSetsEnabled() {
        return
                callV2()
                ? getSessionFactoryV2().isWrapResultSetsEnabled()
                : super.isWrapResultSetsEnabled();
    }

    @Override
    public Type getIdentifierType(final Class persistentClass) throws MappingException {
        return
                callV2()
                ? getSessionFactoryV2().getIdentifierType(persistentClass)
                : super.getIdentifierType(persistentClass);
    }

    @Override
    public String getIdentifierPropertyName(final Class persistentClass) throws MappingException {
        return
                callV2()
                ? getSessionFactoryV2().getIdentifierPropertyName(persistentClass)
                : super.getIdentifierPropertyName(persistentClass);
    }

    @Override
    public Type getPropertyType(final Class persistentClass, final String propertyName)
            throws MappingException {
        return
                callV2()
                ? getSessionFactoryV2().getPropertyType(persistentClass, propertyName)
                : super.getPropertyType(persistentClass, propertyName);
    }

    @Override
    public Databinder openDatabinder() throws HibernateException {
        return
                callV2()
                ? getSessionFactoryV2().openDatabinder()
                : super.openDatabinder();
    }

    @Override
    public ClassMetadata getClassMetadata(final Class persistentClass) throws HibernateException {
        return
                callV2()
                ? getSessionFactoryV2().getClassMetadata(persistentClass)
                : super.getClassMetadata(persistentClass);
    }

    @Override
    public CollectionMetadata getCollectionMetadata(final String roleName) throws HibernateException {
        return
                callV2()
                ? getSessionFactoryV2().getCollectionMetadata(roleName)
                : super.getCollectionMetadata(roleName);
    }

    @Override
    public Map getAllClassMetadata() throws HibernateException {
        return
                callV2()
                ? getSessionFactoryV2().getAllClassMetadata()
                : super.getAllClassMetadata();
    }

    @Override
    public Map getAllCollectionMetadata() throws HibernateException {
        return
                callV2()
                ? getSessionFactoryV2().getAllCollectionMetadata()
                : super.getAllCollectionMetadata();
    }

    @Override
    public void evict(final Class persistentClass) throws HibernateException {
        if (callV2())
            getSessionFactoryV2().evict(persistentClass);
        else
            super.evict(persistentClass);
    }

    @Override
    public void evict(final Class persistentClass, final Serializable id) throws HibernateException {
        if (callV2())
            getSessionFactoryV2().evict(persistentClass, id);
        else
            super.evict(persistentClass, id);
    }

    @Override
    public void evictCollection(final String roleName) throws HibernateException {
        if (callV2())
            getSessionFactoryV2().evictCollection(roleName);
        else
            super.evictCollection(roleName);
    }

    @Override
    public void evictCollection(final String roleName, final Serializable id) throws HibernateException {
        if (callV2())
            getSessionFactoryV2().evictCollection(roleName, id);
        else
            super.evictCollection(roleName, id);
    }

    @Override
    public void evictQueries() throws HibernateException {
        if (callV2())
            getSessionFactoryV2().evictQueries();
        else
            super.evictQueries();
    }

    @Override
    public void evictQueries(final String cacheRegion) throws HibernateException {
        if (callV2())
            getSessionFactoryV2().evictQueries(cacheRegion);
        else
            super.evictQueries(cacheRegion);
    }

    @Override
    public SQLExceptionConverter getSQLExceptionConverter() {
        return
                callV2()
                ? getSessionFactoryV2().getSQLExceptionConverter()
                : super.getSQLExceptionConverter();
    }

    @Override
    public Reference getReference() throws NamingException {
        return
                callV2()
                ? getSessionFactoryV2().getReference()
                : super.getReference();
    }

    private boolean callV2() {
        return callV2(bridgeMode.get());
    }

    private boolean callV2(final HibernateBridgeMode bridgeMode) {
        return bridgeMode.isV2();
    }
}
