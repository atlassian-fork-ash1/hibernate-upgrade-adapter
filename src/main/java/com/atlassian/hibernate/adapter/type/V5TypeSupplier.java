package com.atlassian.hibernate.adapter.type;

public interface V5TypeSupplier {
    org.hibernate.type.Type getV5Type();
}
