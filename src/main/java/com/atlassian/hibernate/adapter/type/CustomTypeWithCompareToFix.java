package com.atlassian.hibernate.adapter.type;

import org.hibernate.MappingException;
import org.hibernate.type.CustomType;
import org.hibernate.usertype.UserType;

/**
 * A CustomType that provides a default compare method for user types
 * that don't implement the Comparable interface.
 */
public class CustomTypeWithCompareToFix extends CustomType {
    public CustomTypeWithCompareToFix(final UserType userType) throws MappingException {
        super(userType);
    }

    @Override
    public int compare(final Object x, final Object y) {
        // this can be called unnecessarily by EntityAction.compareTo
        // from org.hibernate.engine.spi.ExecutableList#222
        if (!(x instanceof Comparable)) {
            return 0;
        }
        return super.compare(x, y);
    }
}
