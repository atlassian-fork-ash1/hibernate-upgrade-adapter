package com.atlassian.hibernate.adapter.type;

import org.hibernate.AssertionFailure;
import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.EnhancedUserType;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

/**
 * A UserType implementation for the deprecated v2 PersistentEnum interface.
 */
public class PersistentEnumUserType implements UserType, EnhancedUserType {
    private final Class enumClass;
    private final Method method;

    @SuppressWarnings("unchecked")
    public PersistentEnumUserType(final Class enumClass) throws MappingException {
        this.enumClass = enumClass;
        try {
            method = enumClass.getDeclaredMethod("fromInt", int.class);
            if (!net.sf.hibernate.util.ReflectHelper.isPublic(enumClass, method)) {
                method.setAccessible(true);
            }
        } catch (final NoSuchMethodException ex) {
            throw new MappingException("PersistentEnum class did not implement fromInt(int): " + enumClass.getName());
        }
    }

    @Override
    public Object assemble(final Serializable cached, final Object owner) throws HibernateException {
        if (cached == null) {
            return null;
        }
        return getInstance((Integer) cached);
    }

    @Override
    @SuppressWarnings("deprecation")
    public Serializable disassemble(final Object value) throws HibernateException {
        if (value == null) {
            return null;
        }
        return ((net.sf.hibernate.PersistentEnum) value).toInt();
    }

    @Override
    public Object deepCopy(final Object value) throws HibernateException {
        return value;
    }

    @Override
    @SuppressWarnings("deprecation")
    public boolean equals(final Object x, final Object y) throws HibernateException {
        if (x == y) {
            return true;
        }
        return x != null
               && y != null && x.getClass() == y.getClass()
               && ((net.sf.hibernate.PersistentEnum) x).toInt() == ((net.sf.hibernate.PersistentEnum) y).toInt();
    }

    @Override
    public int hashCode(final Object x) throws HibernateException {
        return x == null ? 0 : x.hashCode();
    }

    @Override
    public boolean isMutable() {
        return false;
    }

    @Override
    @SuppressWarnings("deprecation")
    public Object nullSafeGet(final ResultSet rs, final String[] names, final SharedSessionContractImplementor session, final Object owner)
            throws HibernateException, SQLException {

        final int code = rs.getInt(names[0]);
        return rs.wasNull() ? null : getInstance(code);
    }

    private Object getInstance(final int code) throws HibernateException {
        try {
            return method.invoke(null, code);
        } catch (final IllegalArgumentException ex) {
            throw new AssertionFailure("Could not invoke fromInt() from PersistentEnumType", ex);
        } catch (final InvocationTargetException ex) {
            throw new HibernateException("InvocationTargetException occurred inside fromInt()", ex);
        } catch (final IllegalAccessException ex) {
            throw new HibernateException("IllegalAccessException occurred calling fromInt()", ex);
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    public void nullSafeSet(
            final PreparedStatement st,
            final Object value,
            final int index,
            final SharedSessionContractImplementor session) throws HibernateException, SQLException {

        if (value == null)
            st.setNull(index, Types.INTEGER);
        else
            st.setInt(index, ((net.sf.hibernate.PersistentEnum) value).toInt());
    }

    @Override
    public Object replace(final Object original, final Object target, final Object owner)
            throws HibernateException {

        return original;
    }

    @Override
    public Class returnedClass() {
        return enumClass;
    }

    @Override
    public int[] sqlTypes() {
        return new int[]{Types.SMALLINT};
    }

    @Override
    @SuppressWarnings("deprecation")
    public String objectToSQLString(final Object value) {
        return Integer.toString(((net.sf.hibernate.PersistentEnum) value).toInt());
    }

    @Override
    @SuppressWarnings("deprecation")
    public String toXMLString(final Object value) {
        return Integer.toString(((net.sf.hibernate.PersistentEnum) value).toInt());
    }

    @Override
    @SuppressWarnings("deprecation")
    public Object fromXMLString(final String xmlValue) {
        return getInstance(new Integer(xmlValue));
    }
}
