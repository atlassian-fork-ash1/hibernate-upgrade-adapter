package com.atlassian.hibernate.adapter.type.invalid;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.UserType;
import org.apache.commons.lang3.NotImplementedException;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Used by CustomTypeV2Adapter where a UserType must be instantiated in it's constructor.
 */
public class InvalidUserType implements UserType {

    private static NotImplementedException notImplemented() {
        return new NotImplementedException("Operation not valid on InvalidUserType");
    }

    @Override
    public int[] sqlTypes() {
        return new int[0];
    }

    @Override
    public Class returnedClass() {
        return Serializable.class;
    }

    @Override
    public boolean equals(final Object x, final Object y) throws HibernateException {
        throw notImplemented();
    }

    @Override
    public Object nullSafeGet(final ResultSet rs, final String[] names, final Object owner) throws HibernateException, SQLException {
        throw notImplemented();
    }

    @Override
    public void nullSafeSet(final PreparedStatement st, final Object value, final int index) throws HibernateException, SQLException {
        throw notImplemented();
    }

    @Override
    public Object deepCopy(final Object value) throws HibernateException {
        throw notImplemented();
    }

    @Override
    public boolean isMutable() {
        throw notImplemented();
    }
}
