package com.atlassian.hibernate.adapter.type;

import com.atlassian.hibernate.adapter.adapters.type.UserTypeV5Adapter;
import com.atlassian.hibernate.adapter.reflection.CustomTypeV2Reflection;
import org.hibernate.boot.model.TypeContributions;
import org.hibernate.boot.model.TypeContributor;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.usertype.UserType;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Contributes hibernate UserType and PersistentEnumType objects upgraded
 * from v2 to v5 (onwards) using UserTypeV5Adapter and PersistentEnumUserType.
 */
public class V2ToV5TypeContributor implements TypeContributor {
    private final net.sf.hibernate.cfg.Configuration config;

    public V2ToV5TypeContributor(final net.sf.hibernate.cfg.Configuration config) {
        this.config = config;
    }

    @Override
    public void contribute(final TypeContributions typeContributions, final ServiceRegistry serviceRegistry) {
        for (final Map.Entry<String, UserType> userType : getPropertyUserTypes().entrySet()) {
            typeContributions.contributeType(new CustomTypeWithCompareToFix(userType.getValue()), userType.getKey());
        }
    }

    /**
     * Get the UserType objects returned from properties on mapped classes.
     */
    @SuppressWarnings("unchecked")
    private Map<String, UserType> getPropertyUserTypes() {
        final LinkedHashMap<String, UserType> list = new LinkedHashMap<>();
        config.getClassMappings().forEachRemaining(clazzObj -> {
            final net.sf.hibernate.mapping.PersistentClass clazz =
                    (net.sf.hibernate.mapping.PersistentClass) clazzObj;
            getPropertyUserTypes(list, clazz);
        });
        return list;
    }

    @SuppressWarnings("unchecked")
    private void getPropertyUserTypes(
            final Map<String, UserType> map,
            final net.sf.hibernate.mapping.PersistentClass persistentClass) {

        getUserTypes(map, persistentClass.getIdentifier().getType());
        persistentClass.getPropertyIterator().forEachRemaining(propertyObj -> {
            final net.sf.hibernate.mapping.Property property = (net.sf.hibernate.mapping.Property) propertyObj;
            getUserTypes(map, property.getType());
        });
    }

    private void getUserTypes(
            final Map<String, UserType> map,
            final net.sf.hibernate.type.Type type) {

        if (type instanceof net.sf.hibernate.type.PersistentCollectionType) {
            final net.sf.hibernate.type.PersistentCollectionType collectionType =
                    (net.sf.hibernate.type.PersistentCollectionType) type;
            final net.sf.hibernate.type.Type elementType =
                    config.getCollectionMapping(collectionType.getRole()).getElement().getType();
            getUserTypes(map, elementType);
            return;
        }

        final UserType propertyUserType = getAdaptedUserTypeFromType(type);
        if (propertyUserType != null) {
            map.put(type.getName(), propertyUserType);
        }
    }

    @SuppressWarnings("deprecation")
    private UserType getAdaptedUserTypeFromType(final net.sf.hibernate.type.Type type) {
        // v2 UserType
        if (type instanceof net.sf.hibernate.type.CustomType) {
            final net.sf.hibernate.type.CustomType customType =
                    (net.sf.hibernate.type.CustomType) type;
            return UserTypeV5Adapter.adapt(CustomTypeV2Reflection.getUserType(customType));
        }

        // v2 deprecated PersistentEnumType
        if (type instanceof net.sf.hibernate.type.PersistentEnumType) {
            final net.sf.hibernate.type.PersistentEnumType enumType =
                    (net.sf.hibernate.type.PersistentEnumType) type;
            return new PersistentEnumUserType(enumType.getReturnedClass());
        }
        return null;
    }
}
