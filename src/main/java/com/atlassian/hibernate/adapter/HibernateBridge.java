package com.atlassian.hibernate.adapter;

import com.atlassian.hibernate.adapter.bridge.factory.SessionFactoryBridge;
import com.atlassian.hibernate.adapter.bridge.factory.SessionFactoryBridgeAssociations;

import static java.util.Objects.requireNonNull;

/**
 * Implemented by SessionFactoryBridge and by v2 / v5 SessionFactory proxy objects
 * to provide alternate SessionFactory and Session object versions.
 * <p>
 * Sessions returned here share the same transaction as their counterparts.
 * For example, the Session returned from getV5Session will share any
 * transaction started in the v2 Session passed in, and vice-versa, as
 * though they are the 'same' Session.
 * <p>
 * For safety, sessions returned that share the 'same transaction'
 * throw an exception if a v2 Session implementation load or save the same
 * object identifiers as a v5 Session implementation (and vice-versa).
 */
public interface HibernateBridge {

    /**
     * Get a reference to the v5 SessionFactory interface.
     */
    static org.hibernate.SessionFactory upgrade(final net.sf.hibernate.SessionFactory sessionFactory) {
        return sessionFactory != null ? get(sessionFactory).getV5SessionFactory() : null;
    }

    /**
     * Get a reference to the v5 SessionFactory interface, and set the hibernate v2 SessionFactory
     * object that's used to find the Session object from Spring when calling SessionFactory.getCurrentSession().
     *
     * This is useful when the SessionFactory is wrapped inside a proxy, before being passed to Spring.
     * Exactly the same wrapped SessionFactory must be used in CurrentSessionContextV5Bridge.currentSession,
     * which is why we need this special 'spring' SessionFactory reference.
     */
    static org.hibernate.SessionFactory upgradeAndSetSpringV2SessionFactory(final net.sf.hibernate.SessionFactory sessionFactory) {
        HibernateBridge bridge = get(requireNonNull(sessionFactory));
        bridge.setSpringV2SessionFactory(sessionFactory);
        return bridge.getV5SessionFactory();
    }

    /**
     * Get the hibernate Session object with the v5 interface and hibernate implementation.
     */
    static org.hibernate.Session upgrade(final net.sf.hibernate.Session session) {
        return session != null ? get(session.getSessionFactory()).getV5Session(session) : null;
    }

    /**
     * Create a v2 net.sf.hibernate.SessionFactory object that has a HibernateBridge that provides the following.
     * - a v5 SessionFactory via HibernateBridge.get(sessionFactory).getV5SessionFactory()
     * - a v2 SessionFactory via HibernateBridge.get(sessionFactory).getV2orV5SessionFactory()
     */
    static net.sf.hibernate.SessionFactory createV2SessionFactoryBridge(final org.hibernate.SessionFactory sessionFactory) {
        return SessionFactoryBridge.createFromSessionFactoryV5(sessionFactory).getV2orV5SessionFactory();
    }

    /**
     * Get an instance of the HibernateBridge from a SessionFactory.
     */
    static HibernateBridge get(final org.hibernate.SessionFactory sessionFactory) {
        HibernateBridge result = SessionFactoryBridgeAssociations.getHibernateBridge(sessionFactory);
        if (result == null) {
            throw noHibernateBridgeException(sessionFactory);
        }
        return result;
    }

    /**
     * Get an instance of the HibernateBridge from a SessionFactory.
     */
    static HibernateBridge get(final net.sf.hibernate.SessionFactory sessionFactory) {
        HibernateBridge result = SessionFactoryBridgeAssociations.getHibernateBridge(sessionFactory);
        if (result == null) {
            throw noHibernateBridgeException(sessionFactory);
        }
        return result;
    }

    static IllegalArgumentException noHibernateBridgeException(final Object sessionFactory) {
        if (sessionFactory == null)
            return new IllegalArgumentException("sessionFactory was null");
        else
            return new IllegalArgumentException("SessionFactory is not a part of a HibernateBridge: " + sessionFactory.getClass().getName());
    }

    /**
     * Get a reference to the v2 SessionFactory interface.
     */
    net.sf.hibernate.SessionFactory getV2SessionFactory();

    /**
     * Get a reference to the v5 SessionFactory interface.
     */
    org.hibernate.SessionFactory getV5SessionFactory();

    /**
     * Get the hibernate SessionFactory object with the v2 or v5 hibernate implementation,
     * depending on the configured HibernateBridgeMode.
     */
    net.sf.hibernate.SessionFactory getV2orV5SessionFactory();

    /**
     * Get the hibernate v2 SessionFactory object that's used to find the Session object from Spring when
     * calling SessionFactory.getCurrentSession().
     */
    net.sf.hibernate.SessionFactory getSpringV2SessionFactory();

    /**
     * Set the hibernate v2 SessionFactory object that's used to find the Session object from Spring when
     * calling SessionFactory.getCurrentSession().
     */
    void setSpringV2SessionFactory(final net.sf.hibernate.SessionFactory sessionFactory);

    /**
     * Get the hibernate Session object with the v2 interface and hibernate implementation.
     * The session passed in may be a v2 interface wrapping a v5 implementation,
     * but the returned object will always be a v2 hibernate implementation.
     * <p>
     * This would be used in situations where the hibernate v5 implementation is known
     * to be incompatible such that the v2 implementation must continue to be used.
     */
    net.sf.hibernate.Session getV2Session(net.sf.hibernate.Session session);

    /**
     * Get the hibernate Session object with the v2 interface and hibernate implementation.
     * <p>
     * This would be used in situations where the hibernate v5 implementation is known
     * to be incompatible, and so the v2 implementation must continue to be used.
     */
    net.sf.hibernate.Session getV2Session(org.hibernate.Session session);

    /**
     * Get the hibernate Session object with the v5 interface and hibernate implementation.
     * <p>
     * This is useful for early adopters that want to use new Hibernate v5 features.
     */
    org.hibernate.Session getV5Session(net.sf.hibernate.Session session);

    /**
     * Get the hibernate Session object with the v2 or v5 hibernate implementation,
     * depending on the HibernateBridgeMode configured at session creation time.
     */
    net.sf.hibernate.Session getV2orV5Session(final org.hibernate.Session session);
}
