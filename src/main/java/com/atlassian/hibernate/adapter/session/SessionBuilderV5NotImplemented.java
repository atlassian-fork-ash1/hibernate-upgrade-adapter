package com.atlassian.hibernate.adapter.session;

import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.ConnectionReleaseMode;
import org.hibernate.FlushMode;
import org.hibernate.Interceptor;
import org.hibernate.SessionBuilder;
import org.hibernate.SessionEventListener;
import org.hibernate.engine.spi.SessionBuilderImplementor;
import org.hibernate.resource.jdbc.spi.PhysicalConnectionHandlingMode;
import org.hibernate.resource.jdbc.spi.StatementInspector;

import java.sql.Connection;

/**
 * A base class for the SessionBuilderImplementor interface that
 * throws NotImplementedException for each method by default.
 */
public abstract class SessionBuilderV5NotImplemented implements SessionBuilderImplementor {
    @Override
    public SessionBuilder interceptor(final Interceptor interceptor) {
        throw notImplemented("interceptor");
    }

    @Override
    public SessionBuilder noInterceptor() {
        throw notImplemented("noInterceptor");
    }

    @Override
    public SessionBuilder connection(final Connection connection) {
        throw notImplemented("connection");
    }

    @Override
    @Deprecated
    public SessionBuilder owner(final org.hibernate.engine.spi.SessionOwner sessionOwner) {
        throw notImplemented("owner");
    }

    @Override
    public SessionBuilder statementInspector(final StatementInspector statementInspector) {
        throw notImplemented("statementInspector");
    }

    @Override
    @Deprecated
    public SessionBuilder connectionReleaseMode(final ConnectionReleaseMode connectionReleaseMode) {
        throw notImplemented("connectionReleaseMode");
    }

    @Override
    public SessionBuilder connectionHandlingMode(final PhysicalConnectionHandlingMode mode) {
        throw notImplemented("connectionHandlingMode");
    }

    @Override
    public SessionBuilder autoJoinTransactions(final boolean autoJoinTransactions) {
        throw notImplemented("autoJoinTransactions");
    }

    @Override
    public SessionBuilder autoClose(final boolean autoClose) {
        throw notImplemented("autoClose");
    }

    @Override
    public SessionBuilder autoClear(final boolean autoClear) {
        throw notImplemented("autoClear");
    }

    @Override
    public SessionBuilder flushMode(final FlushMode flushMode) {
        throw notImplemented("flushMode");
    }

    @Override
    @Deprecated
    public SessionBuilder flushBeforeCompletion(final boolean flushBeforeCompletion) {
        throw notImplemented("flushBeforeCompletion");
    }

    @Override
    public SessionBuilder tenantIdentifier(final String tenantIdentifier) {
        throw notImplemented("tenantIdentifier");
    }

    @Override
    public SessionBuilder eventListeners(final SessionEventListener... listeners) {
        throw notImplemented("listeners");
    }

    @Override
    public SessionBuilder clearEventListeners() {
        throw notImplemented("clearEventListeners");
    }

    private NotImplementedException notImplemented(final String methodName) {
        return new NotImplementedException(methodName + " not implemented");
    }
}
