package com.atlassian.hibernate.adapter.session;

import com.atlassian.hibernate.adapter.bridge.session.SessionBridge;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import java.sql.Connection;

public interface SessionV5Supplier {
    Session get(SessionBridge sessionBridge, Connection connection)
            throws HibernateException;
}
