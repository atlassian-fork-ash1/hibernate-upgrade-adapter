package com.atlassian.hibernate.adapter.session;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Session;

import java.sql.Connection;

public interface SessionV2Supplier {
    Session get(Connection connection) throws HibernateException;
}
