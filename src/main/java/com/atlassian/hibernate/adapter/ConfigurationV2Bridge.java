package com.atlassian.hibernate.adapter;

import com.atlassian.hibernate.adapter.adapters.ConnectionProviderV5Adapter;
import com.atlassian.hibernate.adapter.adapters.DialectAdapter;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.cache.RegionFactoryCacheProviderBridge;
import com.atlassian.hibernate.adapter.adapters.interceptor.InterceptorV5Adapter;
import com.atlassian.hibernate.adapter.bridge.factory.SessionFactoryBridge;
import com.atlassian.hibernate.adapter.generator.IdentifierGeneratorFactoryV5Bridge;
import com.atlassian.hibernate.adapter.logging.HibernateLogConfigBridge;
import com.atlassian.hibernate.adapter.mapping.PersistentClassChangeTracker;
import com.atlassian.hibernate.adapter.proxy.BytecodeProviderImpl_ImplementV2Proxy;
import com.atlassian.hibernate.adapter.reflection.ConfigurationV2Reflection;
import com.atlassian.hibernate.adapter.reflection.LocalSessionFactoryBeanV2Reflection;
import com.atlassian.hibernate.adapter.session.CurrentSessionContextV5Bridge;
import com.atlassian.hibernate.adapter.transpiler.HibernateMappingTranspiler;
import com.atlassian.hibernate.adapter.transpiler.HibernateMappingTranspilerResult;
import com.atlassian.hibernate.adapter.type.V2ToV5TypeContributor;
import com.atlassian.hibernate.util.DeprecationLogDisabler;
import com.atlassian.hibernate.util.Hibernate2PostgresImplicitNamingStrategy;
import com.atlassian.hibernate.util.ThrowableUtil;
import com.google.common.base.Suppliers;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.SessionFactory;
import net.sf.hibernate.cfg.Configuration;
import net.sf.hibernate.cfg.Environment;
import net.sf.hibernate.cfg.NamingStrategy;
import net.sf.hibernate.cfg.Settings;
import net.sf.hibernate.mapping.Collection;
import net.sf.hibernate.mapping.PersistentClass;
import net.sf.hibernate.util.DTDEntityResolver;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.dom4j.io.SAXReader;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataBuilder;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.BootstrapServiceRegistry;
import org.hibernate.boot.registry.BootstrapServiceRegistryBuilder;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.AvailableSettings;
import org.hibernate.engine.jdbc.spi.JdbcServices;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.id.factory.internal.MutableIdentifierGeneratorFactoryInitiator;
import org.hibernate.id.factory.spi.MutableIdentifierGeneratorFactory;
import org.hibernate.integrator.spi.Integrator;
import org.hibernate.resource.jdbc.spi.PhysicalConnectionHandlingMode;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.spi.ServiceRegistryImplementor;
import org.hibernate.service.spi.SessionFactoryServiceRegistry;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.orm.hibernate.LocalSessionFactoryBean;
import org.w3c.dom.Document;
import org.xml.sax.ErrorHandler;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.persistence.PersistenceException;
import javax.sql.DataSource;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.function.Supplier;

/**
 * This is a drop-in replacement for net.sf.hibernate.cfg.Configuration
 * that creates SessionFactoryBridge objects to provide alternate
 * v2 and v5 onwards SessionFactory and Session objects.
 */
public class ConfigurationV2Bridge extends net.sf.hibernate.cfg.Configuration {
    /**
     * For the postgres dialect, set to true to use v2 schema naming for implicit unique and foreign key constraints.
     */
    public static final String USE_V2_POSTGRES_SCHEMA_NAMING = "hibernate.atlassian.use.v2.postgres.schema.naming";

    /**
     * Set to true to lazy initialize the v2 SessionFactory, instead of initializing it up front.
     */
    public static final String LAZY_INIT_V2_SESSION_FACTORY = "hibernate.atlassian.adapter.lazy.init.v2.factory";

    /**
     * Set to true to lazy initialize the v5 SessionFactory, instead of initializing it up front.
     */
    public static final String LAZY_INIT_V5_SESSION_FACTORY = "hibernate.atlassian.adapter.lazy.init.v5.factory";

    /**
     * Set to true to lazy initialize the SessionFactory that isn't configured during initialization.
     */
    public static final String LAZY_INIT_SESSION_FACTORIES = "hibernate.atlassian.adapter.lazy.init.factories";

    /**
     * Set to true to parse .hbm.xml files by hibernate 5 only, skipping hibernate 2 entirely.
     * This is useful when starting to use v5 features in the .hbm.xml file that aren't supported in hibernate 2.
     */
    public static final String PARSE_HBM_TO_HIBERNATE5_ONLY = "hibernate.atlassian.parse.hbm.hibernate5.only";

    /**
     * Disable xml validation when parsing .hbm.xml files with hibernatge 2.
     * This is useful when configuring some hibernate 5 attributes that aren't able to be parsed by hibernate 2.
     */
    public static final String DISABLE_V2_HBM_VALIDATION = "hibernate.atlassian.disable.v2.hbm.validation";

    private static final String MAX_CONNECTION_AGE_PROPERTY_V2 = "hibernate.c3p0.max_connection_age";
    private static final String MAX_CONNECTION_AGE_PROPERTY_V5 = "hibernate.c3p0.maxConnectionAge";

    private static final String[] UNSUPPORTED_HIBERNATE2_PROPERTIES = new String[] {
            Environment.QUERY_CACHE_FACTORY,
            Environment.SQL_EXCEPTION_CONVERTER,
            Environment.SESSION_FACTORY_NAME,
            Environment.TRANSACTION_MANAGER_STRATEGY,
            Environment.USE_MINIMAL_PUTS,
    };

    private static final Log LOG = LogFactory.getLog(Configuration.class);
    private static final HibernateBridgeMode DEFAULT_BRIDGE_MODE = HibernateBridgeMode.V5_ADAPTER;
    private static final Hibernate2PostgresImplicitNamingStrategy V2_POSTGRES_NAMING_STRATEGY = new Hibernate2PostgresImplicitNamingStrategy();

    private HibernateBridgeMode bootMode;
    private final List<byte[]> inputStreams = new ArrayList<>();
    private final Map<String, String> cacheStrategyClassNames = new HashMap<>();
    private final PersistentClassChangeTracker persistentClassChangeTracker = new PersistentClassChangeTracker();
    private Supplier<HibernateBridgeMode> bridgeMode = () -> DEFAULT_BRIDGE_MODE;
    private URL configUrl;
    private boolean configuredCaches;

    private volatile Settings settingsV2;
    private volatile org.hibernate.cfg.Configuration configurationV5;
    private volatile MetadataSources metadataSources;
    private volatile Metadata metadata;

    public ConfigurationV2Bridge() {
        BytecodeProviderImpl_ImplementV2Proxy.install();
        interceptV2PersistentClassObjects();
        HibernateLogConfigBridge.setLogLevels();
        // turn off deprecation reporting of hql '.class' when using 'in' keyword (see HibernateQueryTranspiler)
        DeprecationLogDisabler.disableLogging("logDeprecationOfClassEntityTypeSelector", false);
        // turn off deprecation reporting of createCriteria
        DeprecationLogDisabler.disableLogging("deprecatedLegacyCriteria", false);
    }

    private static void validateHibernate2Properties(final Properties properties) {
        final String supportedTransactionFactory = net.sf.hibernate.transaction.JDBCTransactionFactory.class.getName();
        final Object transactionFactory = properties.get(net.sf.hibernate.cfg.Environment.TRANSACTION_STRATEGY);

        if (transactionFactory != null && !transactionFactory.equals(supportedTransactionFactory)) {
            throw new IllegalArgumentException(
                    "Unsupported value for "
                    + net.sf.hibernate.cfg.Environment.TRANSACTION_STRATEGY
                    + ", only "
                    + supportedTransactionFactory
                    + " is supported.");
        }

        for (final String name : UNSUPPORTED_HIBERNATE2_PROPERTIES) {
            if (properties.contains(name)) {
                throw new NotImplementedException(name + " hibernate property not supported");
            }
        }
    }

    /**
     * Set the HibernateBridgeMode that determines the dominate hibernate stack version to use.
     */
    public void setBridgeMode(final HibernateBridgeMode bridgeMode) {
        this.bridgeMode = () -> bridgeMode;
    }

    /**
     * Set the HibernateBridgeMode that determines the dominate hibernate stack version to use.
     */
    public void setBridgeModeSupplier(final Supplier<HibernateBridgeMode> bridgeMode) {
        this.bridgeMode = bridgeMode;
    }

    @Override
    public Configuration configure(final URL url) throws HibernateException {
        if (useV2ColdStart()) {
            return super.configure(url);
        }
        super.configure(url);
        this.configUrl = url;
        invalidateV5Configuration();
        return this;
    }

    @Override
    public Configuration addInputStream(final InputStream xmlInputStream) throws MappingException {
        if (useV2ColdStart()) {
            return super.addInputStream(xmlInputStream);
        }
        try {
            final byte[] bytes = IOUtils.toByteArray(xmlInputStream);
            if (!Boolean.parseBoolean((String) getProperties().get(PARSE_HBM_TO_HIBERNATE5_ONLY))) {
                addV2InputStream(new ByteArrayInputStream(bytes));
            }
            inputStreams.add(bytes);
            invalidateV5Configuration();
            return this;
        } catch (final IOException ex) {
            throw new MappingException(ex.getMessage(), ex);
        }
    }

    private Configuration addV2InputStream(final InputStream stream) throws MappingException {
        try {
            SAXReader reader = new SAXReader();
            reader.setEntityResolver(new DTDEntityResolver());
            if (Boolean.parseBoolean((String) getProperties().get(DISABLE_V2_HBM_VALIDATION))) {
                // disable xml validation to allow hibernate 5 attributes to be included without breaking hibernate 2
                reader.setErrorHandler(new IgnoreErrors());
            }
            reader.setMergeAdjacentText(true);
            reader.setValidation(true);
            add(reader.read(new InputSource(stream)));
            return this;
        } catch (MappingException ex) {
            throw ex;
        } catch (Exception ex) {
            LOG.error("Could not configure datastore from input stream", ex);
            throw new MappingException(ex);
        } finally {
            try {
                stream.close();
            } catch (IOException ex) {
                LOG.error("could not close input stream", ex);
            }
        }
    }

    public static class IgnoreErrors implements ErrorHandler {
        @Override
        public void warning(final SAXParseException exception) throws SAXException {
        }

        @Override
        public void error(final SAXParseException exception) throws SAXException {
        }

        @Override
        public void fatalError(final SAXParseException exception) throws SAXException {
        }
    }

    //---------- Class / Collection Mappings ----------//

    @Override
    public Iterator getCollectionMappings() {
        if (useV2ColdStart()) {
            return super.getCollectionMappings();
        }
        // NOTE: there are setters on return values, which will have no effect in the v5 world
        return super.getCollectionMappings();
    }

    @Override
    public Collection getCollectionMapping(final String role) {
        if (useV2ColdStart()) {
            return super.getCollectionMapping(role);
        }
        // NOTE: there are setters on the return value, which will have no effect in the v5 world
        return super.getCollectionMapping(role);
    }

    @Override
    protected void reset() {
        super.reset();
        interceptV2PersistentClassObjects();
        invalidateV5Configuration();
    }

    //---------- Build v2 and v5 SessionFactory ----------//

    /**
     * Intercept PersistentClass objects, which will be returned from
     * Configuration.getClassMappings, Configuration.getClassMapping and elsewhere.
     * This is to track any programmatic changes to the mappings.
     */
    private void interceptV2PersistentClassObjects() {
        ConfigurationV2Reflection.setClasses(this, new HashMap() {
            @Override
            @SuppressWarnings("unchecked")
            public Object put(final Object key, final Object value) {
                if (useV2ColdStart()) {
                    return super.put(key, value);
                }

                final Supplier<Boolean> throwOnUnsupportedMutation =
                        () -> getEffectiveBridgeMode().isV5();
                PersistentClass persistentClass = persistentClassChangeTracker.intercept(
                        (PersistentClass) value, throwOnUnsupportedMutation);
                return super.put(key, persistentClass);
            }
        });
    }

    /**
     * Propagate v2 PersistentClass changes to the v5 Metadata object.
     */
    protected void updateV5MetaData(final Metadata metadata) {
        updateV5MetaDataInternal(metadata);
    }

    /**
     * Propagate v2 PersistentClass changes to the v5 Metadata object.
     */
    void updateV5MetaDataInternal(final Metadata metadata) {
        if (useV2PostgresSchemaNamingStrategy()) {
            V2_POSTGRES_NAMING_STRATEGY.updateUniqueKeyConstraints(metadata);
        }
        persistentClassChangeTracker.applyChanges(metadata);
    }

    /**
     * Build the SessionFactoryBridge and return the v2 version of the
     * SessionFactory bridge interface.
     */
    @Override
    public SessionFactory buildSessionFactory() throws HibernateException {
        if (useV2ColdStart()) {
            return super.buildSessionFactory();
        }

        final long beforeMillis = System.currentTimeMillis();

        // these will only be available to the current thread
        final DataSource springDataSource = LocalSessionFactoryBean.getConfigTimeDataSource();
        final LobHandler springLobHandler = LocalSessionFactoryBean.getConfigTimeLobHandler();
        if (LocalSessionFactoryBean.getConfigTimeTransactionManager() != null) {
            throw new IllegalArgumentException("TransactionManager not supported while using hibernate-upgrade-adapter");
        }

        // this usually happens in super.buildSessionFactory(), but we need this for both v2 and v5
        if (shouldConfigureCaches()) {
            super.configureCaches(getSettingsV2());
        }

        // create v2 and/or v5 session factories early if required
        final net.sf.hibernate.impl.SessionFactoryImpl sessionFactoryV2 =
                buildSessionFactoryV2EarlyIfRequired();
        final org.hibernate.engine.spi.SessionFactoryImplementor sessionFactoryV5 =
                buildSessionFactoryV5EarlyIfRequired();

        // wrap v2 and v5 session factories into a SessionFactoryBridge
        Supplier<net.sf.hibernate.SessionFactory> sessionFactoryV2Supplier =
                sessionFactoryV2 != null
                ? () -> sessionFactoryV2
                : Suppliers.memoize(() -> buildSessionFactoryV2Unchecked(springDataSource, springLobHandler))::get;
        Supplier<org.hibernate.SessionFactory> sessionFactoryV5Supplier =
                sessionFactoryV5 != null
                ? () -> sessionFactoryV5
                : Suppliers.memoize(() -> buildSessionFactoryV5Unchecked())::get;
        final SessionFactoryBridge sessionFactoryBridge = new SessionFactoryBridge(
                sessionFactoryV2Supplier, sessionFactoryV5Supplier, this::getEffectiveBridgeMode);
        final net.sf.hibernate.SessionFactory sessionFactory = sessionFactoryBridge.getV2orV5SessionFactory();

        // log the time taken
        final long totalMillis = System.currentTimeMillis() - beforeMillis;
        LOG.debug("ConfigurationV2Bridge.buildSessionFactory() took " + totalMillis + " milliseconds");
        return sessionFactory;
    }

    @Override
    protected Settings buildSettings() throws HibernateException {
        if (useV2ColdStart()) {
            return super.buildSettings();
        }
        return getSettingsV2();
    }

    private synchronized boolean shouldConfigureCaches() {
        boolean result = !configuredCaches;
        configuredCaches = true;
        return result;
    }

    @Override
    protected final void configureCaches(final Settings settings) throws HibernateException {
        // this will be called in buildSettings() instead
    }

    private net.sf.hibernate.impl.SessionFactoryImpl buildSessionFactoryV2EarlyIfRequired()
            throws HibernateException {

        final boolean lazyInit =
                Boolean.parseBoolean((String) getProperties().get(LAZY_INIT_SESSION_FACTORIES))
                || Boolean.parseBoolean((String) getProperties().get(LAZY_INIT_V2_SESSION_FACTORY));

        if (lazyInit && !getBootMode().isV2()) {
            return null;
        }
        return (net.sf.hibernate.impl.SessionFactoryImpl) buildSessionFactoryV2(
                LocalSessionFactoryBean.getConfigTimeDataSource(),
                LocalSessionFactoryBean.getConfigTimeLobHandler());
    }

    private org.hibernate.engine.spi.SessionFactoryImplementor buildSessionFactoryV5EarlyIfRequired()
            throws HibernateException {

        final boolean lazyInit =
                Boolean.parseBoolean((String) getProperties().get(LAZY_INIT_SESSION_FACTORIES))
                || Boolean.parseBoolean((String) getProperties().get(LAZY_INIT_V5_SESSION_FACTORY));

        if (lazyInit && !getBootMode().isV5()) {
            return null;
        }
        return (org.hibernate.engine.spi.SessionFactoryImplementor) buildSessionFactoryV5();
    }

    private net.sf.hibernate.SessionFactory buildSessionFactoryV2Unchecked(
            final DataSource springDataSource,
            final LobHandler springLobHandler) {

        try {
            return buildSessionFactoryV2(springDataSource, springLobHandler);
        } catch (HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    private org.hibernate.SessionFactory buildSessionFactoryV5Unchecked() {
        try {
            return buildSessionFactoryV5();
        } catch (HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @SuppressWarnings("unchecked")
    private net.sf.hibernate.SessionFactory buildSessionFactoryV2(
            final DataSource springDataSource,
            final LobHandler springLobHandler) throws HibernateException {

        LocalSessionFactoryBeanV2Reflection.getConfigTimeDataSourceHolder().set(springDataSource);
        LocalSessionFactoryBeanV2Reflection.getConfigTimeLobHandlerHolder().set(springLobHandler);
        try {
            return super.buildSessionFactory();
        } finally {
            LocalSessionFactoryBeanV2Reflection.getConfigTimeDataSourceHolder().set(null);
            LocalSessionFactoryBeanV2Reflection.getConfigTimeLobHandlerHolder().set(null);
        }
    }

    private org.hibernate.SessionFactory buildSessionFactoryV5() throws HibernateException {

        try {
            return getConfigurationV5().buildSessionFactory();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    private BootstrapServiceRegistry buildBootstrapServiceRegistry() {
        final Integrator integrator = new Integrator() {
            @Override
            public void integrate(
                    final Metadata metadata,
                    final SessionFactoryImplementor sessionFactory,
                    final SessionFactoryServiceRegistry serviceRegistry) {

                updateV5MetaData(metadata);
            }

            @Override
            public void disintegrate(
                    final SessionFactoryImplementor sessionFactory,
                    final SessionFactoryServiceRegistry serviceRegistry) {
            }
        };
        return new BootstrapServiceRegistryBuilder().applyIntegrator(integrator).build();
    }

    //---------- Build v5 Metadata and Configuration ----------//

    private Settings getSettingsV2() throws HibernateException {
        if (settingsV2 != null) {
            return settingsV2;
        }

        // build and cache the settings object
        ConfigurationV2Reflection.secondPassCompile(this);
        settingsV2 = super.buildSettings();
        return settingsV2;
    }

    /**
     * Get the v5 Metadata object.
     */
    public synchronized Metadata getMetadata() throws HibernateException {
        if (metadata != null) {
            return metadata;
        }

        final org.hibernate.cfg.Configuration configV5 = getConfigurationV5();
        final StandardServiceRegistry serviceRegistry = buildServiceRegistry(configV5);
        mapDialectObjects(serviceRegistry);

        final MetadataBuilder metadataBuilder = metadataSources.getMetadataBuilder(serviceRegistry);
        metadataBuilder.applyTypes(new V2ToV5TypeContributor(this));
        if (useV2PostgresSchemaNamingStrategy()) {
            metadataBuilder.applyImplicitNamingStrategy(V2_POSTGRES_NAMING_STRATEGY);
        }

        metadata = metadataBuilder.build();
        updateV5MetaData(metadata);
        return metadata;
    }

    /**
     * Get the lazy-created the v5 Configuration object that's built from V2 Configuration and Settings objects.
     */
    public synchronized org.hibernate.cfg.Configuration getConfigurationV5() throws HibernateException {
        if (configurationV5 == null) {
            configurationV5 = buildConfigurationV5();
        }
        return configurationV5;
    }

    /**
     * Get the lazy-created v5 MetadataSources object.
     */
    private synchronized MetadataSources getMetadataSources() {
        if (metadataSources == null) {
            metadataSources = new MetadataSources(buildBootstrapServiceRegistry());
        }
        return metadataSources;
    }

    private static StandardServiceRegistry buildServiceRegistry(final org.hibernate.cfg.Configuration configV5)
            throws HibernateException {

        final StandardServiceRegistryBuilder serviceRegistryBuilder = configV5.getStandardServiceRegistryBuilder();
        serviceRegistryBuilder.applySettings(configV5.getProperties());
        return serviceRegistryBuilder.build();
    }

    private synchronized void invalidateV5Configuration() {
        settingsV2 = null;
        configurationV5 = null;
        metadataSources = null;
        metadata = null;
        configuredCaches = false;
    }

    /**
     * Build a V5 Configuration object from the V2 Configuration and Settings objects.
     */
    protected org.hibernate.cfg.Configuration buildConfigurationV5()
            throws HibernateException {

        return buildConfigurationV5Internal();
    }

    org.hibernate.cfg.Configuration buildConfigurationV5Internal()
            throws HibernateException {

        final org.hibernate.cfg.Configuration config = new org.hibernate.cfg.Configuration(getMetadataSources()) {
            @Override
            public org.hibernate.SessionFactory buildSessionFactory(final org.hibernate.service.ServiceRegistry serviceRegistry)
                    throws org.hibernate.HibernateException {

                try {
                    mapDialectObjects(serviceRegistry);
                } catch (net.sf.hibernate.HibernateException ex) {
                    throw HibernateExceptionAdapter.adapt(ex);
                }
                return super.buildSessionFactory(serviceRegistry);
            }
        };

        // adapt v2 UserType and PersistentEnumType to v5 UserTypes
        config.registerTypeContributor(new V2ToV5TypeContributor(this));

        // adapt v2 IdentifierGenerator objects to v5
        config.getStandardServiceRegistryBuilder().addInitiator(
                new MutableIdentifierGeneratorFactoryInitiator() {
                    @Override
                    public MutableIdentifierGeneratorFactory initiateService(
                            final Map configurationValues, final ServiceRegistryImplementor registry) {
                        return new IdentifierGeneratorFactoryV5Bridge();
                    }
                }
        );

        // adapt v2 ConnectionProvider objects to v5
        config.getStandardServiceRegistryBuilder().addService(
                org.hibernate.engine.jdbc.connections.spi.ConnectionProvider.class,
                ConnectionProviderV5Adapter.adapt(getSettingsV2().getConnectionProvider()));

        // set config properties
        config.setInterceptor(InterceptorV5Adapter.adapt(getInterceptor()));

        // apply mapping files
        if (configUrl != null) {
            config.configure(this.configUrl);
        }
        for (final byte[] is : inputStreams) {
            config.addInputStream(transpile(new ByteArrayInputStream(is)));
        }

        // set additional config properties
        validateHibernate2Properties(getProperties());
        setV5HibernateProperties(config.getProperties(), getSettingsV2());
        if (useV2PostgresSchemaNamingStrategy()) {
            config.setImplicitNamingStrategy(V2_POSTGRES_NAMING_STRATEGY);
        }
        return config;
    }

    private void mapDialectObjects(final ServiceRegistry serviceRegistry) throws HibernateException {
        // register the v2 v5 dialect mapping
        final org.hibernate.dialect.Dialect v5dialect = serviceRegistry.getService(JdbcServices.class).getDialect();
        final net.sf.hibernate.dialect.Dialect v2dialect = getSettingsV2().getDialect();
        if (!(v2dialect instanceof net.sf.hibernate.dialect.GenericDialect)) {
            DialectAdapter.mapDialectObjects(v2dialect, v5dialect);
        }
    }

    private InputStream transpile(final InputStream is) throws HibernateException {
        try {
            final HibernateMappingTranspilerResult mapping = new HibernateMappingTranspiler().transpile(is);
            cacheStrategyClassNames.putAll(mapping.getCacheStrategyClassNames());
            return mapping.getTranspiledMappingFile();
        } catch (final IOException ex) {
            throw new HibernateException(ex.getMessage(), ex);
        }
    }

    private void setV5HibernateProperties(
            final Properties properties,
            final Settings v2settings)
            throws HibernateException {

        properties.putAll(getProperties());

        // only JDBCTransactionFactory is supported
        properties.remove(net.sf.hibernate.cfg.Environment.TRANSACTION_STRATEGY);

        // set for compatibility with v2 behaviour
        if (!properties.contains(AvailableSettings.ENABLE_LAZY_LOAD_NO_TRANS)) {
            properties.put(AvailableSettings.ENABLE_LAZY_LOAD_NO_TRANS, "true");
        }

        // set for compatibility with v2 behaviour
        if (!properties.contains(AvailableSettings.CONNECTION_HANDLING)) {
            properties.put(
                    AvailableSettings.CONNECTION_HANDLING,
                    PhysicalConnectionHandlingMode.DELAYED_ACQUISITION_AND_HOLD.toString());
        }

        properties.put(
                org.hibernate.cfg.Environment.CURRENT_SESSION_CONTEXT_CLASS,
                CurrentSessionContextV5Bridge.class.getName());

        if (!properties.contains(AvailableSettings.CACHE_REGION_FACTORY)
                && v2settings.getCacheProvider() != null) {

            properties.put(
                    org.hibernate.cfg.Environment.CACHE_REGION_FACTORY,
                    RegionFactoryCacheProviderBridge.class.getName());
            properties.put(
                    RegionFactoryCacheProviderBridge.V2_CACHE_PROVIDER_PROPERTY,
                    v2settings.getCacheProvider());
            properties.put(
                    RegionFactoryCacheProviderBridge.V2_CACHE_STRATEGY_CLASS_MAP_PROPERTY,
                    cacheStrategyClassNames);
        }

        // this will be required once we use the hibernate v5 C3P0ConnectionProvider
        if (getProperties().get(MAX_CONNECTION_AGE_PROPERTY_V2) != null) {
            properties.put(
                    MAX_CONNECTION_AGE_PROPERTY_V5,
                    getProperties().get(MAX_CONNECTION_AGE_PROPERTY_V2));
        }

        final String dialectName = (String) properties.get(org.hibernate.cfg.Environment.DIALECT);
        final String dialect = DialectAdapter.adaptV2toV5(dialectName);
        if (dialect == null) {
            throw new IllegalArgumentException("Failed to find v5 dialect equivalent for v2 " + dialectName);
        }
        properties.put(org.hibernate.cfg.Environment.DIALECT, dialect);
    }

    /**
     * Returns true when using 'v2 cold start mode', which means calls are just delegated
     * to the v2 Configuration object.
     */
    private HibernateBridgeMode getBootMode() {
        if (bootMode == null) {
            // cache the result; v2 cold start mode lasts for the lifetime of the application
            bootMode = getEffectiveBridgeMode();
        }
        return bootMode;
    }

    private boolean useV2ColdStart() {
        return getBootMode() == HibernateBridgeMode.V2_COLD_START;
    }

    private HibernateBridgeMode getEffectiveBridgeMode() {
        return bridgeMode.get();
    }

    //---------- Not Implemented ----------//

    @Override
    public Configuration configure() throws HibernateException {
        if (useV2ColdStart()) {
            return super.configure();
        }
        throw new NotImplementedException("configure() not implemented");
    }

    @Override
    public Configuration configure(final String resource) throws HibernateException {
        if (useV2ColdStart()) {
            return super.configure(resource);
        }
        throw new NotImplementedException("configure(String) not implemented");
    }

    @Override
    public Configuration configure(final File configFile) throws HibernateException {
        if (useV2ColdStart()) {
            return super.configure(configFile);
        }
        throw new NotImplementedException("configure(File) not implemented");
    }

    @Override
    public Configuration configure(final Document document) throws HibernateException {
        if (useV2ColdStart()) {
            return super.configure(document);
        }
        throw new NotImplementedException("configure(Document) not implemented");
    }

    @Override
    public Configuration addFile(final String xmlFile) throws MappingException {
        if (useV2ColdStart()) {
            return super.addFile(xmlFile);
        }
        throw new NotImplementedException("addFile(String) not implemented");
    }

    @Override
    public Configuration addXML(final String xml) throws MappingException {
        if (useV2ColdStart()) {
            return super.addXML(xml);
        }
        throw new NotImplementedException("addXML(String) not implemented");
    }

    @Override
    public Configuration addDocument(final Document doc) throws MappingException {
        if (useV2ColdStart()) {
            return super.addDocument(doc);
        }
        throw new NotImplementedException("addDocument(Document) not implemented");
    }

    @Override
    protected final void add(final org.dom4j.Document doc) throws Exception {
        if (useV2ColdStart()) {
            super.add(doc);
            return;
        }

        super.add(doc);
        invalidateV5Configuration();
    }

    @Override
    public Configuration setNamingStrategy(final NamingStrategy namingStrategy) {
        if (useV2ColdStart()) {
            return super.setNamingStrategy(namingStrategy);
        }

        if (namingStrategy != null) {
            throw new NotImplementedException("setNamingStrategy not implemented");
        }
        return this;
    }

    private boolean useV2PostgresSchemaNamingStrategy() {
        return Boolean.parseBoolean((String) getProperties().get(USE_V2_POSTGRES_SCHEMA_NAMING))
               && getDialectName().toUpperCase(Locale.ENGLISH).contains("POSTGRES");
    }

    private String getDialectName() {
        return (String) getProperties().get(org.hibernate.cfg.Environment.DIALECT);
    }
}
