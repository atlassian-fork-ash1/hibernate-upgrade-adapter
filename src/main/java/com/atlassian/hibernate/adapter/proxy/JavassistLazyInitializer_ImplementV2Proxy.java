package com.atlassian.hibernate.adapter.proxy;

import com.atlassian.hibernate.adapter.adapters.LazyInitializerV2Adapter;
import net.sf.cglib.proxy.Callback;
import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.proxy.pojo.javassist.JavassistLazyInitializer;
import org.hibernate.type.CompositeType;

import java.io.Serializable;
import java.lang.reflect.Method;

/**
 * An implementation of JavassistLazyInitializer that produces proxies that also implement
 * the hibernate v2 net.sf.hibernate.proxy.HibernateProxy interface.
 *
 * @see BytecodeProviderImpl_ImplementV2Proxy
 */
@SuppressWarnings("checkstyle:typename")
class JavassistLazyInitializer_ImplementV2Proxy extends JavassistLazyInitializer {

    @SuppressWarnings("checkstyle:parameternumber")
    public JavassistLazyInitializer_ImplementV2Proxy(
            final String entityName,
            final Class persistentClass,
            final Class[] interfaces,
            final Serializable id,
            final Method getIdentifierMethod,
            final Method setIdentifierMethod,
            final CompositeType componentIdType,
            final org.hibernate.engine.spi.SharedSessionContractImplementor session,
            final boolean overridesEquals) {

        super(entityName, persistentClass, interfaces, id, getIdentifierMethod, setIdentifierMethod, componentIdType, session, overridesEquals);
    }

    /**
     * On the hibernate v2 net.sf.cglib.proxy.Factory interface 'getCallback' method,
     * return the lazy initializer with details about the proxy.
     */
    private static Callback getCallback(final Object proxy, final int i) {
        if (i != 0) {
            throw new IndexOutOfBoundsException();
        }

        org.hibernate.proxy.LazyInitializer lazy = null;
        if (proxy instanceof org.hibernate.proxy.HibernateProxy) {
            lazy = ((org.hibernate.proxy.HibernateProxy) proxy).getHibernateLazyInitializer();
        }
        return (Callback) LazyInitializerV2Adapter.adapt(
                lazy,
                lazy != null ? lazy.getPersistentClass() : proxy.getClass(),
                lazy != null ? lazy.getIdentifier() : null);
    }

    public void constructed() {
        super.constructed();
    }

    @Override
    public Object invoke(
            final Object proxy,
            final Method thisMethod,
            final Method proceed,
            final Object[] args) throws Throwable {
        if (thisMethod.getName().equals("getCallback")) {
            return getCallback(proxy, (int) args[0]);
        }
        return super.invoke(proxy, thisMethod, proceed, args);
    }

    @Override
    protected Object serializableProxy() {
        throw new NotImplementedException("serializableProxy not implemented");
    }
}
