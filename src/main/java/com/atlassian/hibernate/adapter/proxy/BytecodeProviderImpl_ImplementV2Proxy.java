package com.atlassian.hibernate.adapter.proxy;

import com.atlassian.hibernate.util.ThrowableUtil;
import org.hibernate.bytecode.internal.javassist.BytecodeProviderImpl;
import org.hibernate.bytecode.spi.ProxyFactoryFactory;
import org.hibernate.cfg.Environment;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * An implementation of BytecodeProviderImpl that produces proxies that also implement
 * the hibernate v2 net.sf.hibernate.proxy.HibernateProxy interface.
 * <p>
 * This allows v2 Hibernate.getClass, Hibernate.initialize and other variants to
 * operate with entities instantiated by hibernate v5.
 */
@SuppressWarnings("checkstyle:typename")
public class BytecodeProviderImpl_ImplementV2Proxy extends BytecodeProviderImpl {

    /**
     * Install this BytecodeProvider globally, which is presently the only possible way.
     */
    public static void install() {
        try {
            final Field field = Environment.class.getDeclaredField("BYTECODE_PROVIDER_INSTANCE");
            field.setAccessible(true);
            allowFinalFieldSet(field);
            field.set(null, new BytecodeProviderImpl_ImplementV2Proxy());
        } catch (final ReflectiveOperationException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    private static void allowFinalFieldSet(final Field field) throws ReflectiveOperationException {
        final Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);
    }

    @Override
    public ProxyFactoryFactory getProxyFactoryFactory() {
        return new ProxyFactoryFactoryImpl_ImplementV2Proxy();
    }
}
