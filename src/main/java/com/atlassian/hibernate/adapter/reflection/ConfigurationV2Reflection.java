package com.atlassian.hibernate.adapter.reflection;

import com.atlassian.hibernate.util.ThrowableUtil;
import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.cfg.Configuration;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.function.BiConsumer;

public final class ConfigurationV2Reflection {
    private ConfigurationV2Reflection() { }

    private static final Method SECOND_PASS_METHOD = getPrivateMethod("secondPassCompile");
    private static final BiConsumer<Object, Object> CLASSES_SETTER =
            ReflectionHelper.INSTANCE.getPrivateFieldSetter(Configuration.class, "classes");

    public static void secondPassCompile(final Configuration obj) {
        try {
            SECOND_PASS_METHOD.invoke(obj);
        } catch (final ReflectiveOperationException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    public static void setClasses(final Configuration obj, final Map classes) {
        CLASSES_SETTER.accept(obj, classes);
    }

    private static Method getPrivateMethod(final String methodName, final Class... parameterClasses) {
        try {
            final Method method = Configuration.class.getDeclaredMethod(methodName, parameterClasses);
            method.setAccessible(true);
            return method;
        } catch (ReflectiveOperationException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }
}
