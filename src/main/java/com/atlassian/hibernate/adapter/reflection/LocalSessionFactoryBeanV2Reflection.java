package com.atlassian.hibernate.adapter.reflection;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import org.springframework.orm.hibernate.LocalSessionFactoryBean;

import java.util.function.Function;

public final class LocalSessionFactoryBeanV2Reflection {
    private LocalSessionFactoryBeanV2Reflection() { }

    private static final Function<Object, Object> CONFIG_TIME_DATA_SOURCE_HOLDER =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(LocalSessionFactoryBean.class, "configTimeDataSourceHolder");
    private static final Function<Object, Object> CONFIG_TIME_LOG_HANDLER_HOLDER =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(LocalSessionFactoryBean.class, "configTimeLobHandlerHolder");

    public static ThreadLocal getConfigTimeDataSourceHolder() {
        return (ThreadLocal) CONFIG_TIME_DATA_SOURCE_HOLDER.apply(null);
    }

    public static ThreadLocal getConfigTimeLobHandlerHolder() {
        return (ThreadLocal) CONFIG_TIME_LOG_HANDLER_HOLDER.apply(null);
    }
}
