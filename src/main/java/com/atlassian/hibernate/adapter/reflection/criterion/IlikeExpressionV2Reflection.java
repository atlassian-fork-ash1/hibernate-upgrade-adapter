package com.atlassian.hibernate.adapter.reflection.criterion;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.expression.IlikeExpression;

import java.util.function.Function;

public final class IlikeExpressionV2Reflection {
    private IlikeExpressionV2Reflection() { }

    private static final Function<Object, Object> PROPERTY_NAME_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(IlikeExpression.class, "propertyName");
    private static final Function<Object, Object> VALUE_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(IlikeExpression.class, "value");

    public static String getPropertyName(final IlikeExpression obj) {
        return (String) PROPERTY_NAME_FIELD.apply(obj);
    }

    public static Object getValue(final IlikeExpression obj) {
        return VALUE_FIELD.apply(obj);
    }
}
