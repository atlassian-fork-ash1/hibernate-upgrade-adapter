package com.atlassian.hibernate.adapter.reflection.criterion;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.expression.SimpleExpression;

import java.util.function.Function;

public final class SimpleExpressionV2Reflection {
    private SimpleExpressionV2Reflection() { }

    private static final Function<Object, Object> PROPERTY_NAME_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(SimpleExpression.class, "propertyName");
    private static final Function<Object, Object> VALUE_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(SimpleExpression.class, "value");
    private static final Function<Object, Object> IGNORE_CASE_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(SimpleExpression.class, "ignoreCase");

    public static String getPropertyName(final SimpleExpression obj) {
        return (String) PROPERTY_NAME_FIELD.apply(obj);
    }

    public static Object getValue(final SimpleExpression obj) {
        return VALUE_FIELD.apply(obj);
    }

    public static boolean getIgnoreCase(final SimpleExpression obj) {
        return (Boolean) IGNORE_CASE_FIELD.apply(obj);
    }
}
