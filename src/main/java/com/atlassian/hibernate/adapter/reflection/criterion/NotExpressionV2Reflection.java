package com.atlassian.hibernate.adapter.reflection.criterion;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.expression.Criterion;
import net.sf.hibernate.expression.NotExpression;

import java.util.function.Function;

public final class NotExpressionV2Reflection {
    private NotExpressionV2Reflection() { }

    private static final Function<Object, Object> CRITERION_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(NotExpression.class, "criterion");

    public static Criterion getCriterion(final NotExpression obj) {
        return (Criterion) CRITERION_FIELD.apply(obj);
    }
}
