package com.atlassian.hibernate.adapter.reflection.criterion;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.expression.Criterion;
import net.sf.hibernate.expression.LogicalExpression;

import java.util.function.Function;

public final class LogicalExpressionV2Reflection {
    private LogicalExpressionV2Reflection() { }

    private static final Function<Object, Object> LHS_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(LogicalExpression.class, "lhs");
    private static final Function<Object, Object> RHS_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(LogicalExpression.class, "rhs");

    public static Criterion getLhs(final LogicalExpression obj) {
        return (Criterion) LHS_FIELD.apply(obj);
    }

    public static Criterion getRhs(final LogicalExpression obj) {
        return (Criterion) RHS_FIELD.apply(obj);
    }
}
