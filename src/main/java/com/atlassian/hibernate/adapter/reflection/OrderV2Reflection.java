package com.atlassian.hibernate.adapter.reflection;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.expression.Order;

import java.util.function.Function;

public final class OrderV2Reflection {
    private OrderV2Reflection() { }

    private static final Function<Object, Object> PROPERTY_NAME_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(Order.class, "propertyName");
    private static final Function<Object, Object> ASCENDING_FIELD =
            ReflectionHelper.INSTANCE.getPrivateFieldGetter(Order.class, "ascending");

    public static String getPropertyName(final Order obj) {
        return (String) PROPERTY_NAME_FIELD.apply(obj);
    }

    public static boolean getAscending(final Order obj) {
        return (Boolean) ASCENDING_FIELD.apply(obj);
    }
}
