package com.atlassian.hibernate.adapter.reflection;

import com.atlassian.hibernate.util.reflection.ReflectionHelper;
import net.sf.hibernate.expression.Criterion;
import net.sf.hibernate.expression.Junction;

import java.util.List;
import java.util.function.Function;

public final class JunctionV2Reflection {
    private JunctionV2Reflection() { }

    private static final Function<Object, Object> CRITERIA_FIELD = ReflectionHelper.INSTANCE.getPrivateFieldGetter(
            Junction.class, "criteria");

    @SuppressWarnings("unchecked")
    public static List<Criterion> getCriteria(final Junction obj) {
        return (List<Criterion>) CRITERIA_FIELD.apply(obj);
    }
}
