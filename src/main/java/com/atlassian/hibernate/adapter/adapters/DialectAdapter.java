package com.atlassian.hibernate.adapter.adapters;

import com.atlassian.hibernate.util.dialect.PostgresPlusDialectCompatibleBlob;
import com.google.common.base.Throwables;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Properties;
import java.util.WeakHashMap;

/**
 * An adapter bridging the Dialog class between hibernate v2 to
 * and from v5 (onwards).
 */
public final class DialectAdapter {

    private static final Log LOG = LogFactory.getLog(DialectAdapter.class);

    private DialectAdapter() { }

    private static WeakHashMap<org.hibernate.dialect.Dialect, net.sf.hibernate.dialect.Dialect> v5tov2DialectMap =
            new WeakHashMap<>();
    private static WeakHashMap<net.sf.hibernate.dialect.Dialect, org.hibernate.dialect.Dialect> v2tov5DialectMap =
            new WeakHashMap<>();

    /**
     * Map a v2 dialect string to a v5 dialect string.
     */
    public static String adaptV2toV5(final String dialect) throws net.sf.hibernate.HibernateException {
        if (StringUtils.isEmpty(dialect))
            return dialect;

        if (dialect.equals("net.sf.hibernate.dialect.H2Dialect"))
            return org.hibernate.dialect.H2Dialect.class.getName();
        if (dialect.equals(net.sf.hibernate.dialect.Oracle9Dialect.class.getName()))
            return org.hibernate.dialect.Oracle9iDialect.class.getName();
        if (dialect.equals(net.sf.hibernate.dialect.Sybase11_9_2Dialect.class.getName()))
            return org.hibernate.dialect.Sybase11Dialect.class.getName();
        if (dialect.equals(net.sf.hibernate.dialect.PostgreSQLDialect.class.getName()))
            return PostgresPlusDialectCompatibleBlob.class.getName();

        // org.hibernate.dialect.SQLServerDialect may have a bug with it's TopLimitHandler impl
        if (dialect.equals(net.sf.hibernate.dialect.SQLServerDialect.class.getName()))
            return org.hibernate.dialect.SQLServer2005Dialect.class.getName();

        // try a drop in replacement by changing the namespace
        if (dialect.startsWith("net.sf.hibernate.dialect.")) {
            return dialect.replace("net.sf.hibernate.dialect.", "org.hibernate.dialect.");
        }

        // see if there's a class with a 'V5' suffix
        final Class dialectClass = classForNameNoException(dialect);
        if (dialectClass != null && org.hibernate.dialect.Dialect.class.isAssignableFrom(dialectClass)) {
            return dialect;
        }
        final Class dialectClassV5 = classForNameNoException(dialect + "V5");
        if (dialectClassV5 != null) {
            return dialect + "V5";
        }
        throw new net.sf.hibernate.HibernateException("Dialect class not found: " + dialect);
    }

    /**
     * Map a v2 dialect string to a v5 dialect string.
     */
    public static String adaptV5toV2(final String dialect) throws net.sf.hibernate.HibernateException {
        if (StringUtils.isEmpty(dialect))
            return dialect;

        if (dialect.equals(org.hibernate.dialect.H2Dialect.class.getName()))
            return "net.sf.hibernate.dialect.H2Dialect";
        if (dialect.equals(org.hibernate.dialect.Oracle9iDialect.class.getName()))
            return net.sf.hibernate.dialect.Oracle9Dialect.class.getName();
        if (dialect.equals(org.hibernate.dialect.Sybase11Dialect.class.getName()))
            return net.sf.hibernate.dialect.Sybase11_9_2Dialect.class.getName();
        if (dialect.equals(PostgresPlusDialectCompatibleBlob.class.getName())
                || dialect.equals(org.hibernate.dialect.PostgresPlusDialect.class.getName()))
            return net.sf.hibernate.dialect.PostgreSQLDialect.class.getName();

        if (dialect.equals(org.hibernate.dialect.SQLServer2005Dialect.class.getName())
                || dialect.equals(org.hibernate.dialect.SQLServer2008Dialect.class.getName())
                || dialect.equals(org.hibernate.dialect.SQLServer2012Dialect.class.getName()))
            return net.sf.hibernate.dialect.SQLServerDialect.class.getName();

        //find v2 for Oracle10Dialect.
        if (dialect.equals(org.hibernate.dialect.Oracle10gDialect.class.getName())) {
            final String dialectOracle10g = "net.sf.hibernate.dialect.OracleIntlDialect";
            if (classForNameNoException(dialectOracle10g) != null) {
                return dialectOracle10g;
            }
        }

        // try a drop in replacement by changing the namespace
        if (dialect.startsWith("org.hibernate.dialect.")) {
            return dialect.replace("org.hibernate.dialect.", "net.sf.hibernate.dialect.");
        }

        // see if this class has the 'V5' suffix
        if (dialect.endsWith("V5")) {
            final String dialectV2 = dialect.substring(0, dialect.length() - 2);
            final Class dialectClassV2 = classForNameNoException(dialectV2);
            if (dialectClassV2 != null) {
                return dialectV2;
            }
        } else {
            final Class dialectClass = classForNameNoException(dialect);
            if (dialectClass != null && net.sf.hibernate.dialect.Dialect.class.isAssignableFrom(dialectClass)) {
                return dialect;
            }
            final String dialectV2 = dialect + "V2";
            final Class dialectClassV2 = classForNameNoException(dialectV2);
            if (dialectClassV2 != null) {
                return dialectV2;
            }
        }
        throw new net.sf.hibernate.HibernateException("Dialect class not found: " + dialect);
    }

    /**
     * Store a mapping between a v2 and v5 onwards dialect object, so that the v5 object
     * can be returned from the v2 object later from the 'adapt' method.
     */
    public static synchronized void mapDialectObjects(
            final net.sf.hibernate.dialect.Dialect v2,
            final org.hibernate.dialect.Dialect v5) {

        v2tov5DialectMap.put(v2, v5);
        v5tov2DialectMap.put(v5, v2);
    }

    /**
     * Get a v5 dialect object from a v2 dialect object.
     */
    public static synchronized org.hibernate.dialect.Dialect adapt(final net.sf.hibernate.dialect.Dialect dialect) {
        if (dialect == null)
            return null;

        org.hibernate.dialect.Dialect result = v2tov5DialectMap.get(dialect);
        if (result == null) {
            try {
                String dialectNameV5 = adaptV2toV5(dialect.getClass().getName());
                result = org.hibernate.dialect.Dialect.getDialect(createPropertiesWithDialect(dialectNameV5));
            } catch (net.sf.hibernate.HibernateException ex) {
                throw Throwables.propagate(ex);
            }
            mapDialectObjects(dialect, result);
        }
        return result;
    }

    /**
     * Get a v2 dialect object from a v5 dialect object.
     */
    public static synchronized net.sf.hibernate.dialect.Dialect adapt(final org.hibernate.dialect.Dialect dialect) {
        if (dialect == null)
            return null;

        net.sf.hibernate.dialect.Dialect result = v5tov2DialectMap.get(dialect);
        if (result == null) {
            try {
                String dialectNameV2 = adaptV5toV2(dialect.getClass().getName());
                result = net.sf.hibernate.dialect.Dialect.getDialect(createPropertiesWithDialect(dialectNameV2));
            } catch (net.sf.hibernate.HibernateException ex) {
                throw Throwables.propagate(ex);
            }
            mapDialectObjects(result, dialect);
        }
        return result;
    }

    private static Class classForNameNoException(final String className) {
        try {
            return net.sf.hibernate.util.ReflectHelper.classForName(className);
        } catch (final ClassNotFoundException ex) {
            LOG.error("Error loading dialect class: " + ex.getMessage(), ex);
            return null;
        }
    }

    private static Properties createPropertiesWithDialect(final String dialectName) {
        Properties properties = new Properties();
        properties.setProperty(org.hibernate.cfg.Environment.DIALECT, dialectName);
        return properties;
    }
}
