package com.atlassian.hibernate.adapter.adapters.cache;

import org.hibernate.cache.spi.QueryResultsRegion;

/**
 * An adapter bridging the QueryResultsRegion interface for hibernate v2 to v5 (onwards).
 */
public class QueryResultsRegionV5Adapter extends GeneralDataRegionV5Adapter implements QueryResultsRegion {

    protected QueryResultsRegionV5Adapter(
            final String regionName,
            final net.sf.hibernate.cache.Cache underlyingCache) {

        super(regionName, underlyingCache);
    }

    public static QueryResultsRegion adapt(
            final String regionName,
            final net.sf.hibernate.cache.Cache underlyingCache) {

        if (underlyingCache == null) {
            return null;
        }
        return new QueryResultsRegionV5Adapter(regionName, underlyingCache);
    }
}
