package com.atlassian.hibernate.adapter.adapters.generator;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.id.IdentifierGeneratorHelper;

import java.io.Serializable;
import java.sql.SQLException;

/**
 * An adapter bridging the IdentifierGenerator interface for hibernate v2 to v5 (onwards).
 */
public class IdentifierGeneratorV5Adapter implements IdentifierGenerator {
    private final net.sf.hibernate.id.IdentifierGenerator generator;

    protected IdentifierGeneratorV5Adapter(final net.sf.hibernate.id.IdentifierGenerator generator) {
        this.generator = generator;
    }

    public static IdentifierGenerator adapt(final net.sf.hibernate.id.IdentifierGenerator generator) {
        if (generator == null)
            return null;
        if (generator instanceof IdentifierGenerator)
            return (IdentifierGenerator) generator;

        // instanceof Assigned is used in confluence code
        if (generator instanceof net.sf.hibernate.id.Assigned)
            return new org.hibernate.id.Assigned();

        return new IdentifierGeneratorV5Adapter(generator);
    }

    public net.sf.hibernate.id.IdentifierGenerator getV2IdentifierGenerator() {
        return generator;
    }

    @Override
    public Serializable generate(final SharedSessionContractImplementor session, final Object object)
            throws HibernateException {

        final HibernateBridge bridge = HibernateBridge.get(session.getFactory());
        final net.sf.hibernate.engine.SessionImplementor sessionV2 = (net.sf.hibernate.engine.SessionImplementor)
                bridge.getV2orV5Session((Session) session);
        try {
            final Serializable id = generator.generate(sessionV2, object);
            if (id == net.sf.hibernate.id.IdentifierGeneratorFactory.IDENTITY_COLUMN_INDICATOR)
                return IdentifierGeneratorHelper.POST_INSERT_INDICATOR;
            if (id == net.sf.hibernate.id.IdentifierGeneratorFactory.SHORT_CIRCUIT_INDICATOR)
                return IdentifierGeneratorHelper.SHORT_CIRCUIT_INDICATOR;
            return id;
        } catch (final SQLException ex) {
            throw HibernateExceptionAdapter.toV5HibernateException(ex, "Generating identifier");
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }
}
