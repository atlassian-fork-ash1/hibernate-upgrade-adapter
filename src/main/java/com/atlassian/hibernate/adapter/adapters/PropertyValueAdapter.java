package com.atlassian.hibernate.adapter.adapters;

import com.atlassian.hibernate.adapter.adapters.type.collection.PersistentCollectionV2Adapter;

/**
 * An adapter bridging property values between hibernate v2 / v5 (onwards).
 */
public final class PropertyValueAdapter {
    private PropertyValueAdapter() { }

    public static Object adaptToV2(final Object value) {
        return value;
    }

    public static Object[] adaptToV2(final Object[] values) {
        if (values == null)
            return null;

        final Object[] adapted = new Object[values.length];
        for (int i = 0; i < values.length; i++) {
            adapted[i] = adaptToV2(values[i]);
        }
        return adapted;
    }

    public static Object adaptToV5(final Object value) {
        if (value instanceof PersistentCollectionV2Adapter) {
            return ((PersistentCollectionV2Adapter) value).getV5Collection();
        }
        return value;
    }

    public static Object[] adaptToV5(final Object[] values) {
        if (values == null)
            return null;

        final Object[] adapted = new Object[values.length];
        for (int i = 0; i < values.length; i++) {
            adapted[i] = adaptToV5(values[i]);
        }
        return adapted;
    }
}
