package com.atlassian.hibernate.adapter.adapters.criteria;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.LockModeAdapter;
import com.atlassian.hibernate.adapter.adapters.criterion.CriterionV5Adapter;
import net.sf.hibernate.Criteria;
import net.sf.hibernate.FetchMode;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.LockMode;
import net.sf.hibernate.expression.Criterion;
import net.sf.hibernate.expression.Order;
import net.sf.hibernate.transform.ResultTransformer;
import org.apache.commons.lang3.NotImplementedException;

import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.List;

/**
 * An adapter bridging the Criteria interface for hibernate v5 (onwards) to v2.
 */
public class CriteriaV2Adapter implements Criteria {

    private final org.hibernate.SessionFactory sessionFactory;
    private final org.hibernate.Criteria criteria;
    private final List<Criterion> criterionQueue = new ArrayList<>();

    protected CriteriaV2Adapter(final org.hibernate.SessionFactory sessionFactory, final org.hibernate.Criteria criteria) {
        this.sessionFactory = sessionFactory;
        this.criteria = criteria;
    }

    public static Criteria adapt(final org.hibernate.SessionFactory sessionFactory, final org.hibernate.Criteria criteria) {
        if (criteria == null) {
            return null;
        }
        return new CriteriaV2Adapter(sessionFactory, criteria);
    }

    @Override
    public Criteria setMaxResults(final int maxResults) {
        criteria.setMaxResults(maxResults);
        return this;
    }

    @Override
    public Criteria setFirstResult(final int firstResult) {
        criteria.setFirstResult(firstResult);
        return this;
    }

    @Override
    public Criteria setFetchSize(final int fetchSize) {
        criteria.setFetchSize(fetchSize);
        return null;
    }

    @Override
    public Criteria setTimeout(final int timeout) {
        criteria.setTimeout(timeout);
        return null;
    }

    @Override
    public Criteria add(final Criterion criterion) {
        criterionQueue.add(criterion);
        return this;
    }

    @Override
    public Criteria addOrder(final Order order) {
        criteria.addOrder(OrderAdapter.adapt(order));
        return null;
    }

    @Override
    public List list() throws HibernateException {
        try {
            processCriterionBacklog();
            return criteria.list();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Object uniqueResult() throws HibernateException {
        try {
            processCriterionBacklog();
            return criteria.uniqueResult();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Criteria setFetchMode(final String associationPath, final FetchMode mode)
            throws HibernateException {
        try {
            criteria.setFetchMode(associationPath, FetchModeAdapter.adapt(mode));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
        return this;
    }

    @Override
    public Criteria createAlias(final String associationPath, final String alias)
            throws HibernateException {
        try {
            processCriterionBacklog();
            criteria.createAlias(associationPath, alias);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
        return this;
    }

    @Override
    public Criteria createCriteria(final String associationPath)
            throws HibernateException {
        try {
            processCriterionBacklog();
            criteria.createCriteria(associationPath);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
        return this;
    }

    @Override
    public Criteria createCriteria(final String associationPath, final String alias)
            throws HibernateException {
        try {
            processCriterionBacklog();
            criteria.createCriteria(associationPath, alias);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
        return this;
    }

    @Override
    public Class getCriteriaClass() {
        throw new NotImplementedException("getCriteriaClass not implemented");
    }

    public Class getCriteriaClass(final String alias) {
        throw new NotImplementedException("getCriteriaClass not implemented");
    }

    @Override
    @Deprecated
    public Criteria returnMaps() {
        criteria.setResultTransformer(org.hibernate.transform.AliasToEntityMapResultTransformer.INSTANCE);
        return this;
    }

    @Override
    @Deprecated
    public Criteria returnRootEntities() {
        criteria.setResultTransformer(org.hibernate.transform.RootEntityResultTransformer.INSTANCE);
        return this;
    }

    @Override
    public Criteria setResultTransformer(final ResultTransformer resultTransformer) {
        criteria.setResultTransformer(new ResultTransformerV2Adapter(resultTransformer));
        return this;
    }

    @Override
    public Criteria setLockMode(final LockMode lockMode) {
        criteria.setLockMode(LockModeAdapter.adapt(lockMode));
        return this;
    }

    @Override
    public Criteria setLockMode(final String alias, final LockMode lockMode) {
        criteria.setLockMode(alias, LockModeAdapter.adapt(lockMode));
        return this;
    }

    @Override
    public Criteria setCacheable(final boolean cacheable) {
        criteria.setCacheable(cacheable);
        return this;
    }

    @Override
    public Criteria setCacheRegion(final String cacheRegion) {
        criteria.setCacheRegion(cacheRegion);
        return this;
    }

    @Override
    public String toString() {
        processCriterionBacklog();
        return criteria.toString();
    }

    private void processCriterionBacklog() {
        for (Criterion criterion : criterionQueue) {
            criteria.add(CriterionV5Adapter.adapt(sessionFactory, criterion));
        }
        criterionQueue.clear();
    }
}
