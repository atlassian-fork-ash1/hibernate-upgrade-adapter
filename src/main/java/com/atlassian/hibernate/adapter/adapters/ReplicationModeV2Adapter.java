package com.atlassian.hibernate.adapter.adapters;

/**
 * An adapter bridging the ReplicationMode enum between hibernate v2 / v5 (onwards).
 */
public final class ReplicationModeV2Adapter {
    private ReplicationModeV2Adapter() {
    }

    public static net.sf.hibernate.ReplicationMode adapt(final org.hibernate.ReplicationMode value) {
        if (value == null)
            return null;
        if (value == org.hibernate.ReplicationMode.EXCEPTION)
            return net.sf.hibernate.ReplicationMode.EXCEPTION;
        if (value == org.hibernate.ReplicationMode.IGNORE)
            return net.sf.hibernate.ReplicationMode.IGNORE;
        if (value == org.hibernate.ReplicationMode.OVERWRITE)
            return net.sf.hibernate.ReplicationMode.OVERWRITE;
        if (value == org.hibernate.ReplicationMode.LATEST_VERSION)
            return net.sf.hibernate.ReplicationMode.LATEST_VERSION;
        throw new IllegalArgumentException("Unexpected org.hibernate.ReplicationMode value: " + value);
    }

    public static org.hibernate.ReplicationMode adapt(final net.sf.hibernate.ReplicationMode value) {
        if (value == null)
            return null;
        if (value == net.sf.hibernate.ReplicationMode.EXCEPTION)
            return org.hibernate.ReplicationMode.EXCEPTION;
        if (value == net.sf.hibernate.ReplicationMode.IGNORE)
            return org.hibernate.ReplicationMode.IGNORE;
        if (value == net.sf.hibernate.ReplicationMode.OVERWRITE)
            return org.hibernate.ReplicationMode.OVERWRITE;
        if (value == net.sf.hibernate.ReplicationMode.LATEST_VERSION)
            return org.hibernate.ReplicationMode.LATEST_VERSION;
        throw new IllegalArgumentException("Unexpected net.sf.hibernate.ReplicationMode value: " + value);
    }
}
