package com.atlassian.hibernate.adapter.adapters.cache;

/**
 * An adapter bridging the SoftLock interface for hibernate
 * between v2 / v5 (onwards).
 */
public final class SoftLockAdapter {

    private SoftLockAdapter() { }

    /**
     * Cast from a v2 instance to a v5 onwards instance.
     */
    public static net.sf.hibernate.cache.CacheConcurrencyStrategy.SoftLock adapt(
            final org.hibernate.cache.spi.access.SoftLock lock) {

        if (lock == null) {
            return null;
        }
        if (lock instanceof HXLockHolder) {
            return ((HXLockHolder) lock).lock;
        }
        final H2LockHolder result = new H2LockHolder();
        result.lock = lock;
        return result;
    }

    /**
     * Cast from a v5 onwards instance to a v2 instance.
     */
    public static org.hibernate.cache.spi.access.SoftLock adapt(
            final net.sf.hibernate.cache.CacheConcurrencyStrategy.SoftLock lock) {

        if (lock == null) {
            return null;
        }
        if (lock instanceof H2LockHolder) {
            return ((H2LockHolder) lock).lock;
        }
        final HXLockHolder result = new HXLockHolder();
        result.lock = lock;
        return result;
    }

    private static class H2LockHolder implements net.sf.hibernate.cache.CacheConcurrencyStrategy.SoftLock {
        @SuppressWarnings("checkstyle:visibilitymodifier")
        public org.hibernate.cache.spi.access.SoftLock lock;
    }

    private static class HXLockHolder implements org.hibernate.cache.spi.access.SoftLock {
        @SuppressWarnings("checkstyle:visibilitymodifier")
        public net.sf.hibernate.cache.CacheConcurrencyStrategy.SoftLock lock;
    }
}
