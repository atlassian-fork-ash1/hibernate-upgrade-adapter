package com.atlassian.hibernate.adapter.adapters.criteria;

/**
 * An adapter bridging the FetchMode enum for hibernate
 * between v2 / v5 (onwards).
 */
public final class FetchModeAdapter {
    private FetchModeAdapter() {
    }

    public static net.sf.hibernate.FetchMode adapt(final org.hibernate.FetchMode value) {
        if (value == null)
            return null;
        if (value == org.hibernate.FetchMode.JOIN)
            return net.sf.hibernate.FetchMode.EAGER;
        if (value == org.hibernate.FetchMode.SELECT)
            return net.sf.hibernate.FetchMode.LAZY;
        if (value == org.hibernate.FetchMode.DEFAULT)
            return net.sf.hibernate.FetchMode.DEFAULT;
        throw new IllegalArgumentException("Unsupported org.hibernate.FetchMode value: " + value);
    }

    public static org.hibernate.FetchMode adapt(final net.sf.hibernate.FetchMode value) {
        if (value == null)
            return null;
        if (value == net.sf.hibernate.FetchMode.EAGER)
            return org.hibernate.FetchMode.JOIN;
        if (value == net.sf.hibernate.FetchMode.LAZY)
            return org.hibernate.FetchMode.SELECT;
        if (value == net.sf.hibernate.FetchMode.DEFAULT)
            return org.hibernate.FetchMode.DEFAULT;
        throw new IllegalArgumentException("Unsupported net.sf.hibernate.FetchMode value: " + value);
    }
}
