package com.atlassian.hibernate.adapter.adapters.session;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.adapters.ConnectionProviderV2Adapter;
import com.atlassian.hibernate.adapter.adapters.DialectAdapter;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.interceptor.InterceptorV5Adapter;
import com.atlassian.hibernate.adapter.adapters.mapping.ClassMetadataV2Adapter;
import com.atlassian.hibernate.adapter.adapters.mapping.CollectionMetadataV2Adapter;
import com.atlassian.hibernate.adapter.adapters.persister.ClassPersisterV2Adapter;
import com.atlassian.hibernate.adapter.adapters.persister.CollectionPersisterV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV2Adapter;
import com.atlassian.hibernate.adapter.util.HibernateBridgeHolder;
import com.atlassian.hibernate.util.FastClearStatefulPersistenceContext;
import com.atlassian.hibernate.util.ThreadSafeCheckingSessionEventListener;
import net.sf.hibernate.Databinder;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Interceptor;
import net.sf.hibernate.JDBCException;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.Session;
import net.sf.hibernate.SessionFactory;
import net.sf.hibernate.cache.QueryCache;
import net.sf.hibernate.collection.CollectionPersister;
import net.sf.hibernate.connection.ConnectionProvider;
import net.sf.hibernate.dialect.Dialect;
import net.sf.hibernate.engine.SessionFactoryImplementor;
import net.sf.hibernate.exception.SQLExceptionConverter;
import net.sf.hibernate.metadata.ClassMetadata;
import net.sf.hibernate.metadata.CollectionMetadata;
import net.sf.hibernate.persister.ClassPersister;
import net.sf.hibernate.type.Type;
import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.engine.transaction.jta.platform.internal.TransactionManagerAccess;
import org.hibernate.engine.transaction.jta.platform.spi.JtaPlatform;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.persistence.PersistenceException;
import javax.transaction.TransactionManager;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * An adapter bridging the SessionFactory interface for hibernate v5 (onwards) to v2.
 */
public class SessionFactoryV2Adapter implements SessionFactoryImplementor {
    private final org.hibernate.engine.spi.SessionFactoryImplementor sessionFactory;
    private final HibernateBridge hibernateBridge;
    private final Function<org.hibernate.Session, Session> sessionAdapter;

    protected SessionFactoryV2Adapter(
            final org.hibernate.engine.spi.SessionFactoryImplementor sessionFactory,
            final HibernateBridge hibernateBridge,
            final Function<org.hibernate.Session, Session> sessionAdapter) {

        this.sessionFactory = sessionFactory;
        this.hibernateBridge = hibernateBridge;
        this.sessionAdapter = sessionAdapter != null ? sessionAdapter : session -> SessionV2Adapter.adapt(session, this);
    }

    public static SessionFactory adapt(
            final org.hibernate.engine.spi.SessionFactoryImplementor sessionFactory,
            final HibernateBridge hibernateBridge) {

        return adapt(sessionFactory, hibernateBridge, null);
    }

    public static SessionFactory adapt(
            final org.hibernate.engine.spi.SessionFactoryImplementor sessionFactory,
            final HibernateBridge hibernateBridge,
            final Function<org.hibernate.Session, Session> sessionAdapter) {

        if (sessionFactory == null) {
            return null;
        }
        return new SessionFactoryV2Adapter(sessionFactory, hibernateBridge, sessionAdapter);
    }

    protected org.hibernate.engine.spi.SessionFactoryImplementor getSessionFactoryV5() {
        return sessionFactory;
    }

    @Override
    @SuppressWarnings("deprecation")
    public ClassPersister getPersister(final String className) throws MappingException {

        // this is how SessionFactoryBridgeAssociations finds the HibernateBridge
        if (className != null && className.equals(HibernateBridge.class.getName())) {
            return new HibernateBridgeHolder(hibernateBridge);
        }

        try {
            return ClassPersisterV2Adapter.adapt(getSessionFactoryV5().getEntityPersister(className), getSessionFactoryV5(), null);
        } catch (final org.hibernate.MappingException ex) {
            throw (MappingException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    protected org.hibernate.Session setupV5Session(final org.hibernate.Session session) {
        session.addEventListeners(new ThreadSafeCheckingSessionEventListener());
        FastClearStatefulPersistenceContext.updatePersistenceContext((SessionImplementor) session);
        return session;
    }

    //---------- SessionFactory ----------//

    @Override
    public Session openSession(final Connection connection) {
        final org.hibernate.Session sessionV5 = getSessionFactoryV5().withOptions().connection(connection).openSession();
        setupV5Session(sessionV5);
        return sessionAdapter.apply(sessionV5);
    }

    @Override
    public Session openSession(final Interceptor interceptor) throws HibernateException {
        try {
            final org.hibernate.Session sessionV5 = getSessionFactoryV5().withOptions().interceptor(InterceptorV5Adapter.adapt(interceptor)).openSession();
            setupV5Session(sessionV5);
            return sessionAdapter.apply(sessionV5);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Session openSession(final Connection connection, final Interceptor interceptor) {
        final org.hibernate.Session sessionV5 = getSessionFactoryV5().withOptions().connection(connection).interceptor(InterceptorV5Adapter.adapt(interceptor)).openSession();
        setupV5Session(sessionV5);
        return sessionAdapter.apply(sessionV5);
    }

    @Override
    public Session openSession() throws HibernateException {
        try {
            return sessionAdapter.apply(setupV5Session(getSessionFactoryV5().openSession()));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Databinder openDatabinder() throws HibernateException {
        throw new NotImplementedException("openDatabinder not implemented");
    }

    @Override
    @SuppressWarnings("deprecation")
    public ClassMetadata getClassMetadata(final Class persistentClass) throws HibernateException {
        try {
            return ClassMetadataV2Adapter.adapt(getSessionFactoryV5().getClassMetadata(persistentClass), getSessionFactoryV5(), null);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    public CollectionMetadata getCollectionMetadata(final String roleName) throws HibernateException {
        try {
            return CollectionMetadataV2Adapter.adapt(getSessionFactoryV5().getCollectionMetadata(roleName), getSessionFactoryV5());
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    public Map getAllClassMetadata() throws HibernateException {
        try {
            final Map<Class, ClassMetadata> map = new HashMap<>();
            for (String entityName : getSessionFactoryV5().getMetamodel().getAllEntityNames()) {
                org.hibernate.metadata.ClassMetadata metadata = getSessionFactoryV5().getClassMetadata(entityName);
                map.put(metadata.getMappedClass(), ClassMetadataV2Adapter.adapt(metadata, getSessionFactoryV5(), null));
            }
            return map;
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Map getAllCollectionMetadata() throws HibernateException {
        try {
            final Map<String, CollectionMetadata> map = new HashMap<>();
            for (final Object entryObj : getSessionFactoryV5().getMetamodel().collectionPersisters().entrySet()) {
                final Map.Entry<String, org.hibernate.metadata.CollectionMetadata> entry =
                        (Map.Entry<String, org.hibernate.metadata.CollectionMetadata>) entryObj;
                map.put(entry.getKey(), CollectionMetadataV2Adapter.adapt(entry.getValue(), getSessionFactoryV5()));
            }
            return map;
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void close() throws HibernateException {
        try {
            getSessionFactoryV5().close();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void evict(final Class persistentClass) throws HibernateException {
        try {
            getSessionFactoryV5().getCache().evictEntityRegion(persistentClass);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void evict(final Class persistentClass, final Serializable id) throws HibernateException {
        throw new NotImplementedException("evict(Class, Serializable) not implemented");
    }

    @Override
    public void evictCollection(final String roleName) throws HibernateException {
        try {
            getSessionFactoryV5().getCache().evictCollectionRegion(roleName);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void evictCollection(final String roleName, final Serializable id) throws HibernateException {
        throw new NotImplementedException("evictCollection(String, Serializable) not implemented");
    }

    @Override
    public void evictQueries() throws HibernateException {
        try {
            getSessionFactoryV5().getCache().evictQueryRegions();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void evictQueries(final String cacheRegion) throws HibernateException {
        try {
            getSessionFactoryV5().getCache().evictQueryRegion(cacheRegion);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public SQLExceptionConverter getSQLExceptionConverter() {
        return (SQLException sqlException, String message) ->
                (JDBCException) HibernateExceptionAdapter.adapt(
                        HibernateExceptionAdapter.toV5HibernateException(sqlException, message));
    }

    //---------- SessionFactoryImplementor ----------//

    @Override
    @SuppressWarnings("deprecation")
    public ClassPersister getPersister(final Class clazz) throws MappingException {
        try {
            return ClassPersisterV2Adapter.adapt(
                    getSessionFactoryV5().getEntityPersister(clazz.getName()), getSessionFactoryV5(), null);
        } catch (final org.hibernate.MappingException ex) {
            throw (MappingException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    @SuppressWarnings("deprecation")
    public CollectionPersister getCollectionPersister(final String role) throws MappingException {
        try {
            return CollectionPersisterV2Adapter.adapt(getSessionFactoryV5().getCollectionPersister(role), getSessionFactoryV5());
        } catch (final org.hibernate.MappingException ex) {
            throw (MappingException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean isOuterJoinedFetchEnabled() {
        throw new NotImplementedException("isOuterJoinedFetchEnabled not implemented");
    }

    @Override
    public boolean isScrollableResultSetsEnabled() {
        return getSessionFactoryV5().getSessionFactoryOptions().isScrollableResultSetsEnabled();
    }

    @Override
    public boolean isGetGeneratedKeysEnabled() {
        return getSessionFactoryV5().getSessionFactoryOptions().isGetGeneratedKeysEnabled();
    }

    @Override
    @SuppressWarnings("deprecation")
    public String getDefaultSchema() {
        return getSessionFactoryV5().getSettings().getDefaultSchemaName();
    }

    @Override
    @SuppressWarnings("deprecation")
    public Dialect getDialect() {
        return DialectAdapter.adapt(getSessionFactoryV5().getDialect());
    }

    @Override
    @SuppressWarnings("deprecation")
    public Type[] getReturnTypes(final String queryString) throws HibernateException {
        try {
            return TypeV2Adapter.adapt(getSessionFactoryV5().getReturnTypes(queryString));
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public ConnectionProvider getConnectionProvider() {
        final org.hibernate.engine.jdbc.connections.spi.ConnectionProvider connectionProvider =
                getSessionFactoryV5().getServiceRegistry().getService(
                        org.hibernate.engine.jdbc.connections.spi.ConnectionProvider.class);

        return ConnectionProviderV2Adapter.adapt(connectionProvider);
    }

    @Override
    public String[] getImplementors(final Class clazz) {
        throw new NotImplementedException("getImplementors not implemented");
    }

    @Override
    public String getImportedClassName(final String name) {
        throw new NotImplementedException("getImportedClassName not implemented");
    }

    @Override
    public int getJdbcBatchSize() {
        return getSessionFactoryV5().getSessionFactoryOptions().getJdbcBatchSize();
    }

    @Override
    public Integer getJdbcFetchSize() {
        return getSessionFactoryV5().getSessionFactoryOptions().getJdbcFetchSize();
    }

    @Override
    public Integer getMaximumFetchDepth() {
        return getSessionFactoryV5().getSessionFactoryOptions().getMaximumFetchDepth();
    }

    @Override
    public TransactionManager getTransactionManager() {
        final JtaPlatform jta = getSessionFactoryV5().getServiceRegistry().getService(JtaPlatform.class);
        return
                jta instanceof TransactionManagerAccess
                ? ((TransactionManagerAccess) jta).getTransactionManager()
                : null;
    }

    @Override
    public boolean isShowSqlEnabled() {
        throw new NotImplementedException("isShowSqlEnabled not implemented");
    }

    @Override
    public QueryCache getQueryCache() {
        throw new NotImplementedException("getQueryCache not implemented");
    }

    @Override
    public QueryCache getQueryCache(final String regionName) throws HibernateException {
        throw new NotImplementedException("getQueryCache not implemented");
    }

    @Override
    public boolean isQueryCacheEnabled() {
        return getSessionFactoryV5().getSessionFactoryOptions().isQueryCacheEnabled();
    }

    @Override
    public boolean isJdbcBatchVersionedData() {
        return getSessionFactoryV5().getSessionFactoryOptions().isJdbcBatchVersionedData();
    }

    @Override
    public boolean isWrapResultSetsEnabled() {
        return getSessionFactoryV5().getSessionFactoryOptions().isWrapResultSetsEnabled();
    }

    @Override
    public Type getIdentifierType(final Class persistentClass) throws MappingException {
        try {
            return TypeV2Adapter.adapt(getSessionFactoryV5().getIdentifierType(persistentClass.getName()));
        } catch (final org.hibernate.MappingException ex) {
            throw (MappingException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public String getIdentifierPropertyName(final Class persistentClass) throws MappingException {
        try {
            return getSessionFactoryV5().getIdentifierPropertyName(persistentClass.getName());
        } catch (final org.hibernate.MappingException ex) {
            throw (MappingException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Type getPropertyType(final Class persistentClass, final String propertyName) throws MappingException {
        throw new NotImplementedException("getPropertyType not implemented");
    }

    @Override
    public Reference getReference() throws NamingException {
        throw new NotImplementedException("getReference not implemented");
    }
}
