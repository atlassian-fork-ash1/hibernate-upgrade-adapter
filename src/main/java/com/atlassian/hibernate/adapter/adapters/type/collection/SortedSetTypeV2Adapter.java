package com.atlassian.hibernate.adapter.adapters.type.collection;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.collection.CollectionPersister;
import net.sf.hibernate.collection.PersistentCollection;
import net.sf.hibernate.engine.Mapping;
import net.sf.hibernate.engine.SessionFactoryImplementor;
import net.sf.hibernate.engine.SessionImplementor;
import net.sf.hibernate.persister.Joinable;
import net.sf.hibernate.type.ForeignKeyDirection;
import net.sf.hibernate.type.SortedSetType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Map;

/**
 * An adapter bridging the SortedSetType class from hibernate v5 (onwards) to v2.
 */
class SortedSetTypeV2Adapter extends SortedSetType {
    private final org.hibernate.type.SortedSetType type;
    private final PersistentCollectionTypeV2Adapter impl;

    public SortedSetTypeV2Adapter(final org.hibernate.type.SortedSetType type) {
        super(type.getRole(), null);
        this.type = type;
        this.impl = new PersistentCollectionTypeV2Adapter(type);
    }

    public static SortedSetType adapt(final org.hibernate.type.SortedSetType type) {
        if (type == null)
            return null;
        return new SortedSetTypeV2Adapter(type);
    }

    public org.hibernate.type.Type getV5Type() {
        return type;
    }

    //---------- Delegate to PersistentCollectionTypeV2Adapter ----------//

    @Override
    public boolean equals(final Object object) {
        return impl.equals(object);
    }

    @Override
    public int hashCode() {
        return impl.hashCode();
    }

    @Override
    public String toString() {
        return impl.toString();
    }

    @Override
    public Class getReturnedClass() {
        return impl.getReturnedClass();
    }

    @Override
    public boolean isAssociationType() {
        return impl.isAssociationType();
    }

    @Override
    public boolean isPersistentCollectionType() {
        return impl.isPersistentCollectionType();
    }

    @Override
    public boolean isComponentType() {
        return impl.isComponentType();
    }

    @Override
    public boolean isEntityType() {
        return impl.isEntityType();
    }

    @Override
    public Object assemble(final Serializable cached, final SessionImplementor session, final Object owner)
            throws HibernateException {
        return impl.assemble(cached, session, owner);
    }

    @Override
    public Serializable disassemble(final Object value, final SessionImplementor session)
            throws HibernateException {
        return impl.disassemble(value, session);
    }

    @Override
    public boolean isDirty(final Object old, final Object current, final SessionImplementor session)
            throws HibernateException {
        return impl.isDirty(old, current, session);
    }

    @Override
    public Object hydrate(final ResultSet rs, final String[] name, final SessionImplementor session, final Object owner)
            throws HibernateException, SQLException {
        return impl.hydrate(rs, name, session, owner);
    }

    @Override
    public Object resolveIdentifier(final Object value, final SessionImplementor session, final Object owner)
            throws HibernateException {
        return impl.resolveIdentifier(value, session, owner);
    }

    @Override
    public boolean isObjectType() {
        return impl.isObjectType();
    }

    @Override
    public boolean isModified(
            final Object old,
            final Object current,
            final SessionImplementor session)
            throws HibernateException {
        return impl.isModified(old, current, session);
    }

    @Override
    public Object copy(final Object original, final Object target, final SessionImplementor session, final Object owner, final Map copiedAlready)
            throws HibernateException {
        return impl.copy(original, target, session, owner, copiedAlready);
    }

    @Override
    public String getRole() {
        return impl.getRole();
    }

    @Override
    public PersistentCollection instantiate(final SessionImplementor session, final CollectionPersister persister) {
        return impl.instantiate(session, persister);
    }

    @Override
    public Object nullSafeGet(final ResultSet rs, final String name, final SessionImplementor session, final Object owner)
            throws HibernateException, SQLException {
        return impl.nullSafeGet(rs, name, session, owner);
    }

    @Override
    public Object nullSafeGet(final ResultSet rs, final String[] name, final SessionImplementor session, final Object owner)
            throws HibernateException, SQLException {
        return impl.nullSafeGet(rs, name, session, owner);
    }

    @Override
    public void nullSafeSet(final PreparedStatement st, final Object value, final int index, final SessionImplementor session)
            throws HibernateException, SQLException {
        impl.nullSafeSet(st, value, index, session);
    }

    @Override
    public int[] sqlTypes(final Mapping session) throws MappingException {
        return impl.sqlTypes(session);
    }

    @Override
    public int getColumnSpan(final Mapping session) throws MappingException {
        return impl.getColumnSpan(session);
    }

    @Override
    public String toString(final Object value, final SessionFactoryImplementor factory) throws HibernateException {
        return impl.toString(value, factory);
    }

    @Override
    public Object fromString(final String xml) {
        return impl.fromString(xml);
    }

    @Override
    public Object deepCopy(final Object value) throws HibernateException {
        return impl.deepCopy(value);
    }

    @Override
    public String getName() {
        return impl.getName();
    }

    @Override
    public Iterator getElementsIterator(final Object collection) {
        return impl.getElementsIterator(collection);
    }

    @Override
    public boolean isMutable() {
        return impl.isMutable();
    }

    @Override
    @Deprecated
    public boolean hasNiceEquals() {
        return impl.hasNiceEquals();
    }

    @Override
    public PersistentCollection wrap(final SessionImplementor session, final Object collection) {
        return impl.wrap(session, collection);
    }

    @Override
    public ForeignKeyDirection getForeignKeyDirection() {
        return impl.getForeignKeyDirection();
    }

    @Override
    public boolean isArrayType() {
        return impl.isArrayType();
    }

    @Override
    public boolean usePrimaryKeyAsForeignKey() {
        return impl.usePrimaryKeyAsForeignKey();
    }

    @Override
    public Joinable getJoinable(final SessionFactoryImplementor factory) throws MappingException {
        return impl.getJoinable(factory);
    }

    @Override
    public String[] getReferencedColumns(final SessionFactoryImplementor factory) throws MappingException {
        return impl.getReferencedColumns(factory);
    }

    @Override
    public Class getAssociatedClass(final SessionFactoryImplementor factory) throws MappingException {
        return impl.getAssociatedClass(factory);
    }
}
