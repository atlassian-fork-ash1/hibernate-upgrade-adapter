package com.atlassian.hibernate.adapter.adapters.mapping;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV5Adapter;
import com.atlassian.hibernate.util.ThrowableUtil;
import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.MappingException;
import org.hibernate.engine.spi.Mapping;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.id.factory.IdentifierGeneratorFactory;
import org.hibernate.type.Type;

/**
 * An adapter bridging the Mapping interface for hibernate v2 to v5 (onwards).
 */
public class MappingV5Adapter implements Mapping {
    private final net.sf.hibernate.engine.Mapping mapping;

    protected MappingV5Adapter(final net.sf.hibernate.engine.Mapping mapping) {
        this.mapping = mapping;
    }

    public static Mapping adapt(final net.sf.hibernate.engine.Mapping mapping) {
        if (mapping == null)
            return null;
        if (mapping instanceof net.sf.hibernate.SessionFactory)
            return (SessionFactoryImplementor) HibernateBridge.get((net.sf.hibernate.SessionFactory) mapping).getV5SessionFactory();
        return new MappingV5Adapter(mapping);
    }

    private static Class findClass(final String className) {
        try {
            return Class.forName(className);
        } catch (final ClassNotFoundException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    @Deprecated
    public IdentifierGeneratorFactory getIdentifierGeneratorFactory() {
        throw new NotImplementedException("getIdentifierGeneratorFactory not implemented");
    }

    @Override
    public Type getIdentifierType(final String className) throws MappingException {
        try {
            return TypeV5Adapter.adapt(null, mapping.getIdentifierType(findClass(className)));
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public String getIdentifierPropertyName(final String className) throws MappingException {
        try {
            return mapping.getIdentifierPropertyName(findClass(className));
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Type getReferencedPropertyType(final String className, final String propertyName) throws MappingException {
        try {
            return TypeV5Adapter.adapt(null, mapping.getPropertyType(findClass(className), propertyName));
        } catch (final net.sf.hibernate.HibernateException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }
}
