package com.atlassian.hibernate.adapter.adapters.type;

/**
 * An adapter bridging the LiteralType interface from hibernate v5 (onwards) to v2.
 */
public class LiteralTypeV2Adapter extends TypeV2Adapter implements net.sf.hibernate.type.LiteralType {
    private final org.hibernate.type.LiteralType type;

    LiteralTypeV2Adapter(final org.hibernate.type.LiteralType type) {
        super((org.hibernate.type.Type) type);
        this.type = type;
    }

    @SuppressWarnings("unchecked")
    @Override
    public String objectToSQLString(final Object value) throws Exception {
        return type.objectToSQLString(value, null);
    }
}
