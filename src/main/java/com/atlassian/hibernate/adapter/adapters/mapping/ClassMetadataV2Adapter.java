package com.atlassian.hibernate.adapter.adapters.mapping;

import com.atlassian.hibernate.adapter.adapters.PropertyValueAdapter;
import com.atlassian.hibernate.adapter.adapters.persister.AbstractEntityPersisterV2Adapter;
import com.atlassian.hibernate.adapter.adapters.persister.EntityPersisterV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV2Adapter;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.metadata.ClassMetadata;
import net.sf.hibernate.type.Type;

import javax.persistence.PersistenceException;
import java.io.Serializable;

/**
 * An adapter bridging the ClassMetadata interface for hibernate v5 (onwards) to v2.
 */
public class ClassMetadataV2Adapter implements ClassMetadata {
    private final org.hibernate.metadata.ClassMetadata metadata;
    private final org.hibernate.SessionFactory sessionFactory;

    protected ClassMetadataV2Adapter(
            final org.hibernate.metadata.ClassMetadata metadata,
            final org.hibernate.SessionFactory sessionFactory) {

        this.metadata = metadata;
        this.sessionFactory = sessionFactory;
    }

    public static ClassMetadata adapt(
            final org.hibernate.metadata.ClassMetadata metadata,
            final org.hibernate.SessionFactory sessionFactory,
            final org.hibernate.Session session) {

        if (metadata == null)
            return null;

        if (metadata instanceof org.hibernate.persister.entity.SingleTableEntityPersister)
            return EntityPersisterV2Adapter.adaptEntityPersister(
                    (org.hibernate.persister.entity.SingleTableEntityPersister) metadata, sessionFactory, session);
        if (metadata instanceof org.hibernate.persister.entity.AbstractEntityPersister)
            return AbstractEntityPersisterV2Adapter.adaptAbstractEntityPersister(
                    (org.hibernate.persister.entity.AbstractEntityPersister) metadata, sessionFactory, session);

        return new ClassMetadataV2Adapter(metadata, sessionFactory);
    }

    public static ClassMetadata adaptClassMetadataOnly(
            final org.hibernate.metadata.ClassMetadata metadata,
            final org.hibernate.SessionFactory sessionFactory) {

        if (metadata == null)
            return null;
        return new ClassMetadataV2Adapter(metadata, sessionFactory);
    }

    @Override
    public Class getMappedClass() {
        return metadata.getMappedClass();
    }

    @Override
    public Object instantiate(final Serializable id) throws HibernateException {
        return metadata.instantiate(id, getSessionImplementor());
    }

    @Override
    public String getIdentifierPropertyName() {
        return metadata.getIdentifierPropertyName();
    }

    @Override
    public String[] getPropertyNames() {
        return metadata.getPropertyNames();
    }

    @Override
    public Type getIdentifierType() {
        return TypeV2Adapter.adapt(metadata.getIdentifierType());
    }

    @Override
    public Type[] getPropertyTypes() {
        return TypeV2Adapter.adapt(metadata.getPropertyTypes());
    }

    @Override
    public Type getPropertyType(final String propertyName) throws HibernateException {
        return TypeV2Adapter.adapt(metadata.getPropertyType(propertyName));
    }

    @Override
    public Object getPropertyValue(final Object object, final String propertyName) throws HibernateException {
        return PropertyValueAdapter.adaptToV2(metadata.getPropertyValue(object, propertyName));
    }

    @Override
    public void setPropertyValue(final Object object, final String propertyName, final Object value) throws HibernateException {
        metadata.setPropertyValue(PropertyValueAdapter.adaptToV5(object), propertyName, value);
    }

    @Override
    public Object[] getPropertyValues(final Object entity) throws HibernateException {
        return PropertyValueAdapter.adaptToV2(metadata.getPropertyValues(entity));
    }

    @Override
    public void setPropertyValues(final Object object, final Object[] values) throws HibernateException {
        metadata.setPropertyValues(object, PropertyValueAdapter.adaptToV5(values));
    }

    @Override
    public Serializable getIdentifier(final Object entity) throws HibernateException {
        return metadata.getIdentifier(entity, getSessionImplementor());
    }

    @Override
    public void setIdentifier(final Object object, final Serializable id) throws HibernateException {
        metadata.setIdentifier(object, id, getSessionImplementor());
    }

    @Override
    public boolean implementsLifecycle() {
        return metadata.implementsLifecycle();
    }

    @Override
    public boolean implementsValidatable() {
        return false;
    }

    @Override
    public boolean hasProxy() {
        return metadata.hasProxy();
    }

    @Override
    public boolean isMutable() {
        return metadata.isMutable();
    }

    @Override
    public boolean isVersioned() {
        return metadata.isVersioned();
    }

    @Override
    public Object getVersion(final Object object) throws HibernateException {
        return metadata.getVersion(object);
    }

    @Override
    public int getVersionProperty() {
        return metadata.getVersionProperty();
    }

    @Override
    public boolean[] getPropertyNullability() {
        return metadata.getPropertyNullability();
    }

    @Override
    public boolean hasIdentifierProperty() {
        return metadata.hasIdentifierProperty();
    }

    private org.hibernate.engine.spi.SessionImplementor getSessionImplementor() {
        // make a best effort to get a session
        try {
            return (org.hibernate.engine.spi.SessionImplementor) sessionFactory.getCurrentSession();
        } catch (final PersistenceException ex) {
            // if we really needed one, an exception will be thrown by hibernate later
            return null;
        }
    }
}
