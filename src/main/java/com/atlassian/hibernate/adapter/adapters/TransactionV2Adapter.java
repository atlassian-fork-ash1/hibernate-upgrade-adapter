package com.atlassian.hibernate.adapter.adapters;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Transaction;

import javax.persistence.PersistenceException;

/**
 * An adapter bridging the Transaction interface for hibernate v5 (onwards) to v2.
 */
public class TransactionV2Adapter implements Transaction {
    private final org.hibernate.Transaction transaction;
    private boolean commitFailed;
    private boolean wasCommitted;
    private boolean wasRolledBack;

    protected TransactionV2Adapter(final org.hibernate.Transaction transaction) {
        this.transaction = transaction;
    }

    public static Transaction adapt(final org.hibernate.Transaction transaction) {
        if (transaction == null)
            return null;
        return new TransactionV2Adapter(transaction);
    }

    @Override
    public void commit() throws HibernateException {
        try {
            transaction.commit();
            wasCommitted = true;
        } catch (final PersistenceException ex) {
            commitFailed = true;
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void rollback() throws HibernateException {
        try {
            transaction.rollback();
            wasRolledBack = !commitFailed;
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean wasRolledBack() throws HibernateException {
        return wasRolledBack;
    }

    @Override
    public boolean wasCommitted() throws HibernateException {
        return wasCommitted;
    }
}
