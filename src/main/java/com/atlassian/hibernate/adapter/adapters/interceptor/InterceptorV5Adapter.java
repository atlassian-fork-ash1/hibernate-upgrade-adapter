package com.atlassian.hibernate.adapter.adapters.interceptor;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV2Adapter;
import org.hibernate.CallbackException;
import org.hibernate.EntityMode;
import org.hibernate.Interceptor;
import org.hibernate.Transaction;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.Iterator;

/**
 * An adapter bridging the Interceptor interface from hibernate v5 (onwards) to v2.
 */
public class InterceptorV5Adapter implements Interceptor {
    private final net.sf.hibernate.Interceptor interceptor;

    protected InterceptorV5Adapter(final net.sf.hibernate.Interceptor interceptor) {
        this.interceptor = interceptor;
    }

    public static Interceptor adapt(final net.sf.hibernate.Interceptor interceptor) {

        if (interceptor == null)
            return null;
        if (interceptor instanceof InterceptorV2Adapter) {
            return ((InterceptorV2Adapter) interceptor).getInterceptor();
        }
        return new InterceptorV5Adapter(interceptor);
    }

    net.sf.hibernate.Interceptor getInterceptor() {
        return interceptor;
    }

    @Override
    public boolean onLoad(
            final Object entity,
            final Serializable id,
            final Object[] state,
            final String[] propertyNames,
            final Type[] types) throws CallbackException {

        try {
            return interceptor.onLoad(entity, id, state, propertyNames, TypeV2Adapter.adapt(types));
        } catch (final net.sf.hibernate.CallbackException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean onFlushDirty(
            final Object entity,
            final Serializable id,
            final Object[] currentState,
            final Object[] previousState,
            final String[] propertyNames,
            final Type[] types) throws CallbackException {

        try {
            return interceptor.onFlushDirty(entity, id, currentState, previousState, propertyNames, TypeV2Adapter.adapt(types));
        } catch (final net.sf.hibernate.CallbackException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean onSave(
            final Object entity,
            final Serializable id,
            final Object[] state,
            final String[] propertyNames,
            final Type[] types) throws CallbackException {

        try {
            return interceptor.onSave(entity, id, state, propertyNames, TypeV2Adapter.adapt(types));
        } catch (final net.sf.hibernate.CallbackException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void onDelete(
            final Object entity,
            final Serializable id,
            final Object[] state,
            final String[] propertyNames,
            final Type[] types) throws CallbackException {

        try {
            interceptor.onDelete(entity, id, state, propertyNames, TypeV2Adapter.adapt(types));
        } catch (final net.sf.hibernate.CallbackException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void onCollectionRecreate(final Object collection, final Serializable key) throws CallbackException {
    }

    @Override
    public void onCollectionRemove(final Object collection, final Serializable key) throws CallbackException {
    }

    @Override
    public void onCollectionUpdate(final Object collection, final Serializable key) throws CallbackException {
    }

    @Override
    public void preFlush(final Iterator entities) throws CallbackException {
        try {
            interceptor.preFlush(entities);
        } catch (final net.sf.hibernate.CallbackException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void postFlush(final Iterator entities) throws CallbackException {
        try {
            interceptor.postFlush(entities);
        } catch (final net.sf.hibernate.CallbackException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Boolean isTransient(final Object entity) {
        return null;
    }

    @Override
    public int[] findDirty(
            final Object entity,
            final Serializable id,
            final Object[] currentState,
            final Object[] previousState,
            final String[] propertyNames,
            final Type[] types) {

        return interceptor.findDirty(entity, id, currentState, previousState, propertyNames, TypeV2Adapter.adapt(types));
    }

    @Override
    public Object instantiate(final String entityName, final EntityMode entityMode, final Serializable id) throws CallbackException {
        return null;
    }

    @Override
    public String getEntityName(final Object object) throws CallbackException {
        return null;
    }

    @Override
    public Object getEntity(final String entityName, final Serializable id) throws CallbackException {
        return null;
    }

    @Override
    public void afterTransactionBegin(final Transaction tx) {
    }

    @Override
    public void beforeTransactionCompletion(final Transaction tx) {
    }

    @Override
    public void afterTransactionCompletion(final Transaction tx) {
    }

    @Override
    @Deprecated
    public String onPrepareStatement(final String sql) {
        return null;
    }
}
