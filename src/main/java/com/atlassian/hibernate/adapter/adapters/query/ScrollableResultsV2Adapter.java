package com.atlassian.hibernate.adapter.adapters.query;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV2Adapter;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.type.Type;

import javax.persistence.PersistenceException;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.Clob;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * An adapter bridging the ScrollableResults interface for hibernate v5 (onwards) to v2.
 */
public class ScrollableResultsV2Adapter implements net.sf.hibernate.ScrollableResults {
    private final org.hibernate.ScrollableResults results;

    protected ScrollableResultsV2Adapter(final org.hibernate.ScrollableResults results) {
        this.results = results;
    }

    public static net.sf.hibernate.ScrollableResults adapt(final org.hibernate.ScrollableResults results) {
        if (results == null)
            return null;
        return new ScrollableResultsV2Adapter(results);
    }

    @Override
    public boolean next() throws HibernateException {
        try {
            return results.next();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean previous() throws HibernateException {
        try {
            return results.previous();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean scroll(final int i) throws HibernateException {
        try {
            return results.scroll(i);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean last() throws HibernateException {
        try {
            return results.last();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean first() throws HibernateException {
        try {
            return results.first();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void beforeFirst() throws HibernateException {
        try {
            results.beforeFirst();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void afterLast() throws HibernateException {
        try {
            results.afterLast();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean isFirst() throws HibernateException {
        try {
            return results.isFirst();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean isLast() throws HibernateException {
        try {
            return results.isLast();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public void close() throws HibernateException {
        try {
            results.close();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Object[] get() throws HibernateException {
        try {
            return results.get();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Object get(final int i) throws HibernateException {
        try {
            return results.get(i);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Type getType(final int i) {
        return TypeV2Adapter.adapt(results.getType(i));
    }

    @Override
    public Integer getInteger(final int col) throws HibernateException {
        try {
            return results.getInteger(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Long getLong(final int col) throws HibernateException {
        try {
            return results.getLong(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Float getFloat(final int col) throws HibernateException {
        try {
            return results.getFloat(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Boolean getBoolean(final int col) throws HibernateException {
        try {
            return results.getBoolean(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Double getDouble(final int col) throws HibernateException {
        try {
            return results.getDouble(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Short getShort(final int col) throws HibernateException {
        try {
            return results.getShort(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Byte getByte(final int col) throws HibernateException {
        try {
            return results.getByte(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Character getCharacter(final int col) throws HibernateException {
        try {
            return results.getCharacter(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public byte[] getBinary(final int col) throws HibernateException {
        try {
            return results.getBinary(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public String getText(final int col) throws HibernateException {
        try {
            return results.getText(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Blob getBlob(final int col) throws HibernateException {
        try {
            return results.getBlob(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Clob getClob(final int col) throws HibernateException {
        try {
            return results.getClob(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public String getString(final int col) throws HibernateException {
        try {
            return results.getString(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public BigDecimal getBigDecimal(final int col) throws HibernateException {
        try {
            return results.getBigDecimal(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Date getDate(final int col) throws HibernateException {
        try {
            return results.getDate(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Locale getLocale(final int col) throws HibernateException {
        try {
            return results.getLocale(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Calendar getCalendar(final int col) throws HibernateException {
        try {
            return results.getCalendar(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public TimeZone getTimeZone(final int col) throws HibernateException {
        try {
            return results.getTimeZone(col);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public int getRowNumber() throws HibernateException {
        try {
            return results.getRowNumber();
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean setRowNumber(final int rowNumber) throws HibernateException {
        try {
            return results.setRowNumber(rowNumber);
        } catch (final PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }
}
