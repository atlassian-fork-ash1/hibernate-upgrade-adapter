package com.atlassian.hibernate.adapter.adapters.cache;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.cache.CacheException;
import org.hibernate.cache.spi.Region;

import java.util.Map;

/**
 * An adapter bridging the Region interface for hibernate v2 to v5 (onwards).
 */
public abstract class RegionV5Adapter implements Region {
    private final net.sf.hibernate.cache.Cache underlyingCache;
    private final String regionName;

    protected RegionV5Adapter(
            final String regionName,
            final net.sf.hibernate.cache.Cache underlyingCache) {

        this.regionName = regionName;
        this.underlyingCache = underlyingCache;
    }

    protected net.sf.hibernate.cache.Cache getUnderlyingCache() {
        return underlyingCache;
    }

    @Override
    public String getName() {
        return regionName;
    }

    @Override
    public void destroy() throws CacheException {
        try {
            underlyingCache.destroy();
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public boolean contains(final Object key) {
        try {
            return underlyingCache.get(key) != null;
        } catch (final net.sf.hibernate.cache.CacheException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public long nextTimestamp() {
        return underlyingCache.nextTimestamp();
    }

    @Override
    public int getTimeout() {
        return underlyingCache.getTimeout();
    }

    @Override
    public long getSizeInMemory() {
        throw new NotImplementedException("getSizeInMemory() not implemented");
    }

    @Override
    public long getElementCountInMemory() {
        throw new NotImplementedException("getElementCountInMemory not implemented");
    }

    @Override
    public long getElementCountOnDisk() {
        throw new NotImplementedException("getElementCountOnDisk not implemented");
    }

    @Override
    public Map toMap() {
        throw new NotImplementedException("toMap not implemented");
    }
}
