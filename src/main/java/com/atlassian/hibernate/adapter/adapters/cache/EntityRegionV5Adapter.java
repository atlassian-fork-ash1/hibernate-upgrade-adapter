package com.atlassian.hibernate.adapter.adapters.cache;

import org.hibernate.cache.CacheException;
import org.hibernate.cache.spi.CacheDataDescription;
import org.hibernate.cache.spi.EntityRegion;
import org.hibernate.cache.spi.access.AccessType;
import org.hibernate.cache.spi.access.EntityRegionAccessStrategy;

import java.util.function.Function;

/**
 * An adapter bridging the EntityRegion interface for hibernate v2 to v5 (onwards).
 */
public class EntityRegionV5Adapter extends TransactionDataRegionV5Adapter implements EntityRegion {

    protected EntityRegionV5Adapter(
            final String regionName,
            final net.sf.hibernate.cache.Cache underlyingCache,
            final Function<String, String> cacheStrategyClassSupplier,
            final CacheDataDescription metadata) {

        super(regionName, underlyingCache, metadata, cacheStrategyClassSupplier);
    }

    public static EntityRegion adapt(
            final String regionName,
            final net.sf.hibernate.cache.Cache underlyingCache,
            final Function<String, String> cacheStrategyClassSupplier,
            final CacheDataDescription metadata) {

        return new EntityRegionV5Adapter(regionName, underlyingCache, cacheStrategyClassSupplier, metadata);
    }

    @Override
    public EntityRegionAccessStrategy buildAccessStrategy(final AccessType accessType) throws CacheException {
        return EntityRegionAccessStrategyV5Adapter.adapt(this, buildCacheConcurrencyStategy(accessType));
    }
}
