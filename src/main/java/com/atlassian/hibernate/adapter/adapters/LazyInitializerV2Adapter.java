package com.atlassian.hibernate.adapter.adapters;

import net.sf.cglib.proxy.Callback;
import net.sf.hibernate.proxy.LazyInitializer;
import org.apache.commons.lang3.NotImplementedException;

import javax.persistence.PersistenceException;
import java.io.Serializable;

/**
 * An adapter bridging the ConnectionProvider interface for hibernate v5 (onwards) to v2.
 */
public class LazyInitializerV2Adapter extends LazyInitializer implements Callback {
    private final org.hibernate.proxy.LazyInitializer lazyInitializer;

    protected LazyInitializerV2Adapter(
            final org.hibernate.proxy.LazyInitializer lazyInitializer,
            final Class persistentClass,
            final Serializable id) {

        super(persistentClass, id, null, null, null);
        this.lazyInitializer = lazyInitializer;
    }

    public static LazyInitializer adapt(
            final org.hibernate.proxy.LazyInitializer lazyInitializer,
            final Class persistentClass,
            final Serializable id) {

        // null lazyInitializer is allowed here
        return new LazyInitializerV2Adapter(lazyInitializer, persistentClass, id);
    }

    @Override
    public void initialize() throws net.sf.hibernate.HibernateException {
        try {
            target = lazyInitializer.getImplementation();
        } catch (PersistenceException ex) {
            throw HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    protected Object serializableProxy() {
        throw new NotImplementedException("serializableProxy not implemented");
    }
}
