package com.atlassian.hibernate.adapter.adapters.query;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Query;

public interface QueryV2Supplier {
    Query get() throws HibernateException;
}
