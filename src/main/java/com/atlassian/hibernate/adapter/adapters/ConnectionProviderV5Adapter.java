package com.atlassian.hibernate.adapter.adapters;

import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * An adapter bridging the ConnectionProvider interface for hibernate v2 to v5 (onwards).
 */
public class ConnectionProviderV5Adapter implements ConnectionProvider {

    private final net.sf.hibernate.connection.ConnectionProvider connectionProvider;

    protected ConnectionProviderV5Adapter(final net.sf.hibernate.connection.ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    public static ConnectionProvider adapt(final net.sf.hibernate.connection.ConnectionProvider connectionProvider) {
        if (connectionProvider == null) {
            return null;
        }
        if (connectionProvider instanceof ConnectionProviderV2Adapter) {
            return ((ConnectionProviderV2Adapter) connectionProvider).getV5ConnectionProvider();
        }
        return new ConnectionProviderV5Adapter(connectionProvider);
    }

    public net.sf.hibernate.connection.ConnectionProvider getV2ConnectionProvider() {
        return connectionProvider;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return connectionProvider.getConnection();
    }

    @Override
    public void closeConnection(final Connection conn) throws SQLException {
        connectionProvider.closeConnection(conn);
    }

    @Override
    public boolean supportsAggressiveRelease() {
        return false;
    }

    @Override
    public boolean isUnwrappableAs(final Class unwrapType) {
        return false;
    }

    @Override
    public <T> T unwrap(final Class<T> unwrapType) {
        return null;
    }
}
