package com.atlassian.hibernate.adapter.adapters.cache;

import org.hibernate.cache.spi.TimestampsRegion;

/**
 * An adapter bridging the TimestampsRegion interface for hibernate v2 to v5 (onwards).
 */
public class TimestampsRegionV5Adapter extends GeneralDataRegionV5Adapter implements TimestampsRegion {

    protected TimestampsRegionV5Adapter(
            final String regionName,
            final net.sf.hibernate.cache.Cache underlyingCache) {

        super(regionName, underlyingCache);
    }

    public static TimestampsRegion adapt(
            final String regionName,
            final net.sf.hibernate.cache.Cache underlyingCache) {

        if (underlyingCache == null) {
            return null;
        }
        return new TimestampsRegionV5Adapter(regionName, underlyingCache);
    }
}
