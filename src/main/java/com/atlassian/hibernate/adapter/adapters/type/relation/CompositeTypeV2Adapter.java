package com.atlassian.hibernate.adapter.adapters.type.relation;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.adapters.PropertyValueAdapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV2Adapter;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.engine.Cascades;
import net.sf.hibernate.engine.SessionImplementor;
import net.sf.hibernate.type.AbstractComponentType;
import net.sf.hibernate.type.Type;
import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.EntityMode;

/**
 * An adapter bridging the CompositeType interface from hibernate v5 (onwards) to v2.
 */
public class CompositeTypeV2Adapter extends TypeV2Adapter implements AbstractComponentType {
    private final org.hibernate.type.CompositeType type;

    protected CompositeTypeV2Adapter(final org.hibernate.type.CompositeType type) {
        super(type);
        this.type = type;
    }

    public static AbstractComponentType adapt(final org.hibernate.type.CompositeType type) {
        if (type == null)
            return null;
        return new CompositeTypeV2Adapter(type);
    }

    @Override
    public Type[] getSubtypes() {
        return TypeV2Adapter.adapt(type.getSubtypes());
    }

    @Override
    public String[] getPropertyNames() {
        return type.getPropertyNames();
    }

    @Override
    public Object[] getPropertyValues(final Object component, final SessionImplementor session) throws HibernateException {
        final org.hibernate.Session sessionV5 =
                session != null
                        ? HibernateBridge.get(session.getSessionFactory()).getV5Session(session)
                        : null;
        final Object[] result = type.getPropertyValues(component, (org.hibernate.engine.spi.SessionImplementor) sessionV5);
        return PropertyValueAdapter.adaptToV2(result);
    }

    @Override
    public Object[] getPropertyValues(final Object component) throws HibernateException {
        final Object[] result = type.getPropertyValues(component, EntityMode.POJO);
        return PropertyValueAdapter.adaptToV2(result);
    }

    @Override
    public void setPropertyValues(final Object component, final Object[] values) throws HibernateException {
        type.setPropertyValues(component, PropertyValueAdapter.adaptToV5(values), EntityMode.POJO);
    }

    @Override
    public Object getPropertyValue(final Object component, final int i, final SessionImplementor session) throws HibernateException {
        final org.hibernate.Session sessionV5 =
                session != null
                ? HibernateBridge.get(session.getSessionFactory()).getV5Session(session)
                : null;
        final Object result = type.getPropertyValue(component, i, (org.hibernate.engine.spi.SessionImplementor) sessionV5);
        return PropertyValueAdapter.adaptToV2(result);
    }

    @Override
    public Cascades.CascadeStyle cascade(final int i) {
        throw new NotImplementedException("cascade not implemented");
    }

    @Override
    public int enableJoinedFetch(final int i) {
        throw new NotImplementedException("enableJoinedFetch not implemented");
    }
}
