package com.atlassian.hibernate.adapter.adapters.mapping;

import com.atlassian.hibernate.adapter.adapters.persister.AbstractCollectionPersisterV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV2Adapter;
import net.sf.hibernate.metadata.CollectionMetadata;
import net.sf.hibernate.type.Type;

/**
 * An adapter bridging the ClassMetadata interface for hibernate v5 (onwards) to v2.
 */
public class CollectionMetadataV2Adapter implements CollectionMetadata {
    private final org.hibernate.metadata.CollectionMetadata metadata;

    protected CollectionMetadataV2Adapter(final org.hibernate.metadata.CollectionMetadata metadata) {
        this.metadata = metadata;
    }

    public static CollectionMetadata adapt(
            final org.hibernate.metadata.CollectionMetadata metadata,
            final org.hibernate.SessionFactory sessionFactory) {

        if (metadata == null)
            return null;

        if (metadata instanceof org.hibernate.persister.collection.AbstractCollectionPersister)
            return AbstractCollectionPersisterV2Adapter.adaptCollectionPersister(
                    (org.hibernate.persister.collection.AbstractCollectionPersister) metadata,
                    sessionFactory);

        return new CollectionMetadataV2Adapter(metadata);
    }

    @Override
    public Type getKeyType() {
        return TypeV2Adapter.adapt(metadata.getKeyType());
    }

    @Override
    public Type getElementType() {
        return TypeV2Adapter.adapt(metadata.getElementType());
    }

    @Override
    public Type getIndexType() {
        return TypeV2Adapter.adapt(metadata.getIndexType());
    }

    @Override
    public boolean hasIndex() {
        return metadata.hasIndex();
    }

    @Override
    public String getRole() {
        return metadata.getRole();
    }

    @Override
    public boolean isArray() {
        return metadata.isArray();
    }

    @Override
    public boolean isPrimitiveArray() {
        return metadata.isPrimitiveArray();
    }

    @Override
    public boolean isLazy() {
        return metadata.isLazy();
    }
}
