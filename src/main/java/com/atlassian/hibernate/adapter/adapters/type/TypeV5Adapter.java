package com.atlassian.hibernate.adapter.adapters.type;

import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.reflection.CustomTypeV2Reflection;
import com.atlassian.hibernate.adapter.type.PersistentEnumUserType;
import com.atlassian.hibernate.adapter.type.V5TypeSupplier;
import com.atlassian.hibernate.adapter.util.ArrayUtil;
import com.atlassian.hibernate.util.ThrowableUtil;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.type.Type;
import org.hibernate.type.TypeFactory;

/**
 * An adapter bridging the Type interface from hibernate v5 (onwards) to v2.
 */
public final class TypeV5Adapter {
    private TypeV5Adapter() {
    }

    public static Type[] adapt(final SessionFactory sessionFactory, final net.sf.hibernate.type.Type[] types) {
        if (types == null)
            return null;
        return ArrayUtil.transformArray(
                types,
                new org.hibernate.type.Type[types.length],
                type -> TypeV5Adapter.adapt(sessionFactory, type));
    }

    @SuppressWarnings("deprecation")
    public static Type adapt(final SessionFactory sessionFactory, final net.sf.hibernate.type.Type type) {
        if (type == null)
            return null;
        if (type instanceof V5TypeSupplier)
            return ((V5TypeSupplier) type).getV5Type();

        if (type instanceof net.sf.hibernate.type.StringType)
            return org.hibernate.type.StringType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.TextType)
            return org.hibernate.type.TextType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.IntegerType)
            return org.hibernate.type.IntegerType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.LongType)
            return org.hibernate.type.LongType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.BigDecimalType)
            return org.hibernate.type.BigDecimalType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.BinaryType)
            return org.hibernate.type.BinaryType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.BlobType)
            return org.hibernate.type.BlobType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.ByteType)
            return org.hibernate.type.ByteType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.CalendarDateType)
            return org.hibernate.type.CalendarDateType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.CalendarType)
            return org.hibernate.type.CalendarType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.CharacterType)
            return org.hibernate.type.CharacterType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.ClassType)
            return org.hibernate.type.ClassType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.ClobType)
            return org.hibernate.type.ClobType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.CurrencyType)
            return org.hibernate.type.CurrencyType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.DateType)
            return org.hibernate.type.DateType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.DoubleType)
            return org.hibernate.type.DoubleType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.FloatType)
            return org.hibernate.type.FloatType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.LocaleType)
            return org.hibernate.type.LocaleType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.ObjectType)
            return org.hibernate.type.ObjectType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.SerializableType)
            return org.hibernate.type.SerializableType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.ShortType)
            return org.hibernate.type.ShortType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.TimeType)
            return org.hibernate.type.TimeType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.TimeZoneType)
            return org.hibernate.type.TimeZoneType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.TimestampType)
            return org.hibernate.type.TimestampType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.TrueFalseType)
            return org.hibernate.type.TrueFalseType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.YesNoType)
            return org.hibernate.type.YesNoType.INSTANCE;
        if (type instanceof net.sf.hibernate.type.BooleanType)
            return org.hibernate.type.BooleanType.INSTANCE;

        if (type instanceof net.sf.hibernate.type.PersistentEnumType)
            return new org.hibernate.type.CustomType(new PersistentEnumUserType(type.getReturnedClass()));
        if (type instanceof net.sf.hibernate.type.ManyToOneType)
            return adaptManyToOneType(sessionFactory, (net.sf.hibernate.type.ManyToOneType) type);
        if (type instanceof net.sf.hibernate.type.CustomType)
            return adaptCustomType((net.sf.hibernate.type.CustomType) type);

        throw new IllegalArgumentException("Unsupported net.sf.hibernate.type.Type: " + type.getClass().getName());
    }

    private static org.hibernate.type.Type adaptManyToOneType(
            final SessionFactory sessionFactory,
            final net.sf.hibernate.type.ManyToOneType type) {
        if (sessionFactory == null) {
            throw new IllegalArgumentException("ManyToOneType can't be adapted from v2 to v5 (onwards) without a SessionFactory");
        }

        final SessionFactoryImplementor sessionFactoryImpl = (SessionFactoryImplementor) sessionFactory;
        final TypeFactory typeFactory = sessionFactoryImpl.getTypeResolver().getTypeFactory();

        if (type.isUniqueKeyReference()) {
            try {
                return typeFactory.manyToOne(
                        type.getAssociatedClass().getName(),
                        true,
                        type.getIdentifierOrUniqueKeyPropertyName(null),
                        true, true, false, false);
            } catch (final net.sf.hibernate.MappingException ex) {
                // this should never happen
                throw ThrowableUtil.propagateAll(HibernateExceptionAdapter.adapt(ex));
            }
        }
        return typeFactory.manyToOne(type.getAssociatedClass().getName());
    }

    private static org.hibernate.type.Type adaptCustomType(final net.sf.hibernate.type.CustomType type) {
        return new org.hibernate.type.CustomType(
                UserTypeV5Adapter.adapt(CustomTypeV2Reflection.getUserType(type)));
    }
}
