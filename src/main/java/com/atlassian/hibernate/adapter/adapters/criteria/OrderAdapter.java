package com.atlassian.hibernate.adapter.adapters.criteria;

import com.atlassian.hibernate.adapter.reflection.OrderV2Reflection;

/**
 * An adapter bridging the Order class for hibernate
 * between v2 / v5 (onwards).
 */
public final class OrderAdapter {

    private OrderAdapter() { }

    public static net.sf.hibernate.expression.Order adapt(
            final org.hibernate.criterion.Order order) {

        if (order == null) {
            return null;
        }
        return new net.sf.hibernate.expression.Order(
                order.getPropertyName(), order.isAscending()) { };
    }

    public static org.hibernate.criterion.Order adapt(
            final net.sf.hibernate.expression.Order order) {

        if (order == null) {
            return null;
        }
        return new org.hibernate.criterion.Order(
                OrderV2Reflection.getPropertyName(order),
                OrderV2Reflection.getAscending(order)) { };
    }
}
