package com.atlassian.hibernate.adapter.adapters.persister;

import com.atlassian.hibernate.adapter.HibernateBridge;
import com.atlassian.hibernate.adapter.adapters.HibernateExceptionAdapter;
import com.atlassian.hibernate.adapter.adapters.type.JoinableV2Adapter;
import com.atlassian.hibernate.adapter.adapters.type.TypeV2Adapter;
import com.atlassian.hibernate.util.ThrowableUtil;
import net.sf.hibernate.HibernateException;
import net.sf.hibernate.MappingException;
import net.sf.hibernate.QueryException;
import net.sf.hibernate.cache.CacheConcurrencyStrategy;
import net.sf.hibernate.cache.CacheException;
import net.sf.hibernate.collection.AbstractCollectionPersister;
import net.sf.hibernate.collection.CollectionPersister;
import net.sf.hibernate.collection.PersistentCollection;
import net.sf.hibernate.engine.SessionFactoryImplementor;
import net.sf.hibernate.engine.SessionImplementor;
import net.sf.hibernate.id.IdentifierGenerator;
import net.sf.hibernate.loader.CollectionInitializer;
import net.sf.hibernate.mapping.Bag;
import net.sf.hibernate.mapping.Collection;
import net.sf.hibernate.mapping.RootClass;
import net.sf.hibernate.mapping.SimpleValue;
import net.sf.hibernate.mapping.Table;
import net.sf.hibernate.metadata.CollectionMetadata;
import net.sf.hibernate.persister.ClassPersister;
import net.sf.hibernate.persister.Joinable;
import net.sf.hibernate.type.PersistentCollectionType;
import net.sf.hibernate.type.StringType;
import net.sf.hibernate.type.Type;
import org.apache.commons.lang3.NotImplementedException;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * An adapter bridging the AbstractCollectionPersister class for hibernate v5 (onwards) to v2.
 */
public class AbstractCollectionPersisterV2Adapter extends AbstractCollectionPersister {
    private final org.hibernate.persister.collection.AbstractCollectionPersister persister;
    private final org.hibernate.SessionFactory sessionFactory;

    private final Joinable joinableImpl;
    private final CollectionPersister collectionPersisterImpl;

    protected AbstractCollectionPersisterV2Adapter(
            final org.hibernate.persister.collection.AbstractCollectionPersister persister,
            final org.hibernate.SessionFactory sessionFactory) throws MappingException, CacheException {

        // all public methods are overridden, so running the super constructor
        // really happens only because it can't be avoided
        super(createDummyCollection(), null, getV2orV5SessionFactory(sessionFactory));

        this.sessionFactory = sessionFactory;
        this.persister = persister;
        this.joinableImpl = JoinableV2Adapter.adaptJoinableOnly(persister);
        this.collectionPersisterImpl = CollectionPersisterV2Adapter.adaptCollectionPersisterOnly(persister, sessionFactory);
    }

    public static AbstractCollectionPersister adaptCollectionPersister(
            final org.hibernate.persister.collection.AbstractCollectionPersister persister,
            final org.hibernate.SessionFactory sessionFactory) {

        if (persister == null)
            return null;

        try {
            return new AbstractCollectionPersisterV2Adapter(persister, sessionFactory);
        } catch (final HibernateException ex) {
            throw ThrowableUtil.propagateAll(ex);
        }
    }

    private static net.sf.hibernate.engine.SessionFactoryImplementor getV2orV5SessionFactory(
            final org.hibernate.SessionFactory sessionFactory) {

        return (net.sf.hibernate.engine.SessionFactoryImplementor)
                HibernateBridge.get(sessionFactory).getV2orV5SessionFactory();
    }

    /**
     * Create a dummy collection that will just be used by the constructor,
     * with just enough info so that it doesn't throw an exception.
     */
    private static Collection createDummyCollection() {

        final Bag result = new Bag(new RootClass()) {
            @Override
            public PersistentCollectionType getCollectionType() {
                return null;
            }
        };

        final SimpleValue element = new SimpleValue();
        element.setType(new StringType());

        result.setKey(new SimpleValue());
        result.setCollectionTable(new Table());
        result.setElement(element);
        return result;
    }

    private boolean inSuperConstructor() {
        return persister == null;
    }

    //---------- AbstractCollectionPersister Overrides ----------//

    @Override
    protected CollectionInitializer createCollectionInitializer(final SessionFactoryImplementor factory)
            throws MappingException {

        if (inSuperConstructor())
            return null;
        throw new NotImplementedException("createCollectionInitializer not implemented");
    }

    @Override
    protected String generateDeleteString() {
        if (inSuperConstructor())
            return null;
        throw new NotImplementedException("generateDeleteString not implemented");
    }

    @Override
    protected String generateDeleteRowString() {
        if (inSuperConstructor())
            return null;
        throw new NotImplementedException("generateDeleteRowString not implemented");
    }

    @Override
    protected String generateUpdateRowString() {
        if (inSuperConstructor())
            return null;
        throw new NotImplementedException("generateUpdateRowString not implemented");
    }

    @Override
    protected String generateInsertRowString() {
        if (inSuperConstructor())
            return null;
        throw new NotImplementedException("generateInsertRowString not implemented");
    }

    @Override
    protected int doUpdateRows(final Serializable key, final PersistentCollection collection, final SessionImplementor session) throws HibernateException {
        throw new NotImplementedException("doUpdateRows not implemented");
    }

    //---------- PropertyMapping Overrides ----------//

    @Override
    public Type toType(final String propertyName) throws QueryException {
        try {
            return TypeV2Adapter.adapt(persister.toType(propertyName));
        } catch (final org.hibernate.QueryException ex) {
            throw (QueryException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public String[] toColumns(final String alias, final String propertyName) throws QueryException {
        try {
            return persister.toColumns(alias, propertyName);
        } catch (final org.hibernate.QueryException ex) {
            throw (QueryException) HibernateExceptionAdapter.adapt(ex);
        }
    }

    @Override
    public Type getType() {
        return TypeV2Adapter.adapt(persister.getType());
    }

    //---------- Joinable Overrides ----------//

    @Override
    public String getName() {
        return joinableImpl.getName();
    }

    @Override
    public String getTableName() {
        return joinableImpl.getTableName();
    }

    @Override
    public String selectFragment(final String alias, final String suffix, final boolean includeCollectionColumns) {
        return joinableImpl.selectFragment(alias, suffix, includeCollectionColumns);
    }

    @Override
    public String whereJoinFragment(final String alias, final boolean innerJoin, final boolean includeSubclasses) {
        return joinableImpl.whereJoinFragment(alias, innerJoin, includeSubclasses);
    }

    @Override
    public String fromJoinFragment(final String alias, final boolean innerJoin, final boolean includeSubclasses) {
        return joinableImpl.fromJoinFragment(alias, innerJoin, includeSubclasses);
    }

    @Override
    public String[] getJoinKeyColumnNames() {
        return joinableImpl.getJoinKeyColumnNames();
    }

    @Override
    public boolean isCollection() {
        return joinableImpl.isCollection();
    }

    @Override
    public boolean isManyToMany() {
        return joinableImpl.isManyToMany();
    }

    @Override
    public boolean consumesAlias() {
        return joinableImpl.consumesAlias();
    }

    //---------- CollectionPersister Overrides ----------//

    @Override
    public void initialize(final Serializable key, final SessionImplementor session) throws HibernateException {
        collectionPersisterImpl.initialize(key, session);
    }

    @Override
    public CacheConcurrencyStrategy getCache() {
        return collectionPersisterImpl.getCache();
    }

    @Override
    public boolean hasCache() {
        return collectionPersisterImpl.hasCache();
    }

    @Override
    public PersistentCollectionType getCollectionType() {
        return collectionPersisterImpl.getCollectionType();
    }

    @Override
    public Type getKeyType() {
        return collectionPersisterImpl.getKeyType();
    }

    @Override
    public Type getIndexType() {
        return collectionPersisterImpl.getIndexType();
    }

    @Override
    public Type getElementType() {
        return collectionPersisterImpl.getElementType();
    }

    @Override
    public Class getElementClass() {
        return collectionPersisterImpl.getElementClass();
    }

    @Override
    public Object readKey(final ResultSet rs, final SessionImplementor session) throws HibernateException, SQLException {
        return collectionPersisterImpl.readKey(rs, session);
    }

    @Override
    public Object readElement(final ResultSet rs, final Object owner, final SessionImplementor session) throws HibernateException, SQLException {
        return collectionPersisterImpl.readElement(rs, owner, session);
    }

    @Override
    public Object readIndex(final ResultSet rs, final SessionImplementor session) throws HibernateException, SQLException {
        return collectionPersisterImpl.readIndex(rs, session);
    }

    @Override
    public Object readIdentifier(final ResultSet rs, final SessionImplementor session) throws HibernateException, SQLException {
        return collectionPersisterImpl.readIdentifier(rs, session);
    }

    @Override
    public void writeKey(final PreparedStatement st, final Serializable key, final boolean writeOrder, final SessionImplementor session) throws HibernateException, SQLException {
        collectionPersisterImpl.writeKey(st, key, writeOrder, session);
    }

    @Override
    public void writeElement(final PreparedStatement st, final Object elt, final boolean writeOrder, final SessionImplementor session) throws HibernateException, SQLException {
        collectionPersisterImpl.writeElement(st, elt, writeOrder, session);
    }

    @Override
    public void writeIndex(final PreparedStatement st, final Object idx, final boolean writeOrder, final SessionImplementor session) throws HibernateException, SQLException {
        collectionPersisterImpl.writeIndex(st, idx, writeOrder, session);
    }

    @Override
    public void writeIdentifier(final PreparedStatement st, final Object idx, final boolean writeOrder, final SessionImplementor session) throws HibernateException, SQLException {
        collectionPersisterImpl.writeIdentifier(st, idx, writeOrder, session);
    }

    @Override
    public boolean isPrimitiveArray() {
        return collectionPersisterImpl.isPrimitiveArray();
    }

    @Override
    public boolean isArray() {
        return collectionPersisterImpl.isArray();
    }

    @Override
    public boolean isOneToMany() {
        return collectionPersisterImpl.isOneToMany();
    }

    @Override
    public boolean hasIndex() {
        return collectionPersisterImpl.hasIndex();
    }

    @Override
    public boolean isLazy() {
        return collectionPersisterImpl.isLazy();
    }

    @Override
    public boolean isInverse() {
        return collectionPersisterImpl.isInverse();
    }

    @Override
    public void remove(final Serializable id, final SessionImplementor session) throws HibernateException {
        collectionPersisterImpl.remove(id, session);
    }

    @Override
    public void recreate(final PersistentCollection collection, final Serializable key, final SessionImplementor session) throws HibernateException {
        collectionPersisterImpl.recreate(collection, key, session);
    }

    @Override
    public void deleteRows(final PersistentCollection collection, final Serializable key, final SessionImplementor session) throws HibernateException {
        collectionPersisterImpl.deleteRows(collection, key, session);
    }

    @Override
    public void updateRows(final PersistentCollection collection, final Serializable key, final SessionImplementor session) throws HibernateException {
        collectionPersisterImpl.updateRows(collection, key, session);
    }

    @Override
    public void insertRows(final PersistentCollection collection, final Serializable key, final SessionImplementor session) throws HibernateException {
        collectionPersisterImpl.insertRows(collection, key, session);
    }

    @Override
    public String getRole() {
        return collectionPersisterImpl.getRole();
    }

    @Override
    public Class getOwnerClass() {
        return collectionPersisterImpl.getOwnerClass();
    }

    @Override
    public IdentifierGenerator getIdentifierGenerator() {
        return collectionPersisterImpl.getIdentifierGenerator();
    }

    @Override
    public Type getIdentifierType() {
        return collectionPersisterImpl.getIdentifierType();
    }

    @Override
    public boolean hasOrphanDelete() {
        return collectionPersisterImpl.hasOrphanDelete();
    }

    @Override
    public boolean hasOrdering() {
        return collectionPersisterImpl.hasOrdering();
    }

    @Override
    public Serializable getCollectionSpace() {
        return collectionPersisterImpl.getCollectionSpace();
    }

    @Override
    public CollectionMetadata getCollectionMetadata() {
        return collectionPersisterImpl.getCollectionMetadata();
    }

    //---------- QueryableCollection Overrides ----------//

    @Override
    public String selectFragment(final String alias) {
        throw new NotImplementedException("selectFragment(String) not implemented");
    }

    @Override
    public String[] getIndexColumnNames() {
        return persister.getIndexColumnNames();
    }

    @Override
    public String[] getElementColumnNames() {
        return persister.getElementColumnNames();
    }

    @Override
    public String[] getKeyColumnNames() {
        return persister.getKeyColumnNames();
    }

    @Override
    public String getSQLWhereString(final String alias) {
        throw new NotImplementedException("getSQLWhereString not implemented");
    }

    @Override
    public String getSQLOrderByString(final String alias) {
        return persister.getSQLOrderByString(alias);
    }

    @Override
    public boolean hasWhere() {
        return persister.hasWhere();
    }

    @Override
    public ClassPersister getElementPersister() {
        return ClassPersisterV2Adapter.adapt(persister.getElementPersister(), sessionFactory, null);
    }

    @Override
    public int enableJoinedFetch() {
        throw new NotImplementedException("enableJoinedFetch not implemented");
    }
}
