package com.atlassian.hibernate.adapter.adapters.type.collection;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.collection.CollectionPersister;
import net.sf.hibernate.collection.PersistentCollection;
import net.sf.hibernate.engine.CollectionSnapshot;
import net.sf.hibernate.type.Type;
import org.apache.commons.lang3.NotImplementedException;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * An adapter bridging the PersistentCollection interface from hibernate v5 (onwards) to v2.
 */
public class PersistentCollectionV2Adapter extends PersistentCollection {
    private final org.hibernate.collection.spi.PersistentCollection collection;

    public PersistentCollectionV2Adapter(final org.hibernate.collection.spi.PersistentCollection collection) {
        this.collection = collection;
    }

    public static PersistentCollection adapt(final org.hibernate.collection.spi.PersistentCollection collection) {
        if (collection == null)
            return null;
        if (collection instanceof Set)
            return new PersistentSetV2Adapter((org.hibernate.collection.internal.PersistentSet) collection);
        if (collection instanceof List)
            return new PersistentListV2Adapter(collection);
        return new PersistentCollectionV2Adapter(collection);
    }

    public org.hibernate.collection.spi.PersistentCollection getV5Collection() {
        return collection;
    }

    @Override
    public boolean isWrapper(final Object collection) {
        return this.collection.isWrapper(collection);
    }

    //---------- Not Implemented ----------//

    @Override
    public void delayedAddAll(final Collection coll) {
        throw new NotImplementedException("delayedAddAll not implemented");
    }

    @Override
    public void postFlush() {
        throw new NotImplementedException("postFlush not implemented");
    }

    @Override
    public Object getValue() {
        throw new NotImplementedException("getValue not implemented");
    }

    @Override
    public void beginRead() {
        throw new NotImplementedException("beginRead not implemented");
    }

    @Override
    public boolean endRead() {
        throw new NotImplementedException("endRead not implemented");
    }

    @Override
    public boolean isDirectlyAccessible() {
        throw new NotImplementedException("isDirectlyAccessible not implemented");
    }

    @Override
    public boolean needsRecreate(final CollectionPersister persister) {
        throw new NotImplementedException("needsRecreate not implemented");
    }

    @Override
    public CollectionSnapshot getCollectionSnapshot() {
        throw new NotImplementedException("getCollectionSnapshot not implemented");
    }

    @Override
    public void setCollectionSnapshot(final CollectionSnapshot collectionSnapshot) {
        throw new NotImplementedException("setCollectionSnapshot not implemented");
    }

    @Override
    public void preInsert(final CollectionPersister persister) throws HibernateException {
        throw new NotImplementedException("preInsert not implemented");
    }

    @Override
    public void afterRowInsert(final CollectionPersister persister, final Object entry, final int i) throws HibernateException {
        throw new NotImplementedException("afterRowInsert not implemented");
    }

    @Override
    public boolean empty() {
        throw new NotImplementedException("empty not implemented");
    }

    @Override
    public void initializeFromCache(final CollectionPersister persister, final Serializable disassembled, final Object owner) throws HibernateException {
        throw new NotImplementedException("initializeFromCache not implemented");
    }

    @Override
    public Iterator entries() {
        throw new NotImplementedException("entries not implemented");
    }

    @Override
    public Object readFrom(final ResultSet rs, final CollectionPersister role, final Object owner) throws HibernateException, SQLException {
        throw new NotImplementedException("readFrom not implemented");
    }

    @Override
    public void writeTo(final PreparedStatement st, final CollectionPersister role, final Object entry, final int i, final boolean writeOrder) throws HibernateException, SQLException {
        throw new NotImplementedException("writeTo not implemented");
    }

    @Override
    public Object getIndex(final Object entry, final int i) {
        throw new NotImplementedException("getIndex not implemented");
    }

    @Override
    public void beforeInitialize(final CollectionPersister persister) {
        throw new NotImplementedException("beforeInitialize not implemented");
    }

    @Override
    public boolean equalsSnapshot(final Type elementType) throws HibernateException {
        throw new NotImplementedException("equalsSnapshot not implemented");
    }

    @Override
    protected Serializable snapshot(final CollectionPersister persister) throws HibernateException {
        throw new NotImplementedException("snapshot not implemented");
    }

    @Override
    public Serializable disassemble(final CollectionPersister persister) throws HibernateException {
        throw new NotImplementedException("disassemble not implemented");
    }

    @Override
    public boolean entryExists(final Object entry, final int i) {
        throw new NotImplementedException("entryExists not implemented");
    }

    @Override
    public boolean needsInserting(final Object entry, final int i, final Type elemType) throws HibernateException {
        throw new NotImplementedException("needsInsertings not implemented");
    }

    @Override
    public boolean needsUpdating(final Object entry, final int i, final Type elemType) throws HibernateException {
        throw new NotImplementedException("needsUpdating not implemented");
    }

    @Override
    public Iterator getDeletes(final Type elemType) throws HibernateException {
        throw new NotImplementedException("getDeletes not implemented");
    }

    @Override
    public Collection getOrphans(final Serializable snapshot) throws HibernateException {
        throw new NotImplementedException("getOrphans not implemented");
    }
}
