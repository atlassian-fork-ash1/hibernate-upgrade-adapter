package com.atlassian.hibernate.adapter.adapters.criteria;

import org.hibernate.transform.ResultTransformer;

import java.util.List;

/**
 * An adapter bridging the ResultTransformer interface for hibernate v2 to v5 (onwards).
 */
public class ResultTransformerV2Adapter implements ResultTransformer {
    private final net.sf.hibernate.transform.ResultTransformer resultTransformer;

    protected ResultTransformerV2Adapter(final net.sf.hibernate.transform.ResultTransformer resultTransformer) {
        this.resultTransformer = resultTransformer;
    }

    public static ResultTransformer adapt(final net.sf.hibernate.transform.ResultTransformer resultTransformer) {
        if (resultTransformer == null) {
            return null;
        }
        return new ResultTransformerV2Adapter(resultTransformer);
    }

    @Override
    public Object transformTuple(final Object[] tuple, final String[] aliases) {
        return resultTransformer.transformTuple(tuple, aliases);
    }

    @Override
    public List transformList(final List collection) {
        return resultTransformer.transformList(collection);
    }
}
