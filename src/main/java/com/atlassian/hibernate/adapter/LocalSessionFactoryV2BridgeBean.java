package com.atlassian.hibernate.adapter;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.cfg.Configuration;
import net.sf.hibernate.cfg.NamingStrategy;
import org.apache.commons.lang3.NotImplementedException;
import org.hibernate.boot.Metadata;

import javax.transaction.TransactionManager;
import java.util.function.Supplier;

/**
 * This is a drop-in replacement for
 * org.springframework.orm.hibernate.LocalSessionFactoryBean
 * that creates SessionFactoryBridge objects to provide alternate
 * SessionFactory and Session implementation versions.
 */
public class LocalSessionFactoryV2BridgeBean
        extends org.springframework.orm.hibernate.LocalSessionFactoryBean {

    private Supplier<HibernateBridgeMode> bridgeMode;

    @Override
    protected Configuration newConfiguration() throws HibernateException {
        final ConfigurationV2Bridge config = new ConfigurationV2Bridge() {
            @Override
            protected org.hibernate.cfg.Configuration buildConfigurationV5() throws HibernateException {
                return LocalSessionFactoryV2BridgeBean.this.buildConfigurationV5(this);
            }

            @Override
            protected void updateV5MetaData(final Metadata metadata) {
                LocalSessionFactoryV2BridgeBean.this.updateV5MetaData(this, metadata);
            }
        };
        if (bridgeMode != null) {
            config.setBridgeModeSupplier(bridgeMode);
        }
        return config;
    }

    /**
     * Build a V5 Configuration object from the V2 Configuration and Settings objects.
     */
    protected org.hibernate.cfg.Configuration buildConfigurationV5(
            final ConfigurationV2Bridge configV2) throws HibernateException {

        return configV2.buildConfigurationV5Internal();
    }

    /**
     * Propagate v2 PersistentClass changes to the v5 Metadata object.
     */
    protected void updateV5MetaData(
            final ConfigurationV2Bridge configV2,
            final Metadata metadata) {

        configV2.updateV5MetaDataInternal(metadata);
    }

    /**
     * Set the HibernateBridgeMode that determines the dominate hibernate stack version to use.
     */
    public void setBridgeMode(final HibernateBridgeMode bridgeMode) {
        this.bridgeMode = () -> bridgeMode;
    }

    /**
     * Set the HibernateBridgeMode that determines the dominate hibernate stack version to use.
     */
    public void setBridgeModeSupplier(final Supplier<HibernateBridgeMode> bridgeMode) {
        this.bridgeMode = bridgeMode;
    }

    //---------- Not Implemented ----------//

    @Override
    public void setUseTransactionAwareDataSource(final boolean useTransactionAwareDataSource) {
        if (useTransactionAwareDataSource) {
            throw new NotImplementedException("setUseTransactionAwareDataSource not implemented");
        }
    }

    @Override
    public void setJtaTransactionManager(final TransactionManager jtaTransactionManager) {
        if (jtaTransactionManager != null) {
            throw new NotImplementedException("setJtaTransactionManager not implemented");
        }
    }

    @Override
    public void setNamingStrategy(final NamingStrategy namingStrategy) {
        if (namingStrategy != null) {
            throw new NotImplementedException("setNamingStrategy not implemented");
        }
    }
}
